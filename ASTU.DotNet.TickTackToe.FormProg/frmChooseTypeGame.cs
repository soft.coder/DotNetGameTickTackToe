﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ASTU.DotNet.TickTackToe.FormProg
{
    public partial class frmChooseTypeGame : Form
    {
        public bool IsSinglePlayer { get; set; } 
        public frmChooseTypeGame()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSinglePlayer_Click(object sender, EventArgs e)
        {
            this.IsSinglePlayer = true;
            this.Hide();
        }

        private void btnNetwork_Click(object sender, EventArgs e)
        {
            this.IsSinglePlayer = false;
            this.Hide();
        }
    }
}
