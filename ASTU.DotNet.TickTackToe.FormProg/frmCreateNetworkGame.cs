﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using ASTU.DotNet.TickTackToe;
using ASTU.DotNet.TickTackToe.GameParameter;
using ASTU.DotNet.TickTackToe.NetRemoting;


using System.Collections;

namespace ASTU.DotNet.TickTackToe.FormProg
{
    public partial class frmCreateNetworkGame : Form
    {

        public NetRemObj RemObjServer { get; set; }
        public Player ServerPlayer { get; set; } 

        public frmCreateNetworkGame()
        {
            InitializeComponent();
            this.ServerPlayer = new Player();
            
        }



        private void btnCreate_Click(object sender, EventArgs e)
        {
            BinaryServerFormatterSinkProvider serverProv = new BinaryServerFormatterSinkProvider();
            serverProv.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
            BinaryClientFormatterSinkProvider clientProv = new BinaryClientFormatterSinkProvider();

            IDictionary props = new Hashtable();
            int port = int.Parse(txtPort.Text);
            props["port"] = port;

            TcpChannel chan = new TcpChannel(props, clientProv, serverProv);
            ChannelServices.RegisterChannel(chan, false);

            //int port = int.Parse(txtPort.Text);
            //TcpChannel chan = new TcpChannel(port);
            //ChannelServices.RegisterChannel(chan);

            string nameRemoteField = txtName.Text;
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(ASTU.DotNet.TickTackToe.NetRemoting.NetRemObj),
                                                                nameRemoteField,
                                                                WellKnownObjectMode.Singleton);



            string strUrlField = string.Format("tcp://{0}:{1}/{2}", IPAddress.Loopback , port , nameRemoteField);
            object remoteObj = Activator.GetObject(typeof(ASTU.DotNet.TickTackToe.NetRemoting.NetRemObj), strUrlField);
            this.RemObjServer = (NetRemObj)remoteObj;
            
            ServerPlayerInit();
            this.RemObjServer.Connected(this.ServerPlayer);
            //this.RemObjServer.ConnectedNewPlayer += new NetRemObj.NetRemoteObjEventHandler(RemObjServer_ConnectedNewPlayer);
            DisableControls();
            ListInfoInit();
            timerCheckConnection.Enabled = true;

        }

        private void ListInfoInit()
        {
            lstPlayers.Items.Clear();
            string clientInfo, serverInfo;

            if (this.RemObjServer.RemoteParam.ServerPlayer != null)
            {
                serverInfo = string.Format("Server: Type: {0}, Figure: {1}",
                                    this.RemObjServer.RemoteParam.ServerPlayer.Type,
                                    this.RemObjServer.RemoteParam.ServerPlayer.Figure);


                lstPlayers.Items.Add(serverInfo); 
            }

            if (this.RemObjServer.RemoteParam.ClientPlayer != null)
            {
                clientInfo = string.Format("Client: Type: {0}, Figure: {1}",
                            this.RemObjServer.RemoteParam.ClientPlayer.Type,
                            this.RemObjServer.RemoteParam.ClientPlayer.Figure);
                lstPlayers.Items.Add(clientInfo);
            }
        }

        void RemObjServer_ConnectedNewPlayer(object sender, NetRemObjEventArgs args)
        {
            lstPlayers.Items.Add("Client player");
            btnBeginGame.Enabled = true;
        }

        private void DisableControls()
        {
            this.grpFigure.Enabled = false;
            this.grpFirstStep.Enabled = false;
            this.grpTypePlayer.Enabled = false;
            this.btnCreate.Enabled = false;
        }

        private void ServerPlayerInit()
        {
            if (this.rdbServerUser.Checked == true)
                this.ServerPlayer.Type = PlayerType.User;
            else
                this.ServerPlayer.Type = PlayerType.Robot;

            if (this.rdbCircleFigure.Checked == true)
                this.ServerPlayer.Figure = CellState.Tack;
            else
                this.ServerPlayer.Figure = CellState.Tick;

            if ( this.rdbFirstStepServer.Checked == true )
                this.RemObjServer.FirstStepIsServer(true);
            else 
                this.RemObjServer.FirstStepIsServer(false);

        }

        private void frmCreateNetworkGame_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void timerCheckConnection_Tick(object sender, EventArgs e)
        {
            if (this.RemObjServer.RemoteParam.ClientIsReady == true)
                btnBeginGame.Enabled = true;
            else
                btnBeginGame.Enabled = false;
               
            this.ListInfoInit();
        }

        private void btnBeginGame_Click(object sender, EventArgs e)
        {
            this.RemObjServer.IsGameBegin = true;
            this.Hide();

        }
    }
}
