﻿namespace ASTU.DotNet.TickTackToe.FormProg
{
    partial class frmChooseNetworkTypeGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnWCF = new System.Windows.Forms.Button();
            this.btnNetRemoting = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnExit.Location = new System.Drawing.Point(12, 146);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(260, 61);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "ASP.NET";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnWCF
            // 
            this.btnWCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnWCF.Location = new System.Drawing.Point(12, 79);
            this.btnWCF.Name = "btnWCF";
            this.btnWCF.Size = new System.Drawing.Size(260, 61);
            this.btnWCF.TabIndex = 2;
            this.btnWCF.Text = "WCF";
            this.btnWCF.UseVisualStyleBackColor = true;
            this.btnWCF.Click += new System.EventHandler(this.btnWCF_Click);
            // 
            // btnNetRemoting
            // 
            this.btnNetRemoting.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNetRemoting.Location = new System.Drawing.Point(12, 12);
            this.btnNetRemoting.Name = "btnNetRemoting";
            this.btnNetRemoting.Size = new System.Drawing.Size(260, 61);
            this.btnNetRemoting.TabIndex = 1;
            this.btnNetRemoting.Text = ".NET Remoting";
            this.btnNetRemoting.UseVisualStyleBackColor = true;
            this.btnNetRemoting.Click += new System.EventHandler(this.btnNetRemoting_Click);
            // 
            // frmChooseNetworkTypeGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 221);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnWCF);
            this.Controls.Add(this.btnNetRemoting);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmChooseNetworkTypeGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmChooseNetworkGame";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnWCF;
        private System.Windows.Forms.Button btnNetRemoting;
    }
}