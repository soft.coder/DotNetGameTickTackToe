﻿namespace ASTU.DotNet.TickTackToe.FormProg
{
    partial class frmStartSinglePlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLoadRobotPlayerOne = new System.Windows.Forms.Button();
            this.rdbCrossRobot = new System.Windows.Forms.RadioButton();
            this.rdbСrossUser = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLoadRobotPlayerTwo = new System.Windows.Forms.Button();
            this.rdbCircleRobot = new System.Windows.Forms.RadioButton();
            this.rdbCircleUser = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdbFirstStepCircle = new System.Windows.Forms.RadioButton();
            this.rdbFirstStepCross = new System.Windows.Forms.RadioButton();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLoadRobotPlayerOne);
            this.groupBox1.Controls.Add(this.rdbCrossRobot);
            this.groupBox1.Controls.Add(this.rdbСrossUser);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(197, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Игрок 1";
            // 
            // btnLoadRobotPlayerOne
            // 
            this.btnLoadRobotPlayerOne.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Open_16x16;
            this.btnLoadRobotPlayerOne.Location = new System.Drawing.Point(160, 52);
            this.btnLoadRobotPlayerOne.Name = "btnLoadRobotPlayerOne";
            this.btnLoadRobotPlayerOne.Size = new System.Drawing.Size(31, 30);
            this.btnLoadRobotPlayerOne.TabIndex = 2;
            this.btnLoadRobotPlayerOne.UseVisualStyleBackColor = true;
            this.btnLoadRobotPlayerOne.Visible = false;
            this.btnLoadRobotPlayerOne.Click += new System.EventHandler(this.btnLoadRobotPlayerOne_Click);
            // 
            // rdbCrossRobot
            // 
            this.rdbCrossRobot.AutoSize = true;
            this.rdbCrossRobot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbCrossRobot.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.robot_icon_16x16;
            this.rdbCrossRobot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbCrossRobot.Location = new System.Drawing.Point(6, 55);
            this.rdbCrossRobot.Name = "rdbCrossRobot";
            this.rdbCrossRobot.Size = new System.Drawing.Size(89, 24);
            this.rdbCrossRobot.TabIndex = 0;
            this.rdbCrossRobot.Text = "Робот";
            this.rdbCrossRobot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbCrossRobot.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbCrossRobot.UseVisualStyleBackColor = true;
            // 
            // rdbСrossUser
            // 
            this.rdbСrossUser.AutoSize = true;
            this.rdbСrossUser.Checked = true;
            this.rdbСrossUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbСrossUser.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.User_icon;
            this.rdbСrossUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbСrossUser.Location = new System.Drawing.Point(6, 25);
            this.rdbСrossUser.Name = "rdbСrossUser";
            this.rdbСrossUser.Size = new System.Drawing.Size(113, 24);
            this.rdbСrossUser.TabIndex = 0;
            this.rdbСrossUser.TabStop = true;
            this.rdbСrossUser.Text = "Человек ";
            this.rdbСrossUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbСrossUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbСrossUser.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLoadRobotPlayerTwo);
            this.groupBox2.Controls.Add(this.rdbCircleRobot);
            this.groupBox2.Controls.Add(this.rdbCircleUser);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(215, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(197, 94);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Игрок 2";
            // 
            // btnLoadRobotPlayerTwo
            // 
            this.btnLoadRobotPlayerTwo.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Open_16x16;
            this.btnLoadRobotPlayerTwo.Location = new System.Drawing.Point(160, 52);
            this.btnLoadRobotPlayerTwo.Name = "btnLoadRobotPlayerTwo";
            this.btnLoadRobotPlayerTwo.Size = new System.Drawing.Size(31, 30);
            this.btnLoadRobotPlayerTwo.TabIndex = 2;
            this.btnLoadRobotPlayerTwo.UseVisualStyleBackColor = true;
            this.btnLoadRobotPlayerTwo.Visible = false;
            this.btnLoadRobotPlayerTwo.Click += new System.EventHandler(this.btnLoadRobotPlayerTwo_Click);
            // 
            // rdbCircleRobot
            // 
            this.rdbCircleRobot.AutoSize = true;
            this.rdbCircleRobot.Checked = true;
            this.rdbCircleRobot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbCircleRobot.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.robot_icon_16x16;
            this.rdbCircleRobot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbCircleRobot.Location = new System.Drawing.Point(6, 55);
            this.rdbCircleRobot.Name = "rdbCircleRobot";
            this.rdbCircleRobot.Size = new System.Drawing.Size(89, 24);
            this.rdbCircleRobot.TabIndex = 0;
            this.rdbCircleRobot.TabStop = true;
            this.rdbCircleRobot.Text = "Робот";
            this.rdbCircleRobot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbCircleRobot.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbCircleRobot.UseVisualStyleBackColor = true;
            // 
            // rdbCircleUser
            // 
            this.rdbCircleUser.AutoSize = true;
            this.rdbCircleUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbCircleUser.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.User_icon;
            this.rdbCircleUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbCircleUser.Location = new System.Drawing.Point(6, 25);
            this.rdbCircleUser.Name = "rdbCircleUser";
            this.rdbCircleUser.Size = new System.Drawing.Size(113, 24);
            this.rdbCircleUser.TabIndex = 0;
            this.rdbCircleUser.Text = "Человек ";
            this.rdbCircleUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbCircleUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbCircleUser.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdbFirstStepCircle);
            this.groupBox3.Controls.Add(this.rdbFirstStepCross);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(12, 112);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(197, 94);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Первый ход";
            // 
            // rdbFirstStepCircle
            // 
            this.rdbFirstStepCircle.AutoSize = true;
            this.rdbFirstStepCircle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbFirstStepCircle.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.circle_icon_16x16;
            this.rdbFirstStepCircle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbFirstStepCircle.Location = new System.Drawing.Point(6, 55);
            this.rdbFirstStepCircle.Name = "rdbFirstStepCircle";
            this.rdbFirstStepCircle.Size = new System.Drawing.Size(100, 24);
            this.rdbFirstStepCircle.TabIndex = 0;
            this.rdbFirstStepCircle.Text = "Нолики";
            this.rdbFirstStepCircle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbFirstStepCircle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbFirstStepCircle.UseVisualStyleBackColor = true;
            // 
            // rdbFirstStepCross
            // 
            this.rdbFirstStepCross.AutoSize = true;
            this.rdbFirstStepCross.Checked = true;
            this.rdbFirstStepCross.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbFirstStepCross.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.cross_icon_16x16;
            this.rdbFirstStepCross.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbFirstStepCross.Location = new System.Drawing.Point(6, 25);
            this.rdbFirstStepCross.Name = "rdbFirstStepCross";
            this.rdbFirstStepCross.Size = new System.Drawing.Size(114, 24);
            this.rdbFirstStepCross.TabIndex = 0;
            this.rdbFirstStepCross.TabStop = true;
            this.rdbFirstStepCross.Text = "Крестики";
            this.rdbFirstStepCross.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbFirstStepCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbFirstStepCross.UseVisualStyleBackColor = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "DotNet dll|*.dll";
            // 
            // btnOk
            // 
            this.btnOk.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Ok_icon_16x16;
            this.btnOk.Location = new System.Drawing.Point(358, 172);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(54, 33);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Ок";
            this.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmStartSinglePlay
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 217);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmStartSinglePlay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Начало";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbСrossUser;
        private System.Windows.Forms.RadioButton rdbCrossRobot;
        private System.Windows.Forms.RadioButton rdbCircleUser;
        private System.Windows.Forms.RadioButton rdbCircleRobot;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbFirstStepCircle;
        private System.Windows.Forms.RadioButton rdbFirstStepCross;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnLoadRobotPlayerOne;
        private System.Windows.Forms.Button btnLoadRobotPlayerTwo;
    }
}