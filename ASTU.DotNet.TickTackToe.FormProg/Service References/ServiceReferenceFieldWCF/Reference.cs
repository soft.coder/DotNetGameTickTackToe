﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASTU.DotNet.TickTackToe.FormProg.ServiceReferenceFieldWCF {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReferenceFieldWCF.FieldWCF")]
    public interface FieldWCF {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/Connect", ReplyAction="http://tempuri.org/FieldWCF/ConnectResponse")]
        int Connect();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/GetCountConnected", ReplyAction="http://tempuri.org/FieldWCF/GetCountConnectedResponse")]
        int GetCountConnected();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/GetLastTurnState", ReplyAction="http://tempuri.org/FieldWCF/GetLastTurnStateResponse")]
        ASTU.DotNet.TickTackToe.CellState GetLastTurnState();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/Turn", ReplyAction="http://tempuri.org/FieldWCF/TurnResponse")]
        void Turn(ASTU.DotNet.TickTackToe.CellState state, int x, int y);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/Cancel", ReplyAction="http://tempuri.org/FieldWCF/CancelResponse")]
        void Cancel(int TurnStep);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/IsEnd", ReplyAction="http://tempuri.org/FieldWCF/IsEndResponse")]
        bool IsEnd();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/GetMarkedCells", ReplyAction="http://tempuri.org/FieldWCF/GetMarkedCellsResponse")]
        ASTU.DotNet.TickTackToe.Cell[] GetMarkedCells();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/GetMarkedCellsWithState", ReplyAction="http://tempuri.org/FieldWCF/GetMarkedCellsWithStateResponse")]
        ASTU.DotNet.TickTackToe.Cell[] GetMarkedCellsWithState(ASTU.DotNet.TickTackToe.CellState state);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/FieldWCF/Clear", ReplyAction="http://tempuri.org/FieldWCF/ClearResponse")]
        void Clear();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface FieldWCFChannel : ASTU.DotNet.TickTackToe.FormProg.ServiceReferenceFieldWCF.FieldWCF, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FieldWCFClient : System.ServiceModel.ClientBase<ASTU.DotNet.TickTackToe.FormProg.ServiceReferenceFieldWCF.FieldWCF>, ASTU.DotNet.TickTackToe.FormProg.ServiceReferenceFieldWCF.FieldWCF {
        
        public FieldWCFClient() {
        }
        
        public FieldWCFClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FieldWCFClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FieldWCFClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FieldWCFClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int Connect() {
            return base.Channel.Connect();
        }
        
        public int GetCountConnected() {
            return base.Channel.GetCountConnected();
        }
        
        public ASTU.DotNet.TickTackToe.CellState GetLastTurnState() {
            return base.Channel.GetLastTurnState();
        }
        
        public void Turn(ASTU.DotNet.TickTackToe.CellState state, int x, int y) {
            base.Channel.Turn(state, x, y);
        }
        
        public void Cancel(int TurnStep) {
            base.Channel.Cancel(TurnStep);
        }
        
        public bool IsEnd() {
            return base.Channel.IsEnd();
        }
        
        public ASTU.DotNet.TickTackToe.Cell[] GetMarkedCells() {
            return base.Channel.GetMarkedCells();
        }
        
        public ASTU.DotNet.TickTackToe.Cell[] GetMarkedCellsWithState(ASTU.DotNet.TickTackToe.CellState state) {
            return base.Channel.GetMarkedCellsWithState(state);
        }
        
        public void Clear() {
            base.Channel.Clear();
        }
    }
}
