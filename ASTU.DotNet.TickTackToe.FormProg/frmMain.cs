﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;

using ASTU.DotNet.TickTackToe;

using Robocop = ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV;

using ASTU.DotNet.TickTackToe.GameParameter;

using ASTU.DotNet.TickTackToe.NetRemoting;

using System.Net;
using System.Runtime.Remoting;

namespace ASTU.DotNet.TickTackToe.FormProg
{
    public partial class frmMain : Form
    {
        #region PropertiesOfClass
        int BaseX { get; set; }
        int BaseY { get; set; }

        int CellWidth { get; set; }
        Pen CellPen { get; set; }

        IField TickTackField { get; set; }


        Robocop.Robot myRobot { get; set; }


        private Font NumberingGridFont { get; set; }
        private SolidBrush NumbericGridBrush { get; set; }


        public bool IsDebugMode { get; set; }

        public frmChooseTypeGame wndChooseTypeGame { get; set; }
        public frmChooseNetworkTypeGame wndChooseNetworkTypeGame { get; set; } 
        public frmStartSinglePlay wndStartSinglePlay { get; set; }
        public frmStartNetworkGame wndStartNetworkGame { get; set; }
        public frmCreateNetworkGame wndCreateNetworkGame { get; set; }
        public frmConnectToNetworkGame wndConnectToNetworkGame { get; set; } 

        public IGameParam StartParam { get; set; }

        public bool IsUserCanStep { get; set; }
        public bool IsRobotCanStep { get; set; }

        public bool IsMoveOnField { get; set; }
        public Point BeginPoint { get; set; }
        public Point EndPoint { get; set; }
        public int DifX { get; set; }
        public int DifY { get; set; }

        public bool IsSinglePlayer { get; set; }
        public bool IsNetworkGame { get; set; }

        public NetRemObj remoteObj { get; set; }
        public bool IsNetRemotingGame { get; set; }
        public NetworkOrder OurOrderNetworkPlay { get; set; }
        public PlayerType PlayerNetType { get; set; }

        public bool IsWcfGame { get; set; } 
        public GameParam ParamWcfGame { get; set; }
        public OrderPlay MyWCFOrder { get; set; }
        public ServiceReferenceFieldWCF.FieldWCFClient FieldWCF { get; set; } 

        public bool isAspNetGame { get; set; }
        public GameParam ParamAspGame { get; set; }
        public OrderPlay MyASPOrder { get; set; }
        public ServiceReferenceASP.FieldWCFClient FieldASP {get; set; }

        public bool isMouseOnPictureBoxField { get; set; }

        #endregion


        #region MainInitialization
        public frmMain()
        {
            InitializeComponent();
            this.BaseX = 0;
            this.BaseY = 0;

            TickTackField = new Field();
            
            this.CellWidth = 32;
            CellPen = new Pen(Color.Gray, 2);

            wndChooseTypeGame = new frmChooseTypeGame();

            wndChooseTypeGame.ShowDialog();

            if (wndChooseTypeGame.IsSinglePlayer == true)
            {
                this.IsSinglePlayer = true;

                wndStartSinglePlay = new frmStartSinglePlay();
                wndStartSinglePlay.ShowDialog();

                StartParam = new GameParam(wndStartSinglePlay.PlayersParam);
                //this.IsUserCanStep = false;

                myRobot = new Robocop.Robot();
                //myRobot.Init(CellState.Tick);

                this.NumberingGridFont = new Font(FontFamily.GenericMonospace, 10);
                this.NumbericGridBrush = new SolidBrush(Color.Green);

                this.IsDebugMode = true;

                this.toolStripButtonUndo.Enabled = false;

                //StartPlay();

                //this.tsCmbSize.SelectedIndex = 1;


                timerTurn.Enabled = true;
            }
            else
            {
                this.IsNetworkGame = true;
                
                wndChooseNetworkTypeGame = new frmChooseNetworkTypeGame();
                
                //wndChooseNetworkTypeGame.ShowDialog();
                // !!!!!!!!! Only NetRemoting !!!!! other works but don't need today
                wndChooseNetworkTypeGame.IsNetRemtingGame = true;
                
                if (wndChooseNetworkTypeGame.IsNetRemtingGame == true)
                {
                    this.IsNetRemotingGame = true;
                    remoteObj = new NetRemObj();

                    wndStartNetworkGame = new frmStartNetworkGame();
                    wndStartNetworkGame.ShowDialog();


                    this.NumberingGridFont = new Font(FontFamily.GenericMonospace, 10);
                    this.NumbericGridBrush = new SolidBrush(Color.Green);

                    this.IsDebugMode = true;
                    this.toolStripButtonUndo.Enabled = false;
                    this.toolStripButtonRestart.Enabled = false;

                    this.tsCmbSize.SelectedIndex = 1;


                    if (wndStartNetworkGame.IsCreatedGame == true)
                    {
                        this.OurOrderNetworkPlay = NetworkOrder.ServerOrder;
                        wndCreateNetworkGame = new frmCreateNetworkGame();
                        wndCreateNetworkGame.ShowDialog();
                        if (wndCreateNetworkGame.RemObjServer != null)
                        {
                            remoteObj = wndCreateNetworkGame.RemObjServer;
                            this.TickTackField = (IField)remoteObj;
                            this.PlayerNetType = this.remoteObj.RemoteParam.ServerPlayer.Type;
                            timerTurn.Enabled = true;
                        }
                    }
                    else
                    {
                        this.OurOrderNetworkPlay = NetworkOrder.ClientOrder;
                        wndConnectToNetworkGame = new frmConnectToNetworkGame();
                        wndConnectToNetworkGame.ShowDialog();
                        if (wndConnectToNetworkGame.RemObjClient != null)
                        {
                            remoteObj = wndConnectToNetworkGame.RemObjClient;
                            this.TickTackField = (IField)remoteObj;
                            this.PlayerNetType = this.remoteObj.RemoteParam.ClientPlayer.Type;
                            timerTurn.Enabled = true;
                        }
                    } 
                }
                else if (wndChooseNetworkTypeGame.IsWcfGame == true)
                {
                    this.IsWcfGame = true;
                    this.FieldWCF = new ServiceReferenceFieldWCF.FieldWCFClient();
                    //this.TickTackField = (IField)this.FieldWCF;

                    int countConnected = FieldWCF.Connect();
                    
                    if (countConnected != -1 )
                    {
                        this.ParamWcfGame = new GameParam();
                        this.ParamWcfGame.First = new Player(PlayerType.User, CellState.Tick);
                        this.ParamWcfGame.Second = new Player(PlayerType.User, CellState.Tack);
                        this.ParamWcfGame.Order = OrderPlay.FirstPlayer;
                        if (countConnected == 1)
                        {
                            this.MyWCFOrder = OrderPlay.FirstPlayer;
                            timerTurn.Enabled = true;
                        }
                        else if (countConnected == 2)
                        {
                            this.MyWCFOrder = OrderPlay.SecondPlayer;
                            timerTurn.Enabled = true;
                        }
                        else
                            throw new ApplicationException("WCF Host connect denied count player");
                    }
                }
                else if (wndChooseNetworkTypeGame.IsAspNetGame == true)
                {
                    this.isAspNetGame = true;
                    this.FieldASP = new ServiceReferenceASP.FieldWCFClient();
                    //this.TickTackField = (IField)this.FieldWCF;

                    int countConnected = FieldASP.Connect();

                    if (countConnected != -1)
                    {
                        this.ParamAspGame = new GameParam();
                        this.ParamAspGame.First = new Player(PlayerType.User, CellState.Tick);
                        this.ParamAspGame.Second = new Player(PlayerType.User, CellState.Tack);
                        this.ParamAspGame.Order = OrderPlay.FirstPlayer;
                        if (countConnected == 1)
                        {
                            this.MyASPOrder = OrderPlay.FirstPlayer;
                            timerTurn.Enabled = true;
                        }
                        else if (countConnected == 2)
                        {
                            this.MyASPOrder = OrderPlay.SecondPlayer;
                            timerTurn.Enabled = true;
                        }
                        else
                            throw new ApplicationException("WCF Host connect denied count player");
                    }
                    Application.Exit();
                }
            }

            pbField.MouseWheel += new MouseEventHandler(pbField_MouseWheel);
            pbField.Focus();
        }

        void pbField_MouseWheel(object sender, MouseEventArgs e)
        {
            if (this.isMouseOnPictureBoxField == true)
            {
                int deltaCellWidth = (e.Delta / 120) * 4;
                int cellWidthNew = this.CellWidth + deltaCellWidth;
                if (cellWidthNew >= 6 && cellWidthNew <= 300)
                {
                    
                    this.BaseX += deltaCellWidth / 2;
                    this.BaseY += deltaCellWidth / 2;

                    double mouseOffsetOfCenterPosX = (double)e.X - (double)pbField.Width / 2.0 - this.BaseX;
                    double oldOffsetOfLogCenterMousePosX = mouseOffsetOfCenterPosX + CellWidth / 2.0;
                    double numCellMousePosX = (oldOffsetOfLogCenterMousePosX) / (double)this.CellWidth;
                    double newOffsetOfLogCenterMousePosX = numCellMousePosX * (this.CellWidth + deltaCellWidth);
                    double needDeltaOffsetMousePosX = newOffsetOfLogCenterMousePosX - oldOffsetOfLogCenterMousePosX;
                    this.BaseX -= (int)Math.Round(needDeltaOffsetMousePosX);

                    double mouseOffsetOfCenterPosY = (double)e.Y - (double)pbField.Height / 2.0 - this.BaseY;
                    double oldOffsetOfLogCenterMousePosY = mouseOffsetOfCenterPosY + CellWidth / 2.0;
                    double numCellMousePosY = (oldOffsetOfLogCenterMousePosY) / (double)this.CellWidth;
                    double newOffsetOfLogCenterMousePosY = numCellMousePosY * (this.CellWidth + deltaCellWidth);
                    double needDeltaOffsetMousePosY = newOffsetOfLogCenterMousePosY - oldOffsetOfLogCenterMousePosY;
                    this.BaseY -= (int)Math.Round(needDeltaOffsetMousePosY);


                    this.CellWidth += deltaCellWidth;
                    this.statusLbl.Text = this.CellWidth.ToString();
                }
                pbField.Invalidate();
            }
        }

        #endregion 

        #region PaintMethods
        private void pictureBoxField_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            g.TranslateTransform(this.pbField.Width / 2 - CellWidth / 2, this.pbField.Height / 2 - CellWidth / 2);
            g.TranslateTransform(BaseX, BaseY); 

            int halfHeight = pbField.Height / 2 + CellWidth/2;
            int halfWidth = pbField.Width / 2 + CellWidth/2;


            DrawCrossAndCircle(g);

            //g.DrawLine(this.CellPen, new Point(0, -halfHeight),
            //                         new Point(0, halfHeight));
            //g.DrawLine(this.CellPen, new Point(-halfWidth, 0),
            //                         new Point(halfWidth,  0));
           
            //for (int x0 = CellWidth, y0 = CellWidth;
            //         x0 < halfWidth + BaseX || y0 < halfHeight + BaseY;
            //         x0 += CellWidth, y0 += CellWidth)
            //{
            //    //vertical line
            //    if (x0 < halfWidth + BaseX)
            //    {
            //        g.DrawLine(this.CellPen, new Point( x0, -(halfHeight + BaseY)),
            //                                 new Point( x0,  (halfHeight - BaseY)));
            //        g.DrawLine(this.CellPen, new Point(-x0, -(halfHeight + BaseY)),
            //                                 new Point(-x0,  (halfHeight - BaseY)));
            //    }
            //    //if( -x0 > halfWidth

            //    //horizontal line
            //    if (y0 < halfHeight + BaseY)
            //    {
            //        g.DrawLine(this.CellPen, new Point(-(halfWidth + BaseX),  y0),
            //                                 new Point( (halfWidth - BaseX),  y0));
            //        g.DrawLine(this.CellPen, new Point(-(halfWidth + BaseX), -y0),
            //                                 new Point( (halfWidth - BaseX), -y0));
            //    }
            //}

            //vertical


            this.CellPen.Width = (float)Math.Round((double)this.CellWidth * 0.0666667);
            this.NumberingGridFont = new Font(FontFamily.GenericMonospace, (float)this.CellWidth * 0.3333f);
            for (int x0 = 0; x0 < halfWidth - BaseX; x0 += CellWidth)
            {
                g.DrawLine(this.CellPen, new Point(x0, -(halfHeight + BaseY)),
                                         new Point(x0,  (halfHeight - BaseY)));
                GridNumbDraw(g, x0 , 0);
            }
            for (int x0 = -CellWidth; x0 > -(halfWidth + BaseX ); x0 -= CellWidth)
            {
                g.DrawLine(this.CellPen, new Point(x0, -(halfHeight + BaseY)),
                                         new Point(x0,  (halfHeight - BaseY)));
                GridNumbDraw(g, x0, 0);
            }
            //horizontal
            for (int y0 = 0; y0 < halfHeight - BaseY ; y0 += CellWidth)
            {
                g.DrawLine(this.CellPen, new Point(-(halfWidth + BaseX), y0),
                                         new Point( (halfWidth - BaseX), y0));

                GridNumbDraw(g, 0, y0);
            }
            for (int y0 = -CellWidth; y0 > -(halfHeight + BaseY); y0 -= CellWidth)
            {
                g.DrawLine(this.CellPen, new Point(-(halfWidth + BaseX), y0),
                                         new Point( (halfWidth - BaseX), y0));
                GridNumbDraw(g, 0, y0);
            }

            if (IsDebugMode == true)
            {
                g.DrawLine(new Pen(Brushes.Red, this.CellPen.Width), new Point(0, -halfHeight - this.BaseY),
                                         new Point(0, halfHeight - this.BaseY));
                g.DrawLine(new Pen(Brushes.Red, this.CellPen.Width), new Point(-halfWidth - this.BaseX, 0),
                                         new Point(halfWidth - this.BaseX, 0));
            }
        }

        private void GridNumbDraw(Graphics g, int x , int y)
        {
            if (IsDebugMode == true)
            {
                string strNum = ((x + y) / CellWidth).ToString();
                
                g.DrawString(strNum, this.NumberingGridFont, this.NumbericGridBrush, new PointF(x, y));
            }
        }

        private void DrawCrossAndCircle(Graphics g)
        {
            IList<Cell> markedCells ;
            if (this.IsSinglePlayer == true || this.IsNetRemotingGame == true)
                markedCells = TickTackField.GetMarkedCells();
            else if ( this.IsWcfGame == true ) 
                markedCells = this.FieldWCF.GetMarkedCells();
            else if ( this.isAspNetGame == true ) 
                markedCells = this.FieldASP.GetMarkedCells();
            else
                throw new ApplicationException("What is the network type game");
            foreach (Cell cell in markedCells )
            {
                int x = cell.X * CellWidth;
                int y = cell.Y * CellWidth;
                float crossOrCircleWidth = (float)Math.Round(((double)this.CellWidth * 0.0625));
                if (cell.State == CellState.Tick)
                {
                    Pen p = new Pen(Brushes.Red, crossOrCircleWidth);
                    p.StartCap = System.Drawing.Drawing2D.LineCap.Round;
                    p.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                    g.DrawLine(p, new PointF(x + this.CellPen.Width, y + this.CellPen.Width), 
                                    new PointF(x + CellWidth - this.CellPen.Width , 
                                               y + CellWidth - this.CellPen.Width ));
                    g.DrawLine(p, new PointF(x + this.CellPen.Width, y + CellWidth - this.CellPen.Width), 
                                    new PointF(x + CellWidth - this.CellPen.Width, y + this.CellPen.Width));
                }
                else if (cell.State == CellState.Tack)
                {
                    RectangleF rect = new RectangleF(x + this.CellPen.Width, y + this.CellPen.Width, 
                                                    CellWidth - this.CellPen.Width * 2f,
                                                    CellWidth - this.CellPen.Width * 2f);
                    g.DrawEllipse(new Pen(Brushes.Green, crossOrCircleWidth), rect);
                }
            }
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            pbField.Invalidate();
        }



        private void pbField_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.EndPoint = e.Location;

                this.BaseX -= this.DifX;
                this.BaseY -= this.DifY;
                
                this.DifX = EndPoint.X - BeginPoint.X;
                this.DifY = EndPoint.Y - BeginPoint.Y;

                this.BaseX += this.DifX;
                this.BaseY += this.DifY;

                pbField.Invalidate();
            }
            Point CurPos = this.CurrnetPosInGrid(e);

            //if (TickTackField.IsSet(posX, posY)) this.Cursor = Cursors.No;
            //else this.Cursor = Cursors.NoMove2D;

            if (this.IsDebugMode == true && e.Button == MouseButtons.Middle)
            {
                int x = e.Location.X - (this.pbField.Width / 2 - CellWidth / 2);
                int y = e.Location.Y - (this.pbField.Height / 2 - CellWidth / 2);
                object[] statusInform = new object[] { x, y, e.Location.X, e.Y , CurPos.X, CurPos.Y };
                statusLbl.Text = string.Format("X:{0} ; Y {1} - X:{2} ; Y:{3} - PosX:{4} ; PosY:{5}", statusInform);

            }
            this.isMouseOnPictureBoxField = true;
            pbField.Focus();
        }
        private void pbField_MouseLeave(object sender, EventArgs e)
        {
            this.isMouseOnPictureBoxField = false;
            this.Focus();
        }

        private void toolStripChkBtnDebugMode_CheckStateChanged(object sender, EventArgs e)
        {
            if (toolStripChkBtnDebugMode.Checked == true)
                this.IsDebugMode = true;
            else
                this.IsDebugMode = false;

            pbField.Invalidate();
            statusLbl.Text = "";
        }

        #endregion


        #region TurnMethods



        //private void StartPlay()
        //{
        //    if (StartParam.First.Type == PlayerType.Robot && StartParam.Second.Type == PlayerType.Robot)
        //    {
        //        this.IsRobotCanStep = true;
        //        while (this.CheckOfEndTheGame() != true) 
        //        {
        //            StartParam.Order = OrderPlay.FirstPlayer;
        //            TurnRobot(StartParam.First);
                    
        //            Thread.Sleep(500);
                    
        //            StartParam.Order = OrderPlay.SecondPlayer;
        //            TurnRobot(StartParam.Second);

        //            StartParam.Order = OrderPlay.FirstPlayer;
        //            pbField.Invalidate();
        //        }
        //    }
        //    else if (StartParam.First.Type == PlayerType.Robot && StartParam.Second.Type == PlayerType.User)
        //    {
        //        this.IsRobotCanStep = true;
        //        this.IsUserCanStep = false;
        //        TurnRobot(StartParam.First);
                
        //        this.IsRobotCanStep = false;
        //        this.IsUserCanStep = true;
                                
        //    }
        //    else if (StartParam.First.Type == PlayerType.User && StartParam.Second.Type == PlayerType.Robot)
        //    {
        //        this.IsUserCanStep = true;
        //        this.IsRobotCanStep = false;
                
                
        //    }
        //}

        private void pbField_MouseClick(object sender, MouseEventArgs e)
        {
            if (this.IsUserCanStep == true && e.Button == MouseButtons.Left)
            {
                try
                {
                    Turn(e);
                }
                catch (ApplicationException ex)
                {
                    MessageBox.Show(ex.Message , "Ошибка хода");
                }
            }
        }

        private void Turn(MouseEventArgs e)
        {
            try
            {
                if (this.IsSinglePlayer == true)
                {
                    if (StartParam.Order == OrderPlay.FirstPlayer)
                    {
                        TurnPlayer(StartParam.First, e);
                        StartParam.Order = OrderPlay.SecondPlayer;
                    }
                    else
                    {
                        TurnPlayer(StartParam.Second, e);
                        StartParam.Order = OrderPlay.FirstPlayer;
                    }
                    pbField.Invalidate();
                    this.IsUserCanStep = false;
                }
                else if ( this.IsNetworkGame == true ) 
                {
                    if (this.IsNetRemotingGame == true)
                    {
                        if (this.OurOrderNetworkPlay == NetworkOrder.ServerOrder)
                        {
                            TurnPlayer(this.remoteObj.RemoteParam.ServerPlayer, e);
                            this.remoteObj.RemoteParam.Order = NetworkOrder.ClientOrder;
                            this.Cursor = Cursors.Cross;
                        }
                        else
                        {
                            TurnPlayer(this.remoteObj.RemoteParam.ClientPlayer, e);
                            this.remoteObj.RemoteParam.Order = NetworkOrder.ServerOrder;
                            this.Cursor = Cursors.Cross;
                        } 
                    }
                    else if (this.IsWcfGame == true)
                    {
                        if (this.MyWCFOrder == OrderPlay.FirstPlayer)
                        {
                            Point PosTurn = CurrnetPosInGrid(e);
                            this.FieldWCF.Turn(this.ParamWcfGame.First.Figure, PosTurn.X, PosTurn.Y);
                            pbField.Invalidate();

                            if (CheckOfEndTheGame() == true)
                                throw new ApplicationException("Game Over");
                        }
                        else
                        {
                            Point PosTurn = CurrnetPosInGrid(e);
                            this.FieldWCF.Turn(this.ParamWcfGame.Second.Figure, PosTurn.X, PosTurn.Y);
                            pbField.Invalidate();

                            if (CheckOfEndTheGame() == true)
                                throw new ApplicationException("Game Over");
                        }
                    }
                    else if (this.isAspNetGame == true)
                    {
                        if (this.MyASPOrder == OrderPlay.FirstPlayer)
                        {
                            Point PosTurn = CurrnetPosInGrid(e);
                            this.FieldASP.Turn(this.ParamAspGame.First.Figure, PosTurn.X, PosTurn.Y);
                            pbField.Invalidate();

                            if (CheckOfEndTheGame() == true)
                                throw new ApplicationException("Game Over");
                        }
                        else
                        {
                            Point PosTurn = CurrnetPosInGrid(e);
                            this.FieldASP.Turn(this.ParamAspGame.Second.Figure, PosTurn.X, PosTurn.Y);
                            pbField.Invalidate();

                            if (CheckOfEndTheGame() == true)
                                throw new ApplicationException("Game Over");
                        }
                    }
                }

                //this.toolStripButtonUndo.Enabled = true;
            }
            catch (ApplicationException ex)
            {
                
                switch (ex.Message)
                {
                    case "Нельзя пойти в занятую ячейку":
                        break;
                    case "Нельзя пойти дважды.":
                        break;
                    case "Нельзя пойти пустым местом":
                        break;
                    case "Game Over":
                        {
                            MessageGameOver();
                            RestartPlay();
                            return;
                        }
                    default:
                        {
                            TickTackField.Clear();
                            TickTackField.LastTurnState = CellState.Tick;
                            pbField.Invalidate();
                            break;
                        }
                }

                throw new ApplicationException(ex.Message , ex);
            }
        }

        private void TurnPlayer(Player player,  MouseEventArgs e )
        {
            if (player.Type == PlayerType.User)
            {
                TurnUser(player, e);

            }
            else if (player.Type == PlayerType.Robot)
            {

                TurnRobot(player);

            }
            else
                throw new ApplicationException("Неизвестный тип игрока [User;Robot]");
        }

        private void TurnUser(Player player, MouseEventArgs e)
        {
            Point PosTurn = CurrnetPosInGrid(e);
            TickTackField.Turn(player.Figure, PosTurn.X, PosTurn.Y); 
            pbField.Invalidate();

            if (CheckOfEndTheGame() == true)
                throw new ApplicationException("Game Over");
        }

        private void TurnRobot(Player player)
        {
            //myRobot.Init(player.Figure);
            player.Robot.Init(player.Figure);

            int xr, yr;
            DateTime dtStart = DateTime.Now;
            //myRobot.Turn(this.TickTackField, out xr, out yr);
            player.Robot.Turn(this.TickTackField, out xr, out yr);
            DateTime dtEnd = DateTime.Now;

            TimeSpan ts = dtEnd - dtStart;

            TickTackField.Turn(player.Figure, xr, yr);

            statusLbl.Text = ts.ToString();
            pbField.Invalidate();

            if (CheckOfEndTheGame() == true)
                throw new ApplicationException("Game Over");
        }

        private bool CheckOfEndTheGame()
        {

            if (this.IsSinglePlayer == true || this.IsNetRemotingGame ==  true)
            {
                if (TickTackField.IsEnd())
                    return true;
                else
                    return false; 
            }
            else if (this.IsWcfGame)
            {
                if (FieldWCF.IsEnd())
                    return true;
                else
                    return false;
            }
            else if (this.isAspNetGame)
            {
                if (FieldASP.IsEnd())
                    return true;
                else
                    return false;
            }
            return false;
            
        }

        private Point CurrnetPosInGrid(MouseEventArgs e)
        {
            Point CurPos = new Point();

            int x = e.Location.X - (this.pbField.Width / 2 - CellWidth / 2) - BaseX;
            int y = e.Location.Y - (this.pbField.Height / 2 - CellWidth / 2) - BaseY;

            CurPos.X = x / CellWidth;
            CurPos.Y = y / CellWidth;

            if (x < 0)
                CurPos.X -= 1;
            if (y < 0)
                CurPos.Y -= 1;
            return CurPos;
        }

        #endregion


        #region UndoTurn
        private void toolStripButtonUndo_Click(object sender, EventArgs e)
        {
            UndoTurn();

        }

        private void UndoTurn()
        {
            try
            {
                TickTackField.Cancel(1);
                pbField.Invalidate();
                if (TickTackField.Count < 2)
                    this.toolStripButtonUndo.Enabled = false;
            }
            catch (ApplicationException ex)
            {
                MessageBox.Show(ex.Message, "Не могу отменить последний ход");
            }
        }

        private void toolStripButtonRestart_Click(object sender, EventArgs e)
        {
            RestartPlay();
        }

        private void RestartPlay()
        {
            timerTurn.Enabled = false;
            TickTackField.Clear();

            this.BaseX = 0;
            this.BaseY = 0;

            pbField.Invalidate();

            if (this.IsSinglePlayer == true)
            {
                this.tsChkPause.Checked = false;

                this.Hide();
                wndStartSinglePlay.ShowDialog();
                StartParam = new GameParam(wndStartSinglePlay.PlayersParam);
                this.Show();

                TickTackField.LastTurnState = StartParam.Second.Figure;
            }
            else if ( this.IsNetworkGame == true ) 
            {
                if (this.IsNetRemotingGame == true)
                {
                    this.remoteObj.RemoteParam.Order = (this.OurOrderNetworkPlay == NetworkOrder.ServerOrder) ? NetworkOrder.ClientOrder : NetworkOrder.ServerOrder; 
                }
                else if (this.IsWcfGame == true)
                {
                    this.FieldWCF.Clear();
                    pbField.Invalidate();
                }
                else if (this.isAspNetGame == true)
                {
                    this.FieldASP.Clear();
                    pbField.Invalidate();
                } 
            }
            

            timerTurn.Enabled = true;
        }
        #endregion
        #region MoveInTheGrid

        private void toolStripMoveLeft_Click(object sender, EventArgs e)
        {
            this.BaseX += 10;
            pbField.Invalidate();
        }

        private void toolStripButtonMoveRight_Click(object sender, EventArgs e)
        {
            this.BaseX -= 10;
            pbField.Invalidate();
        }

        private void toolStripButtonMoveCenter_Click(object sender, EventArgs e)
        {
            this.BaseX = 0;
            this.BaseY = 0;
            pbField.Invalidate();
        }

        private void toolStripButtonMoveUp_Click(object sender, EventArgs e)
        {
            this.BaseY += 10;
            pbField.Invalidate();
        }

        private void toolStripButtonMoveDown_Click(object sender, EventArgs e)
        {
            this.BaseY -= 10;
            pbField.Invalidate();
        }

        #endregion 


        private void pbField_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.BeginPoint = e.Location;
                this.IsMoveOnField = true;
            }
            //if (this.IsDebugMode == true && e.Button == MouseButtons.Right)
            //{
            //    Point CurPos = this.CurrnetPosInGrid(e);
            //    if (TickTackField.IsSet(CurPos.X, CurPos.Y))
            //    {
            //        Cell curCell = TickTackField[CurPos.X, CurPos.Y];

            //        string strDebug = string.Empty;

            //        strDebug += string.Format("Position: [{0}, {1}]\n", curCell.X, curCell.Y);
            //        strDebug += string.Format("BiMaxWeight: {0}\n" , curCell.BiMaxWeight.Direct);
            //        strDebug += string.Format(" CanWin: {0}\n", curCell.BiMaxWeight.CanWin);
            //        strDebug += string.Format(" Weight: {0}\n", curCell.BiMaxWeight.Weight);
            //        strDebug += string.Format("  CountSet: {0}\n", curCell.BiMaxWeight.CountSet);
            //        strDebug += string.Format("  CountEmpty: {0}\n", curCell.BiMaxWeight.CountEmpty);
            //        strDebug += string.Format("  CountCanStep: {0}\n", curCell.BiMaxWeight.CountCanStep);

            //        foreach (BiDirect direct in curCell.BiDir.Keys)
            //        {
            //            if (direct == curCell.BiDir[direct].Direct)
            //            {
            //                strDebug += string.Format("BiDir: {0}\n", curCell.BiDir[direct].Direct);
            //                strDebug += string.Format(" CanWin: {0}\n", curCell.BiDir[direct].CanWin);
            //                strDebug += string.Format(" Weight: {0}\n", curCell.BiDir[direct].Weight);
            //                strDebug += string.Format("  CountSet: {0}\n", curCell.BiDir[direct].CountSet);
            //                strDebug += string.Format("  CountEmpty: {0}\n", curCell.BiDir[direct].CountEmpty);
            //                strDebug += string.Format("  CountCanStep: {0}\n", curCell.BiDir[direct].CountCanStep);

            //            }
            //            else
            //                strDebug += string.Format("Error: directKey:{0} != fieldDirect:{1}\n\n",
            //                                            direct, curCell.BiDir[direct].Direct);
            //        }
            //        toolTipDebug.Show(strDebug, this, new Point(e.X + 100, e.Y - 200));
            //    }
            //}
           
        }

        private void pbField_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.EndPoint = e.Location;

                this.BaseX -= this.DifX;
                this.BaseY -= this.DifY;
                
                this.DifX = EndPoint.X - BeginPoint.X;
                this.DifY = EndPoint.Y - BeginPoint.Y;

                this.BaseX += this.DifX;
                this.BaseY += this.DifY;

                this.IsMoveOnField = false;
                this.DifX = 0;
                this.DifY = 0;

                pbField.Invalidate();
            }

            toolTipDebug.Hide(this);

        }

        private void timerTurn_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.IsSinglePlayer == true)
                {
                    if (StartParam.Order == OrderPlay.FirstPlayer)
                    {
                        if (this.tsChkPause.Checked == false && StartParam.First.Type == PlayerType.Robot)
                        {
                            TurnRobot(StartParam.First);
                            StartParam.Order = OrderPlay.SecondPlayer;
                        }
                        else if (this.StartParam.First.Type == PlayerType.User)
                        {
                            this.IsUserCanStep = true;

                        }
                    }
                    else if (StartParam.Order == OrderPlay.SecondPlayer)
                    {
                        if (this.tsChkPause.Checked == false && StartParam.Second.Type == PlayerType.Robot)
                        {
                            TurnRobot(StartParam.Second);
                            StartParam.Order = OrderPlay.FirstPlayer;
                        }
                        else if (StartParam.Second.Type == PlayerType.User)
                        {
                            this.IsUserCanStep = true;
                        }
                    }
                }
                else if (this.IsNetworkGame == true)
                {
                    if (this.IsNetRemotingGame == true)
                    {
                        pbField.Invalidate();
                        if (this.remoteObj.RemoteParam.Order == this.OurOrderNetworkPlay)
                        {
                            if (this.PlayerNetType == PlayerType.User)
                                this.IsUserCanStep = true;
                            else if (this.PlayerNetType == PlayerType.Robot)
                                TurnNetworkRobot();

                            this.statusLbl.Text = "Ожидание хода игрока";
                        }
                        else
                        {
                            this.IsUserCanStep = false;

                            this.statusLbl.Text = "Ожидание хода соперника";
                        }
                    }
                    else if (this.IsWcfGame == true)
                    {
                        pbField.Invalidate();
                        if (this.FieldWCF.GetCountConnected() == 2)
                        {
                            if (this.MyWCFOrder == OrderPlay.FirstPlayer)
                            {
                                if (this.ParamWcfGame.First.Figure != this.FieldWCF.GetLastTurnState())
                                {
                                    this.IsUserCanStep = true;
                                    this.statusLbl.Text = "Ожидание хода игрока";
                                }
                                else
                                {
                                    this.IsUserCanStep = false;
                                    this.statusLbl.Text = "Ожидание хода соперника";
                                }
                            }
                            else
                            {
                                if (this.ParamWcfGame.Second.Figure != this.FieldWCF.GetLastTurnState())
                                {
                                    this.IsUserCanStep = true;
                                    this.statusLbl.Text = "Ожидание хода игрока";
                                }
                                else
                                {
                                    this.IsUserCanStep = false;
                                    this.statusLbl.Text = "Ожидание хода соперника";
                                }
                            }
                        }
                    }
                    else if (this.isAspNetGame == true)
                    {
                        pbField.Invalidate();
                        if (this.FieldASP.GetCountConnected() == 2)
                        {
                            if (this.MyASPOrder == OrderPlay.FirstPlayer)
                            {
                                if (this.ParamAspGame.First.Figure != this.FieldASP.GetLastTurnState())
                                {
                                    this.IsUserCanStep = true;
                                    this.statusLbl.Text = "Ожидание хода игрока";
                                }
                                else
                                {
                                    this.IsUserCanStep = false;
                                    this.statusLbl.Text = "Ожидание хода соперника";
                                }
                            }
                            else
                            {
                                if (this.ParamAspGame.Second.Figure != this.FieldASP.GetLastTurnState())
                                {
                                    this.IsUserCanStep = true;
                                    this.statusLbl.Text = "Ожидание хода игрока";
                                }
                                else
                                {
                                    this.IsUserCanStep = false;
                                    this.statusLbl.Text = "Ожидание хода соперника";
                                }
                            }
                        }
                    }
                }
            }
            catch (RemotingException ex)
            {
                MessageBox.Show(string.Format("Message: {0}", ex.Message ), "Remoting Exception");
            }
            catch (ApplicationException ex)
            {
                if (ex.Message == "Game Over")
                {
                    MessageGameOver();
                    RestartPlay();
                }
            }
            catch (Exception ex)
            {
                timerTurn.Enabled = false;
                MessageBox.Show(ex.Message, "Ошибка");
                
            }
        }

        private void TurnNetworkRobot()
        {
            if (this.OurOrderNetworkPlay == NetworkOrder.ServerOrder)
            {
                TurnRobot(this.remoteObj.RemoteParam.ServerPlayer);
                this.remoteObj.RemoteParam.Order = NetworkOrder.ClientOrder;
            }
            else
            {
                TurnRobot(this.remoteObj.RemoteParam.ClientPlayer);
                this.remoteObj.RemoteParam.Order = NetworkOrder.ServerOrder;
            }
        }

        private void MessageGameOver()
        {
            string msgWin = string.Empty;
            if (this.IsSinglePlayer == true)
            {
                if (StartParam.Order == OrderPlay.FirstPlayer)
                {
                    msgWin += "Игрок 1 победил";
                    if (StartParam.First.Figure == CellState.Tack)
                        msgWin += " (Нолики)";
                    else if (StartParam.First.Figure == CellState.Tick)
                        msgWin += " (Крестики)";
                }
                else if (StartParam.Order == OrderPlay.SecondPlayer)
                {
                    msgWin += "Игрок 2 победил";
                    if (StartParam.Second.Figure == CellState.Tack)
                        msgWin += " (Нолики)";
                    else if (StartParam.Second.Figure == CellState.Tick)
                        msgWin += " (Крестики)";
                }
            }
            else if ( this.IsNetworkGame == true ) 
            {
                if (this.IsNetRemotingGame == true)
                {
                    if (this.remoteObj.RemoteParam.Order == this.OurOrderNetworkPlay)
                    {
                        msgWin = "Вы победили!";
                    }
                    else
                    {
                        msgWin = "Вы проиграли";
                    } 
                }
                else if (this.IsWcfGame == true)
                {
                    msgWin = "Вы победили";
                }
                else if (this.isAspNetGame == true)
                {
                    msgWin = "Вы победили";
                }
            }
            MessageBox.Show(msgWin, "Игра окончена");
        }

      


        private void tsCmbSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selIndx = this.tsCmbSize.SelectedIndex;

            if (selIndx == 0)
            {
                this.CellWidth = 10;
                //this.CellPen.Width = 1;
                pbField.Invalidate();
                return;
            }
            if (selIndx == 1)
            {
                this.CellWidth = 32;
                //this.CellPen.Width = 2;
                pbField.Invalidate();
                return;
            }
            if (selIndx == 2)
            {
                this.CellWidth = 50;
                //this.CellPen.Width = 3;
                pbField.Invalidate();
                return;
            }
            if (selIndx == 3)
            {
                this.CellWidth = 80;
                //this.CellPen.Width = 4;
                pbField.Invalidate();
                return;
            }
            if (selIndx == 4)
            {
                this.CellWidth = 130;
                //this.CellPen.Width = 8;
                pbField.Invalidate();
                return;
            }
            this.CellWidth = 32;
            return;
        }

        private void timerWaitWCFPlayer_Tick(object sender, EventArgs e)
        {

        }





        




    }
}
