﻿namespace ASTU.DotNet.TickTackToe.FormProg
{
    partial class frmConnectToNetworkGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpTypePlayer = new System.Windows.Forms.GroupBox();
            this.rdbClientrRobot = new System.Windows.Forms.RadioButton();
            this.rdbClientUser = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lstPlayers = new System.Windows.Forms.ListBox();
            this.btnReady = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.timerCheckBeginGame = new System.Windows.Forms.Timer(this.components);
            this.grpTypePlayer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpTypePlayer
            // 
            this.grpTypePlayer.Controls.Add(this.rdbClientrRobot);
            this.grpTypePlayer.Controls.Add(this.rdbClientUser);
            this.grpTypePlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpTypePlayer.Location = new System.Drawing.Point(18, 164);
            this.grpTypePlayer.Name = "grpTypePlayer";
            this.grpTypePlayer.Size = new System.Drawing.Size(161, 94);
            this.grpTypePlayer.TabIndex = 9;
            this.grpTypePlayer.TabStop = false;
            this.grpTypePlayer.Text = "Игрок";
            // 
            // rdbClientrRobot
            // 
            this.rdbClientrRobot.AutoSize = true;
            this.rdbClientrRobot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbClientrRobot.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.robot_icon_16x16;
            this.rdbClientrRobot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbClientrRobot.Location = new System.Drawing.Point(6, 55);
            this.rdbClientrRobot.Name = "rdbClientrRobot";
            this.rdbClientrRobot.Size = new System.Drawing.Size(89, 24);
            this.rdbClientrRobot.TabIndex = 0;
            this.rdbClientrRobot.Text = "Робот";
            this.rdbClientrRobot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbClientrRobot.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbClientrRobot.UseVisualStyleBackColor = true;
            // 
            // rdbClientUser
            // 
            this.rdbClientUser.AutoSize = true;
            this.rdbClientUser.Checked = true;
            this.rdbClientUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbClientUser.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.User_icon;
            this.rdbClientUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbClientUser.Location = new System.Drawing.Point(6, 25);
            this.rdbClientUser.Name = "rdbClientUser";
            this.rdbClientUser.Size = new System.Drawing.Size(113, 24);
            this.rdbClientUser.TabIndex = 0;
            this.rdbClientUser.TabStop = true;
            this.rdbClientUser.Text = "Человек ";
            this.rdbClientUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbClientUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbClientUser.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(14, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Имя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(190, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Порт";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtName.Location = new System.Drawing.Point(102, 10);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(218, 26);
            this.txtName.TabIndex = 5;
            this.txtName.Text = "RemoteField.rem";
            // 
            // txtPort
            // 
            this.txtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPort.Location = new System.Drawing.Point(244, 42);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(76, 26);
            this.txtPort.TabIndex = 6;
            this.txtPort.Text = "32469";
            // 
            // txtIP
            // 
            this.txtIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIP.Location = new System.Drawing.Point(102, 42);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(82, 26);
            this.txtIP.TabIndex = 5;
            this.txtIP.Text = "127.0.0.1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(14, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "IP адресс";
            // 
            // lstPlayers
            // 
            this.lstPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lstPlayers.FormattingEnabled = true;
            this.lstPlayers.ItemHeight = 20;
            this.lstPlayers.Location = new System.Drawing.Point(18, 74);
            this.lstPlayers.Name = "lstPlayers";
            this.lstPlayers.Size = new System.Drawing.Size(302, 84);
            this.lstPlayers.TabIndex = 11;
            // 
            // btnReady
            // 
            this.btnReady.Enabled = false;
            this.btnReady.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnReady.Location = new System.Drawing.Point(185, 213);
            this.btnReady.Name = "btnReady";
            this.btnReady.Size = new System.Drawing.Size(135, 45);
            this.btnReady.TabIndex = 3;
            this.btnReady.Text = "Готов";
            this.btnReady.UseVisualStyleBackColor = true;
            this.btnReady.Click += new System.EventHandler(this.btnReady_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnConnect.Location = new System.Drawing.Point(185, 164);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(135, 45);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "Подключиться";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // timerCheckBeginGame
            // 
            this.timerCheckBeginGame.Tick += new System.EventHandler(this.timerCheckBeginGame_Tick);
            // 
            // frmConnectToNetworkGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 275);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.btnReady);
            this.Controls.Add(this.lstPlayers);
            this.Controls.Add(this.grpTypePlayer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIP);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtPort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmConnectToNetworkGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подключиться к игре";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmConnectToNetworkGame_FormClosed);
            this.grpTypePlayer.ResumeLayout(false);
            this.grpTypePlayer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpTypePlayer;
        private System.Windows.Forms.RadioButton rdbClientrRobot;
        private System.Windows.Forms.RadioButton rdbClientUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstPlayers;
        private System.Windows.Forms.Button btnReady;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Timer timerCheckBeginGame;
    }
}