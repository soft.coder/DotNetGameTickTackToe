﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using ASTU.DotNet.TickTackToe.NetRemoting;
using ASTU.DotNet.TickTackToe.GameParameter;

using System.Collections;

namespace ASTU.DotNet.TickTackToe.FormProg
{
    public partial class frmConnectToNetworkGame : Form
    {

        public NetRemObj RemObjClient { get; set; }
        public Player ClientPlayer { get; set; }


        public frmConnectToNetworkGame()
        {
            InitializeComponent();
            this.ClientPlayer = new Player();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {

            BinaryServerFormatterSinkProvider serverProv = new BinaryServerFormatterSinkProvider();
            serverProv.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

            BinaryClientFormatterSinkProvider clientProv = new BinaryClientFormatterSinkProvider();

            IDictionary props = new Hashtable();
            int port = int.Parse(txtPort.Text);
            props["port"] = 0;

            TcpChannel chan = new TcpChannel(props, clientProv, serverProv);
            ChannelServices.RegisterChannel(chan);


            //int port = int.Parse(txtPort.Text);
            //TcpChannel chan = new TcpChannel();
            //ChannelServices.RegisterChannel(chan);

            string nameRemoteField = txtName.Text;
            IPAddress ipServer = IPAddress.Parse(txtIP.Text);
            string strUrlField = string.Format("tcp://{0}:{1}/{2}", ipServer, port, nameRemoteField);

            object remoteObj = Activator.GetObject(typeof(ASTU.DotNet.TickTackToe.NetRemoting.NetRemObj), strUrlField);

            this.RemObjClient = (NetRemObj)remoteObj;

            ClientPlayerInit();

            
            //this.RemObjClient.ConnectedTwoPlayers += new NetRemObj.NetRemoteObjEventHandler(RemObjClient_ConnectedTwoPlayers);
            this.RemObjClient.Connected(this.ClientPlayer);

            DisableControls();

            ListInfoInit();

            timerCheckBeginGame.Enabled = true;
           
        }

        private void ListInfoInit()
        {
            string clientInfo, serverInfo;
            clientInfo = string.Format("Client: Type: {0}, Figure: {1}", 
                                        this.RemObjClient.RemoteParam.ClientPlayer.Type, 
                                        this.RemObjClient.RemoteParam.ClientPlayer.Figure);
            serverInfo = string.Format("Server: Type: {0}, Figure: {1}", 
                                        this.RemObjClient.RemoteParam.ServerPlayer.Type, 
                                        this.RemObjClient.RemoteParam.ServerPlayer.Figure);
            
            lstPlayers.Items.Add(clientInfo);
            lstPlayers.Items.Add(serverInfo);
        }



        private void DisableControls()
        {
            this.grpTypePlayer.Enabled = false;
            this.btnReady.Enabled = true;
            this.btnConnect.Enabled = false;
        }

        private void ClientPlayerInit()
        {
            if (this.rdbClientUser.Checked == true)
                this.ClientPlayer.Type = PlayerType.User;
            else
                this.ClientPlayer.Type = PlayerType.Robot;

            this.ClientPlayer.Figure = CellState.Tick;
        }

        private void btnReady_Click(object sender, EventArgs e)
        {
            this.RemObjClient.RemoteParam.ClientIsReady = true;
            this.btnReady.Enabled = false;
        }

        private void frmConnectToNetworkGame_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void timerCheckBeginGame_Tick(object sender, EventArgs e)
        {
            if ( this.RemObjClient.IsGameBegin == true )
            {
                this.Hide();
            }
        }
    }
}
