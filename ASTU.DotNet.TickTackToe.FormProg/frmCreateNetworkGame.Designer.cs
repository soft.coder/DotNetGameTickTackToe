﻿namespace ASTU.DotNet.TickTackToe.FormProg
{
    partial class frmCreateNetworkGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grpFigure = new System.Windows.Forms.GroupBox();
            this.rdbCircleFigure = new System.Windows.Forms.RadioButton();
            this.rdbCrossFigure = new System.Windows.Forms.RadioButton();
            this.grpTypePlayer = new System.Windows.Forms.GroupBox();
            this.rdbServerRobot = new System.Windows.Forms.RadioButton();
            this.rdbServerUser = new System.Windows.Forms.RadioButton();
            this.lstPlayers = new System.Windows.Forms.ListBox();
            this.btnBeginGame = new System.Windows.Forms.Button();
            this.grpFirstStep = new System.Windows.Forms.GroupBox();
            this.rdbFirstStepClient = new System.Windows.Forms.RadioButton();
            this.rdbFirstStepServer = new System.Windows.Forms.RadioButton();
            this.timerCheckConnection = new System.Windows.Forms.Timer(this.components);
            this.grpFigure.SuspendLayout();
            this.grpTypePlayer.SuspendLayout();
            this.grpFirstStep.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(278, 6);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(117, 26);
            this.txtPort.TabIndex = 0;
            this.txtPort.Text = "32469";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(224, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Порт";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(272, 139);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(120, 47);
            this.btnCreate.TabIndex = 2;
            this.btnCreate.Text = "Создать игру";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(58, 6);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(162, 26);
            this.txtName.TabIndex = 0;
            this.txtName.Text = "RemoteField.rem";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя";
            // 
            // grpFigure
            // 
            this.grpFigure.Controls.Add(this.rdbCircleFigure);
            this.grpFigure.Controls.Add(this.rdbCrossFigure);
            this.grpFigure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpFigure.Location = new System.Drawing.Point(141, 39);
            this.grpFigure.Name = "grpFigure";
            this.grpFigure.Size = new System.Drawing.Size(125, 94);
            this.grpFigure.TabIndex = 4;
            this.grpFigure.TabStop = false;
            this.grpFigure.Text = "Фигура";
            // 
            // rdbCircleFigure
            // 
            this.rdbCircleFigure.AutoSize = true;
            this.rdbCircleFigure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbCircleFigure.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.circle_icon_16x16;
            this.rdbCircleFigure.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbCircleFigure.Location = new System.Drawing.Point(6, 55);
            this.rdbCircleFigure.Name = "rdbCircleFigure";
            this.rdbCircleFigure.Size = new System.Drawing.Size(100, 24);
            this.rdbCircleFigure.TabIndex = 0;
            this.rdbCircleFigure.Text = "Нолики";
            this.rdbCircleFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbCircleFigure.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbCircleFigure.UseVisualStyleBackColor = true;
            // 
            // rdbCrossFigure
            // 
            this.rdbCrossFigure.AutoSize = true;
            this.rdbCrossFigure.Checked = true;
            this.rdbCrossFigure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbCrossFigure.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.cross_icon_16x16;
            this.rdbCrossFigure.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbCrossFigure.Location = new System.Drawing.Point(6, 25);
            this.rdbCrossFigure.Name = "rdbCrossFigure";
            this.rdbCrossFigure.Size = new System.Drawing.Size(114, 24);
            this.rdbCrossFigure.TabIndex = 0;
            this.rdbCrossFigure.TabStop = true;
            this.rdbCrossFigure.Text = "Крестики";
            this.rdbCrossFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbCrossFigure.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbCrossFigure.UseVisualStyleBackColor = true;
            // 
            // grpTypePlayer
            // 
            this.grpTypePlayer.Controls.Add(this.rdbServerRobot);
            this.grpTypePlayer.Controls.Add(this.rdbServerUser);
            this.grpTypePlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpTypePlayer.Location = new System.Drawing.Point(10, 38);
            this.grpTypePlayer.Name = "grpTypePlayer";
            this.grpTypePlayer.Size = new System.Drawing.Size(125, 94);
            this.grpTypePlayer.TabIndex = 3;
            this.grpTypePlayer.TabStop = false;
            this.grpTypePlayer.Text = "Игрок";
            // 
            // rdbServerRobot
            // 
            this.rdbServerRobot.AutoSize = true;
            this.rdbServerRobot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbServerRobot.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.robot_icon_16x16;
            this.rdbServerRobot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbServerRobot.Location = new System.Drawing.Point(6, 55);
            this.rdbServerRobot.Name = "rdbServerRobot";
            this.rdbServerRobot.Size = new System.Drawing.Size(89, 24);
            this.rdbServerRobot.TabIndex = 0;
            this.rdbServerRobot.Text = "Робот";
            this.rdbServerRobot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbServerRobot.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbServerRobot.UseVisualStyleBackColor = true;
            // 
            // rdbServerUser
            // 
            this.rdbServerUser.AutoSize = true;
            this.rdbServerUser.Checked = true;
            this.rdbServerUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbServerUser.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.User_icon;
            this.rdbServerUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbServerUser.Location = new System.Drawing.Point(6, 25);
            this.rdbServerUser.Name = "rdbServerUser";
            this.rdbServerUser.Size = new System.Drawing.Size(113, 24);
            this.rdbServerUser.TabIndex = 0;
            this.rdbServerUser.TabStop = true;
            this.rdbServerUser.Text = "Человек ";
            this.rdbServerUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbServerUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbServerUser.UseVisualStyleBackColor = true;
            // 
            // lstPlayers
            // 
            this.lstPlayers.FormattingEnabled = true;
            this.lstPlayers.ItemHeight = 20;
            this.lstPlayers.Location = new System.Drawing.Point(15, 139);
            this.lstPlayers.Name = "lstPlayers";
            this.lstPlayers.Size = new System.Drawing.Size(251, 104);
            this.lstPlayers.TabIndex = 5;
            // 
            // btnBeginGame
            // 
            this.btnBeginGame.Enabled = false;
            this.btnBeginGame.Location = new System.Drawing.Point(272, 192);
            this.btnBeginGame.Name = "btnBeginGame";
            this.btnBeginGame.Size = new System.Drawing.Size(120, 47);
            this.btnBeginGame.TabIndex = 2;
            this.btnBeginGame.Text = "Начать игру";
            this.btnBeginGame.UseVisualStyleBackColor = true;
            this.btnBeginGame.Click += new System.EventHandler(this.btnBeginGame_Click);
            // 
            // grpFirstStep
            // 
            this.grpFirstStep.Controls.Add(this.rdbFirstStepClient);
            this.grpFirstStep.Controls.Add(this.rdbFirstStepServer);
            this.grpFirstStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpFirstStep.Location = new System.Drawing.Point(272, 39);
            this.grpFirstStep.Name = "grpFirstStep";
            this.grpFirstStep.Size = new System.Drawing.Size(125, 94);
            this.grpFirstStep.TabIndex = 4;
            this.grpFirstStep.TabStop = false;
            this.grpFirstStep.Text = "Первый ход";
            // 
            // rdbFirstStepClient
            // 
            this.rdbFirstStepClient.AutoSize = true;
            this.rdbFirstStepClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbFirstStepClient.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.remote_16x16;
            this.rdbFirstStepClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbFirstStepClient.Location = new System.Drawing.Point(6, 55);
            this.rdbFirstStepClient.Name = "rdbFirstStepClient";
            this.rdbFirstStepClient.Size = new System.Drawing.Size(99, 24);
            this.rdbFirstStepClient.TabIndex = 0;
            this.rdbFirstStepClient.Text = "Клиент";
            this.rdbFirstStepClient.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbFirstStepClient.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbFirstStepClient.UseVisualStyleBackColor = true;
            // 
            // rdbFirstStepServer
            // 
            this.rdbFirstStepServer.AutoSize = true;
            this.rdbFirstStepServer.Checked = true;
            this.rdbFirstStepServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rdbFirstStepServer.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.home_16x16;
            this.rdbFirstStepServer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rdbFirstStepServer.Location = new System.Drawing.Point(6, 25);
            this.rdbFirstStepServer.Name = "rdbFirstStepServer";
            this.rdbFirstStepServer.Size = new System.Drawing.Size(99, 24);
            this.rdbFirstStepServer.TabIndex = 0;
            this.rdbFirstStepServer.TabStop = true;
            this.rdbFirstStepServer.Text = "Сервер";
            this.rdbFirstStepServer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdbFirstStepServer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbFirstStepServer.UseVisualStyleBackColor = true;
            // 
            // timerCheckConnection
            // 
            this.timerCheckConnection.Interval = 500;
            this.timerCheckConnection.Tick += new System.EventHandler(this.timerCheckConnection_Tick);
            // 
            // frmCreateNetworkGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 258);
            this.Controls.Add(this.lstPlayers);
            this.Controls.Add(this.grpFirstStep);
            this.Controls.Add(this.grpFigure);
            this.Controls.Add(this.grpTypePlayer);
            this.Controls.Add(this.btnBeginGame);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtPort);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCreateNetworkGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Создать игру";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmCreateNetworkGame_FormClosed);
            this.grpFigure.ResumeLayout(false);
            this.grpFigure.PerformLayout();
            this.grpTypePlayer.ResumeLayout(false);
            this.grpTypePlayer.PerformLayout();
            this.grpFirstStep.ResumeLayout(false);
            this.grpFirstStep.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpFigure;
        private System.Windows.Forms.RadioButton rdbCircleFigure;
        private System.Windows.Forms.RadioButton rdbCrossFigure;
        private System.Windows.Forms.GroupBox grpTypePlayer;
        private System.Windows.Forms.RadioButton rdbServerRobot;
        private System.Windows.Forms.RadioButton rdbServerUser;
        private System.Windows.Forms.ListBox lstPlayers;
        private System.Windows.Forms.Button btnBeginGame;
        private System.Windows.Forms.GroupBox grpFirstStep;
        private System.Windows.Forms.RadioButton rdbFirstStepClient;
        private System.Windows.Forms.RadioButton rdbFirstStepServer;
        private System.Windows.Forms.Timer timerCheckConnection;

    }
}