﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV;

using ASTU.DotNet.TickTackToe.GameParameter;


namespace ASTU.DotNet.TickTackToe.FormProg
{
  
    public partial class frmStartSinglePlay : Form
    {

        public IGameParam PlayersParam { get; set; } 


        public frmStartSinglePlay()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                PlayerType PlayerOneType , PlayerTwoType ;
                CellState FirstStepFigure ,SecondStepFigure;

                if (rdbFirstStepCircle.Checked == true){
                    FirstStepFigure = CellState.Tack;
                    SecondStepFigure = CellState.Tick;
                }
                else {
                    FirstStepFigure = CellState.Tick;
                    SecondStepFigure = CellState.Tack;
                }

                if (rdbСrossUser.Checked == true)
                    PlayerOneType = PlayerType.User;
                else 
                    PlayerOneType = PlayerType.Robot;

                if (rdbCircleUser.Checked == true)
                    PlayerTwoType = PlayerType.User;
                else 
                    PlayerTwoType = PlayerType.Robot;

                this.PlayersParam = new GameParam(new Player(PlayerOneType, FirstStepFigure),
                                                     new Player(PlayerTwoType, SecondStepFigure));

                this.Hide();

            }
            catch (ApplicationException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка приложения");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Непредвиденная ошибка");
            }
        }
        private void btnLoadRobotPlayerOne_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = openFileDialog.FileName;
                PlayersParam.First.Robot =  LoadExternRobotAI(path);
            }
        }

        private void btnLoadRobotPlayerTwo_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = openFileDialog.FileName;
                Assembly asmRobot = null;
                try
                {
                    asmRobot = Assembly.LoadFrom(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Type RobotType = (Type)(from r in asmRobot.GetTypes()
                                        where r.IsClass && r.GetInterface("IRobot") != null
                                        select r).First();
                IRobot robot = (IRobot)asmRobot.CreateInstance(RobotType.FullName, true);
                PlayersParam.Second.Robot = robot;
            }
        }
        public static IRobot LoadExternRobotAI(string path)
        {
            Assembly asmRobot = null;
            try
            {
                asmRobot = Assembly.LoadFrom(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Type RobotType = (Type)(from r in asmRobot.GetTypes()
                                    where r.IsClass && r.GetInterface("IRobot") != null
                                    select r).First();
            IRobot robot = (IRobot)asmRobot.CreateInstance(RobotType.FullName);
            return robot;
        }
    }
    //public enum PlayerType
    //{
    //    User,
    //    Robot
    //}
    //public enum OrderPlay
    //{
    //    FirstPlayer,
    //    SecondPlayer
    //}

    //public class Player
    //{
    //    public PlayerType Type { get; set; }
    //    public CellState Figure { get; set; }
    //    public IRobot Robot { get; set; }
        
    //    public Player()
    //    {
    //        this.Robot = new Robot.CherednichenkoSV.Robot();
    //    }
    //    public Player(PlayerType type, CellState figure): this()
    //    {
    //        this.Type = type;
    //        this.Figure = figure;            
    //    }
    //    public Player(Player player): this(player.Type , player.Figure )
    //    { }
    //}
    //public class GameParameters
    //{
    //    public Player First { get; set; }
    //    public Player Second { get; set; }

    //    public OrderPlay Order { get; set; } 

    //    public GameParameters()
    //    { First = new Player(); Second = new Player(); Order = OrderPlay.FirstPlayer; }

    //    public GameParameters(Player first, Player second)
    //    {
    //        this.Order = OrderPlay.FirstPlayer;
    //        this.First = new Player(first);
    //        this.Second = new Player(second);
    //    }
    //    public GameParameters(GameParameters game) : this(game.First , game.Second)
    //    { }
    //}

}
