﻿namespace ASTU.DotNet.TickTackToe.FormProg
{
    partial class frmMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripMoveLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMoveRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonMoveCenter = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonMoveUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMoveDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripChkBtnDebugMode = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRestart = new System.Windows.Forms.ToolStripButton();
            this.tsChkPause = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsCmbSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolTipDebug = new System.Windows.Forms.ToolTip(this.components);
            this.pbField = new System.Windows.Forms.PictureBox();
            this.timerTurn = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbField)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLbl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(750, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLbl
            // 
            this.statusLbl.Name = "statusLbl";
            this.statusLbl.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMoveLeft,
            this.toolStripButtonMoveRight,
            this.toolStripSeparator1,
            this.toolStripButtonMoveCenter,
            this.toolStripSeparator2,
            this.toolStripButtonMoveUp,
            this.toolStripButtonMoveDown,
            this.toolStripSeparator3,
            this.toolStripButtonUndo,
            this.toolStripSeparator4,
            this.toolStripChkBtnDebugMode,
            this.toolStripSeparator5,
            this.toolStripButtonRestart,
            this.tsChkPause,
            this.toolStripSeparator6,
            this.tsCmbSize});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(750, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripMoveLeft
            // 
            this.toolStripMoveLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripMoveLeft.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Arrow_Left_32x32;
            this.toolStripMoveLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripMoveLeft.Name = "toolStripMoveLeft";
            this.toolStripMoveLeft.Size = new System.Drawing.Size(23, 22);
            this.toolStripMoveLeft.Text = "MoveLeft";
            this.toolStripMoveLeft.Visible = false;
            this.toolStripMoveLeft.Click += new System.EventHandler(this.toolStripMoveLeft_Click);
            // 
            // toolStripButtonMoveRight
            // 
            this.toolStripButtonMoveRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoveRight.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Arrow_Right_32;
            this.toolStripButtonMoveRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoveRight.Name = "toolStripButtonMoveRight";
            this.toolStripButtonMoveRight.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonMoveRight.Text = "MoveRight";
            this.toolStripButtonMoveRight.Visible = false;
            this.toolStripButtonMoveRight.Click += new System.EventHandler(this.toolStripButtonMoveRight_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator1.Visible = false;
            // 
            // toolStripButtonMoveCenter
            // 
            this.toolStripButtonMoveCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoveCenter.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.cross_32x32;
            this.toolStripButtonMoveCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoveCenter.Name = "toolStripButtonMoveCenter";
            this.toolStripButtonMoveCenter.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonMoveCenter.Text = "MoveCenter";
            this.toolStripButtonMoveCenter.Click += new System.EventHandler(this.toolStripButtonMoveCenter_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator2.Visible = false;
            // 
            // toolStripButtonMoveUp
            // 
            this.toolStripButtonMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoveUp.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Arrow_Up_32x32;
            this.toolStripButtonMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoveUp.Name = "toolStripButtonMoveUp";
            this.toolStripButtonMoveUp.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonMoveUp.Text = "MoveUp";
            this.toolStripButtonMoveUp.Visible = false;
            this.toolStripButtonMoveUp.Click += new System.EventHandler(this.toolStripButtonMoveUp_Click);
            // 
            // toolStripButtonMoveDown
            // 
            this.toolStripButtonMoveDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoveDown.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Arrow_Down_32x32;
            this.toolStripButtonMoveDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoveDown.Name = "toolStripButtonMoveDown";
            this.toolStripButtonMoveDown.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonMoveDown.Text = "MoveDown";
            this.toolStripButtonMoveDown.Visible = false;
            this.toolStripButtonMoveDown.Click += new System.EventHandler(this.toolStripButtonMoveDown_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator3.Visible = false;
            // 
            // toolStripButtonUndo
            // 
            this.toolStripButtonUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUndo.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.undo_icon_16x16;
            this.toolStripButtonUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUndo.Name = "toolStripButtonUndo";
            this.toolStripButtonUndo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUndo.Text = "Undo";
            this.toolStripButtonUndo.Visible = false;
            this.toolStripButtonUndo.Click += new System.EventHandler(this.toolStripButtonUndo_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripChkBtnDebugMode
            // 
            this.toolStripChkBtnDebugMode.Checked = true;
            this.toolStripChkBtnDebugMode.CheckOnClick = true;
            this.toolStripChkBtnDebugMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripChkBtnDebugMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripChkBtnDebugMode.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.debug_16x16;
            this.toolStripChkBtnDebugMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripChkBtnDebugMode.Name = "toolStripChkBtnDebugMode";
            this.toolStripChkBtnDebugMode.Size = new System.Drawing.Size(23, 22);
            this.toolStripChkBtnDebugMode.Text = "DebugMode";
            this.toolStripChkBtnDebugMode.CheckStateChanged += new System.EventHandler(this.toolStripChkBtnDebugMode_CheckStateChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRestart
            // 
            this.toolStripButtonRestart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRestart.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Windows_Restart_icon_16x16;
            this.toolStripButtonRestart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRestart.Name = "toolStripButtonRestart";
            this.toolStripButtonRestart.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRestart.Text = "RestartButton";
            this.toolStripButtonRestart.Click += new System.EventHandler(this.toolStripButtonRestart_Click);
            // 
            // tsChkPause
            // 
            this.tsChkPause.CheckOnClick = true;
            this.tsChkPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsChkPause.Image = global::ASTU.DotNet.TickTackToe.FormProg.Properties.Resources.Pause_Hot_icon_16x16;
            this.tsChkPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsChkPause.Name = "tsChkPause";
            this.tsChkPause.Size = new System.Drawing.Size(23, 22);
            this.tsChkPause.Text = "Pause";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tsCmbSize
            // 
            this.tsCmbSize.AutoCompleteCustomSource.AddRange(new string[] {
            "10x10",
            "30x30",
            "50x50",
            "80x80",
            "130x130"});
            this.tsCmbSize.Items.AddRange(new object[] {
            "10 X 10",
            "32 X 32",
            "50 X 50",
            "80 X 80",
            "130 X 130"});
            this.tsCmbSize.Name = "tsCmbSize";
            this.tsCmbSize.Size = new System.Drawing.Size(75, 25);
            this.tsCmbSize.Visible = false;
            this.tsCmbSize.SelectedIndexChanged += new System.EventHandler(this.tsCmbSize_SelectedIndexChanged);
            // 
            // toolTipDebug
            // 
            this.toolTipDebug.ToolTipTitle = "Debug";
            // 
            // pbField
            // 
            this.pbField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbField.BackColor = System.Drawing.Color.White;
            this.pbField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbField.Cursor = System.Windows.Forms.Cursors.Default;
            this.pbField.Location = new System.Drawing.Point(12, 35);
            this.pbField.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.pbField.Name = "pbField";
            this.pbField.Size = new System.Drawing.Size(726, 383);
            this.pbField.TabIndex = 0;
            this.pbField.TabStop = false;
            this.pbField.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxField_Paint);
            this.pbField.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbField_MouseClick);
            this.pbField.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbField_MouseDown);
            this.pbField.MouseLeave += new System.EventHandler(this.pbField_MouseLeave);
            this.pbField.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbField_MouseMove);
            this.pbField.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbField_MouseUp);
            // 
            // timerTurn
            // 
            this.timerTurn.Tick += new System.EventHandler(this.timerTurn_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 450);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pbField);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TickTack";
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbField;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLbl;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripMoveLeft;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoveRight;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoveCenter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoveUp;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoveDown;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripChkBtnDebugMode;
        private System.Windows.Forms.ToolTip toolTipDebug;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonRestart;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tsChkPause;
        private System.Windows.Forms.Timer timerTurn;
        private System.Windows.Forms.ToolStripComboBox tsCmbSize;
    }
}

