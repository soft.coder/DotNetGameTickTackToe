﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ASTU.DotNet.TickTackToe.FormProg
{
    public partial class frmStartNetworkGame : Form
    {
        public bool IsCreatedGame { get; set; }
        public frmStartNetworkGame()
        {
            InitializeComponent();
        }

        private void btnCreateGame_Click(object sender, EventArgs e)
        {
            this.IsCreatedGame = true;
            this.Hide();
        }

        private void btnConnectGame_Click(object sender, EventArgs e)
        {
            this.IsCreatedGame = false;
            this.Hide();
        }
    }
}
