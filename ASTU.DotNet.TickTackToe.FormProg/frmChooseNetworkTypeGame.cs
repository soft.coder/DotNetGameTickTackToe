﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ASTU.DotNet.TickTackToe.FormProg
{
    public partial class frmChooseNetworkTypeGame : Form
    {
        public bool IsNetRemtingGame { get; set; }
        public bool IsWcfGame { get; set; }
        public bool IsAspNetGame { get; set; } 

        public frmChooseNetworkTypeGame()
        {
            InitializeComponent();
        }

        private void btnNetRemoting_Click(object sender, EventArgs e)
        {
            this.IsNetRemtingGame = true;
            this.Hide();
        }

        private void btnWCF_Click(object sender, EventArgs e)
        {
            this.IsWcfGame = true;
            this.Hide();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.IsAspNetGame = true;
            this.Hide();
        }
            
    }
}
