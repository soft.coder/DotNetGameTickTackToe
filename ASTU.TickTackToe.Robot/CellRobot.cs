﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe;

namespace ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV
{
    public class CellRobot : Cell
    {
        
        #region PropBiDirect

        public Dictionary<BiDirect, BiDirectInfo> BiDir { get; set; }

        public BiDirectInfo BiMaxWeight
        {
            get
            {
                BiDirectInfo maxBiDir = new BiDirectInfo(); ;

                foreach (BiDirect dir in BiDir.Keys)
                {
                    Random r = new Random();                    
                    if (this.BiDir[dir].Weight > maxBiDir.Weight)
                        maxBiDir = this.BiDir[dir];
                    else if ( this.BiDir[dir].Weight == maxBiDir.Weight && r.Next(0, 100) > 33 )
                        maxBiDir = this.BiDir[dir];

                }
                return maxBiDir;
            }
        }

        public int SumWeight
        {
            get
            {
                int sum = 0;
                foreach (BiDirect direct in this.BiDir.Keys)
                    sum += this.BiDir[direct].Weight;
                return sum;
            }
        }


        #endregion

        #region ExtProp

        public CellState StateEnemy
        { get{ return (this.State == CellState.Tack) ? CellState.Tick : CellState.Tack; } }

        #endregion


        #region Constructors
        public CellRobot() : base()
        { 
            this.BiDir = new Dictionary<BiDirect, BiDirectInfo>();
            foreach (string strDir in Enum.GetNames(typeof(BiDirect)))
            {
                BiDirect dir = (BiDirect)Enum.Parse(typeof(BiDirect), strDir, true);
                this.BiDir[dir] = new BiDirectInfo(dir);
                
            }
        }
        public CellRobot(int x, int y) : this()
        { this.X = x; this.Y = y; }
        public CellRobot(int x, int y, CellState state) : this(x, y)
        { this.State = state; }

        public CellRobot(Cell cell) : this(cell.X , cell.Y , cell.State)
        { }
        #endregion

        #region MethodsOfBiDrect

        public void ClearBiDirect()
        {
            foreach (BiDirect dir in this.BiDir.Keys)
                this.BiDir[dir].Clear();
        }

        #endregion

    }
}
