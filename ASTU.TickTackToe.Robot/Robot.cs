﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe;


namespace ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV
{
    [Serializable]
    public class Robot : IRobot
    {
        #region Properties
        private CellState StateTurn { get; set; }

        private bool IsCrossWin { get; set; }

        private Dictionary<CellRobot, BiDirect> bestCellToTurn = new Dictionary<CellRobot, BiDirect>();
        #endregion

        #region Constructors

        public string Name()
        {
            return "Robot Attack";
        }

        public void Init(CellState state)
        {
            StateTurn = state;
        }
        #endregion

        #region TurnRobots
        public void Turn(IField field, out int x, out int y)
        {
            if (field.Count == 0)
            {
                x = 0;
                y = 0;
                return;
            }
            CellRobot bestCellAttack = new CellRobot();
            //foreach (Cell cell in field.FieldCells.Where(c => c.State == StateTurn))
            foreach (Cell cell in field.GetMarkedCells(StateTurn))
            {
                CellRobot cellRobot = new CellRobot(cell);
                cellRobot.ClearBiDirect();

                Direction.BiWeightCellDefine(field, cellRobot);

                if (cellRobot.BiMaxWeight.Weight > bestCellAttack.BiMaxWeight.Weight)
                    bestCellAttack = cellRobot;
            }
           
            CellRobot bestCellDefence = new CellRobot();
            //foreach (Cell cell in field.FieldCells.Where(c => c.State == bestCellAttack.StateEnemy))
            foreach (Cell cell in field.GetMarkedCells(bestCellAttack.StateEnemy))
            {
                CellRobot cellRobot = new CellRobot(cell);
                cellRobot.ClearBiDirect();

                Direction.BiWeightCellDefine(field, cellRobot);

                if (cellRobot.BiMaxWeight.Weight > bestCellDefence.BiMaxWeight.Weight)
                    bestCellDefence = cellRobot;
            }
            //bestCellDefence.State = bestCellAttack.State;

            if ( bestCellAttack.BiMaxWeight.Weight > bestCellDefence.BiMaxWeight.Weight)
                TurnToTheBestDirect(field, bestCellAttack, out x, out y);
            else
                TurnDefence(field, bestCellDefence, out x, out y);
        }

        private void TurnDefence(IField field, CellRobot bestCell, out int x, out int y)
        {
            x = y = 0;

            int firstX, firstY,
                 secondX, secondY;

            Direction.SwitchBiDirect(bestCell.BiMaxWeight.Direct, out firstX, out secondX, out firstY, out secondY);

            CellState stateEnemy = (bestCell.State == CellState.Tack) ? CellState.Tick : CellState.Tack; ;

            DirectInfo firstDirect = new DirectInfo();
            DirectInfo secondDirect = new DirectInfo();


            WeightDirect(field, bestCell, firstX, firstY, firstDirect);
            WeightDirect(field, bestCell, secondX, secondY, secondDirect);

            int stepX, stepY;
            if (firstDirect.Weight > secondDirect.Weight)
            {
                stepX = firstX;
                stepY = firstY;
            }
            else
            {
                stepX = secondX;
                stepY = secondY;
            }

            for (int xs = bestCell.X + stepX,
                    ys = bestCell.Y + stepY,
                    countStep = 0;
                    countStep <= 4;
                    xs += stepX,
                    ys += stepY,
                    countStep++) 
            {
                if (field[xs, ys].State == CellState.Empty)
                {
                    x = xs;
                    y = ys;
                    return;
                }
            }




        }

        private static void WeightDirect(IField field, CellRobot bestCell, int firstX, int firstY, DirectInfo firstDirect)
        {
            for (int xd = bestCell.X + firstX,
                    yd = bestCell.Y + firstY,
                    countStep = 0;
                    countStep <= 4;
                    xd += firstX,
                    yd += firstY,
                    countStep++)
            {
                if (field[xd, yd].State == bestCell.State)
                    firstDirect.CountSet += 1;
                else if (field[xd, yd].State == CellState.Empty)
                    firstDirect.CountEmpty += 1;
                else
                    break;
            }
        }


        private void TurnToTheBestDirect(IField field, CellRobot bestCell, out int x, out int y)
        {
            x = 0; y = 0;
            int dFdX, dSdX,
                dFdY, dSdY;

            Direction.SwitchBiDirect(bestCell.BiMaxWeight.Direct, out dFdX, out dSdX, out dFdY, out dSdY);


            bool dfCanStep, dsCanStep;
            dfCanStep = dsCanStep = true;
            CellState stateEnemy = (bestCell.State == CellState.Tack) ? CellState.Tick : CellState.Tack;

            for (int xfd = bestCell.X + dFdX, xsd = bestCell.X + dSdX,
                    yfd = bestCell.Y + dFdY, ysd = bestCell.Y + dSdY,
                    countStep = 0;
                    countStep < 5;
                    xfd += dFdX, xsd += dSdX, yfd += dFdY, ysd += dSdY,
                    countStep++)
            {
                if (field[xfd, yfd].State == CellState.Empty && dfCanStep == true)
                {
                    x = xfd;
                    y = yfd;
                    return;
                }
                else if (field[xfd, yfd].State == stateEnemy)
                    dfCanStep = false;

                if (field[xsd, ysd].State == CellState.Empty && dsCanStep == true)
                {
                    x = xsd;
                    y = ysd;
                    return;
                }
                else if (field[xsd, ysd].State == stateEnemy)
                    dsCanStep = false;
            }
            throw new ApplicationException("Не найденена свободная ячейка на выбранной лучшей ячейки и ее направлении");
        }


        #endregion
    }
}
 