﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV
{
    public interface IRobot
    {
        string Name();
        void Turn(IField field, out int x, out int y);
        void Init(CellState state);
    }
}
