﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe;

namespace ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV
{
    public class Direction
    {
        public static void SwitchBiDirect(BiDirect direct, out int dFdX, out int dSdX, out int dFdY, out int dSdY)
        {
            switch (direct)
            {
                case BiDirect.LeftToRight:
                    {
                        dFdX = -1; dSdX = 1;
                        dFdY = dSdY = 0;
                        break;
                    }
                case BiDirect.TopToDown:
                    {
                        dFdX = dSdX = 0;
                        dFdY = -1; dSdY = 1;
                        break;
                    }
                case BiDirect.LeftDownToRightUp:
                    {
                        dFdX = -1; dSdX = 1;
                        dFdY = 1; dSdY = -1;
                        break;
                    }
                case BiDirect.LeftUpToRightDown:
                    {
                        dFdX = -1; dSdX = 1;
                        dFdY = -1; dSdY = 1;
                        break;
                    }
                default:
                    {
                        dFdX = dFdY = dSdX = dSdY = 0;
                        break;
                    }
            }
        }

        #region DefineBiWeightCell

        public static void BiWeightCellDefine(IField field, CellRobot cell)
        {
            UnivDefineWeightCell(field, cell, BiDirect.LeftToRight);
            UnivDefineWeightCell(field, cell, BiDirect.TopToDown);
            UnivDefineWeightCell(field, cell, BiDirect.LeftDownToRightUp);
            UnivDefineWeightCell(field, cell, BiDirect.LeftUpToRightDown);
        }

        /// <summary>
        /// Универсальный метод определения веса ячейки для всех четырех возможных направлений хода из этой ячейки
        /// (Слева - Направо; Сверху - Вниз ; Снизу слева - направо Вверх ; Сверху слева - направо Вниз ;)
        /// </summary>
        /// <param name="cell">Ячейка для которой определяются значения веса по направлениям</param>
        /// <param name="direct">Направление</param>
        private static void UnivDefineWeightCell(IField field, CellRobot cell, BiDirect direct)
        {
            int FirstX, FirstY,
                SecondX, SecondY;
            Direction.SwitchBiDirect(direct, out FirstX, out SecondX,
                                             out FirstY, out SecondY);

            cell.BiDir[direct].CountEmpty = 0;
            cell.BiDir[direct].CountSet = 0;

            for (int x = cell.X + FirstX,
                    y = cell.Y + FirstY,
                    countStep = 0;
                    field[x, y].State != cell.StateEnemy &&
                    countStep < 4;
                    x += FirstX,
                    y += FirstY,
                    countStep++)
            {
                if (field[x, y].State == cell.State)
                    cell.BiDir[direct].CountSet += 1;
                else if (field[x, y].State == CellState.Empty)
                    cell.BiDir[direct].CountEmpty += 1;
            }

            for (int x = cell.X + SecondX,
                    y = cell.Y + SecondY,
                    countStep = 0;
                    field[x, y].State != cell.StateEnemy &&
                    countStep < 4;
                    x += SecondX,
                    y += SecondY,
                    countStep++)
            {
                if (field[x, y].State == cell.State)
                    cell.BiDir[direct].CountSet += 1;
                else if (field[x, y].State == CellState.Empty)
                    cell.BiDir[direct].CountEmpty += 1;
            }
        }

        #endregion
    }



    #region ClassesBiDirection

    public enum BiDirect
    {
        LeftToRight,
        TopToDown,
        LeftDownToRightUp,
        LeftUpToRightDown
    }

    public class DirectInfo
    {
        #region Prop
        public int CountSet { get; set; }
        public int CountEmpty { get; set; }

        public int CountCanStep
        {
            get
            {
                return this.CountSet + this.CountEmpty;
            }
        }


        public int Weight
        {
            get
            {
                if (this.CanWin == true)
                {
                    return (int)Math.Pow(this.CountSet, 2) + this.CountEmpty;
                }
                else
                    return 0;
            }
        }
        public bool CanWin
        {
            get
            {
                if (this.CountCanStep >= 4)
                    return true;
                else
                    return false;
            }
        }
        #endregion

        #region Constructors
        public DirectInfo()
        { }
        public DirectInfo(int countSet, int countEmpty)
        {
            this.CountSet = countSet;
            this.CountEmpty = countEmpty;
        }
        public void Clear()
        {
            this.CountEmpty = 0;
            this.CountSet = 0;
        }
        #endregion

    }

    public class BiDirectInfo
    {

        #region Properties
        private BiDirect fieldDirect;
        public BiDirect Direct { get { return fieldDirect; } }

        public int CountSet { get; set; }
        public int CountEmpty { get; set; }

        public int CountCanStep 
        {
            get
            {
                return this.CountSet + this.CountEmpty;
            }
        }


        public int Weight
        {
            get
            {
                if (this.CanWin == true)
                {
                    return (int)Math.Pow(this.CountSet, 2) + this.CountEmpty;
                }
                else
                    return 0;
            }
        }
        public bool CanWin
        {
            get
            {
                if (this.CountCanStep >= 4)
                    return true;
                else
                    return false;
            }
        }

        #endregion

        #region Constructors
        public BiDirectInfo()
        { this.fieldDirect = BiDirect.LeftToRight; }
        public BiDirectInfo(BiDirect direct)
            : this()
        { this.fieldDirect = direct; }
        public BiDirectInfo(BiDirect direct, int countSet, int countEmpty)
            : this(direct)
        {
            this.CountSet = countSet;
            this.CountEmpty = countEmpty;
        }
        public void Clear()
        {
            //this.fieldDirect = BiDirect.LeftToRight;
            this.CountEmpty = 0;
            this.CountSet = 0;
        }
        #endregion


    }


    #endregion

}
