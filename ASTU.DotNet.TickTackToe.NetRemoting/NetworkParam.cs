﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe.GameParameter;

namespace ASTU.DotNet.TickTackToe.NetRemoting
{
    [Serializable]
    public enum NetworkOrder 
    {
        ServerOrder,
        ClientOrder
    }
    
    public class NetworkParam : MarshalByRefObject
    {

        public Player ServerPlayer { get; private set; }
        public Player ClientPlayer { get; private set; }

        public NetworkOrder Order { get; set; }

        public bool ClientIsReady { get; set; }

        public NetworkParam()
        {
            this.ServerPlayer = null;
            this.ClientPlayer = null;
            this.Order = NetworkOrder.ServerOrder;
        }

        public bool ConnectServer(Player server)
        {

            if (this.ServerPlayer == null)
            {
                this.ServerPlayer = server;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ConnectClient(Player client)
        {
            if (this.ClientPlayer == null)
            {
                this.ClientPlayer = client;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DisconnectServer()
        {
            if (this.ServerPlayer == null)
            {
                this.ServerPlayer = null;
                return true;
            }
            else
                return false;
        }
        public bool DisconnectClient()
        {
            if (this.ClientPlayer == null)
            {
                this.ClientPlayer = null;
                return false;
            }
            else
                return false;
        }

    }
}
