﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe;
using ASTU.DotNet.TickTackToe.GameParameter;

namespace ASTU.DotNet.TickTackToe.NetRemoting
{
    [Serializable]
    public class NetRemObjEventArgs : EventArgs
    {
        public Player PlayerConnected { get; private set; }
        public bool IsConnected { get; private set; }
        public NetRemObjEventArgs(Player player, bool isConnected)
        {
            this.PlayerConnected = player;
            this.IsConnected = isConnected;
        }
    }

    [Serializable]
    public class FieldEventArgs : EventArgs
    {
        public CellState State { get; set; } 
        public int X { get; set; }
        public int Y { get; set; }

        public FieldEventArgs(CellState state, int x, int y)
        {
            this.State = state;
            this.X = x;
            this.Y = y;
        }
    }


    public class NetRemObj : MarshalByRefObject , IField
    {
        public NetworkParam RemoteParam { get; set; }
        private IField RemoteField { get; set; }
        public bool IsGameBegin { get; set; } 

        public int NumConnected { get; set; }

        public int Port { get; set; }

        public delegate void NetRemoteObjEventHandler(object sender, NetRemObjEventArgs args);
        public event NetRemoteObjEventHandler ConnectedNewPlayer;
        public event NetRemoteObjEventHandler ConnectedTwoPlayers;

        public delegate void FieldTurnEventHandler(object sender, FieldEventArgs args);
        public event FieldTurnEventHandler Turned;

        public NetRemObj()
        {
            this.RemoteField = new Field();
            this.RemoteParam = new NetworkParam();
            this.NumConnected = 1;
        }

        public void Connected(Player player)
        {
            bool isConnected = false;
            if (this.NumConnected == 1)
            {
                isConnected = this.RemoteParam.ConnectServer(player);
                this.NumConnected++;
            }
            else if (this.NumConnected == 2)
            {
                isConnected = this.RemoteParam.ConnectClient(player);
                this.NumConnected++;
                
                //ConnectedTwoPlayers(this, new NetRemObjEventArgs(player, isConnected));
                
                if (this.RemoteParam.ServerPlayer.Figure == this.RemoteParam.ClientPlayer.Figure)
                {
                    this.RemoteParam.ClientPlayer.Figure = (this.RemoteParam.ServerPlayer.Figure == CellState.Tack) ? CellState.Tick : CellState.Tack;
                }
            }                                 
                
            //if (ConnectedNewPlayer != null)
                //ConnectedNewPlayer.Invoke(this, new NetRemObjEventArgs(player, isConnected)); 
        }

        public void FirstStepIsServer(bool isServer)
        {
            if (isServer == true)
                this.RemoteParam.Order = NetworkOrder.ServerOrder;
            else
                this.RemoteParam.Order = NetworkOrder.ClientOrder;
        }


        #region IField Members

        void IField.Cancel(int TurnStep)
        {
            this.RemoteField.Cancel(TurnStep);
        }

        void IField.Clear()
        {
            this.RemoteField.Clear();
        }

        int IField.Count
        {
            get { return this.RemoteField.Count; }
        }

        IList<Cell> IField.GetMarkedCells(CellState state)
        {
            return this.RemoteField.GetMarkedCells(state);
        }

        IList<Cell> IField.GetMarkedCells()
        {
            return this.RemoteField.GetMarkedCells();
        }

        bool IField.IsEnd()
        {
            return this.RemoteField.IsEnd();
        }

        CellState IField.LastTurnState
        {
            get
            {
                return this.RemoteField.LastTurnState;
            }
            set
            {
                this.RemoteField.LastTurnState = value;
            }
        }

        void IField.Turn(CellState state, int x, int y)
        {
            this.RemoteField.Turn(state, x, y);
            //if (this.Turned != null)
            //    this.Turned(this, new FieldEventArgs(state, x , y));
        }

        Cell IField.this[int x, int y]
        {
            get
            {
                return this.RemoteField[x, y];
            }
            set
            {
                this.RemoteField[x, y] = value;
            }
        }

        #endregion

        
    }
}
