﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;

namespace ASTU.DotNet.TickTackToe.WCF.ConsoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Console Based WCF Host for TickTackToe App *****");
            using (ServiceHost serviceHost = new ServiceHost(typeof(FieldWCF)))
            {
                serviceHost.Open();
                Console.WriteLine("The service is ready.");
                //DisplayHostInfo(serviceHost);
                Console.WriteLine("Press the Enter key to terminate service.");
                Console.ReadLine();
            }
        }
    }
}
