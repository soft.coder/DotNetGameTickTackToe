﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.Runtime.Serialization;

namespace ASTU.DotNet.TickTackToe
{
    [Serializable]
    public enum CellState 
    {
        Empty,
        /// <summary>
        /// Cross
        /// </summary>
        Tick,
        /// <summary>
        /// Circle
        /// </summary>
        Tack
    }

    [Serializable]    
    [DataContract]
    public class Cell : ICell
    {
        #region Properties
        [DataMember]
        public int X { get; set; }
        [DataMember]
        public int Y { get; set; }
        [DataMember]
        public CellState State { get; set; }


        
        #region ExtProp

        private CellState StateEnemy
        { get{ return (this.State == CellState.Tack) ? CellState.Tick : CellState.Tack; } }

        #endregion

        #endregion

        #region Constructors
        public Cell()
        { 
            this.State = CellState.Empty;

        }
        public Cell(int x, int y) : this()
        { this.X = x; this.Y = y; }
        public Cell(int x, int y, CellState state) : this(x, y)
        { this.State = state; }
        #endregion

    }

}
