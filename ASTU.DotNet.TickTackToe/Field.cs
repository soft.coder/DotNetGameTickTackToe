﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;

namespace ASTU.DotNet.TickTackToe
{
    internal enum BiDirect
    {
        LeftToRight,
        TopToDown,
        LeftDownToRightUp,
        LeftUpToRightDown
    }

    
    public class Field : MarshalByRefObject , IField
    {
        //public List<Cell> FieldCells = new List<Cell>();

        #region Properties
        private List<Cell> FieldCells{ get; set; }

        public int Count 
        {
            get { return this.FieldCells.Count; } 
        } 

        public Cell this[int x,int y] 
        {
            get 
            {
                var cell = from c in FieldCells
                           where c.X == x && c.Y == y
                           select c;
                if (cell.Count() != 0)
                    return cell.First();
                else
                    return new Cell(x, y);
            }
            set
            {

            }
        }

        public CellState LastTurnState { get; set; }

        //public bool IsCrossWin { get; set; }
        
        #endregion

        #region Constructors
        
        public Field()
        {
            this.FieldCells = new List<Cell>();
        }

        #endregion

        #region TurnMethods
        public virtual void Turn(CellState state, int x, int y)
        {
            ApplicationException ex = new ApplicationException();
            
            if (state == CellState.Empty)
                throw new ApplicationException("Нельзя пойти пустым местом");

            if (state == this.LastTurnState)
                throw new ApplicationException("Нельзя пойти дважды.");
            
            var cell = from c in FieldCells
                       where c.X == x && c.Y == y
                       select c;


            if (cell.Count() != 0)
            {
                if (cell.First().State == CellState.Tick || cell.First().State == CellState.Tack)
                    throw new ApplicationException("Нельзя пойти в занятую ячейку");
                cell.First().State = state;
            }
            else
                AddCell(new Cell(x, y , state));
            this.LastTurnState = state;
        }

        private void AddCell(Cell cell)
        {
            FieldCells.Add(new Cell(cell.X, cell.Y, cell.State));
            this.LastTurnState = cell.State;
        }

        public void Cancel(int TurnStep)
        {
            if (TurnStep * 2 > this.FieldCells.Count)
                throw new ApplicationException(string.Format("Ошибка при отмене хода: TurnStep={0}; FieldCells.Count={1}",
                                                               TurnStep, this.FieldCells.Count));

            for (int i = 0; i < TurnStep; i++)
            {
                this.FieldCells.RemoveAt(this.FieldCells.Count - 1);
                this.FieldCells.RemoveAt(this.FieldCells.Count - 1);
            }
            
        }
        
        #endregion 
        
        #region CheckEndMethods

        public bool IsEnd()
        {
            foreach (Cell cell in this.FieldCells.Where( c => c.State != CellState.Empty))
            {
                if (CheckEndInCurrCell(cell, cell.X, cell.Y, 1) == true)
                {
                    //if (cell.State == CellState.Tick) 
                    //    this.IsCrossWin = true;
                    //else 
                    //    this.IsCrossWin = false;
                    
                    return true;
                }
            }
            return false;
        }
       

        private bool CheckEndInCurrCell(Cell cell, int x, int y, int curN)
        {
            if (UnivVerifyEnd(cell, BiDirect.LeftToRight) ||
                UnivVerifyEnd(cell, BiDirect.TopToDown) ||
                UnivVerifyEnd(cell, BiDirect.LeftDownToRightUp) ||
                UnivVerifyEnd(cell, BiDirect.LeftUpToRightDown))
                return true;
            else
                return false;
        }
        private bool UnivVerifyEnd(Cell cell, BiDirect direct)
        {
            int x = cell.X;
            int y = cell.Y;

            int dFdX , dSdX , 
                dFdY , dSdY;

            SwitchBiDirect(direct, out dFdX, out dSdX, out dFdY, out dSdY);
            

            int n = 1;
            bool firstDirection = false,
                 secondDirection = false;


            for (int xfd = x  + dFdX, xsd = x + dSdX, yfd = y + dFdY, ysd = y + dSdY;
                 !firstDirection || !secondDirection; xfd += dFdX , xsd += dSdX, yfd +=dFdY, ysd +=dSdY)
            {
                if (firstDirection == false && this[xfd, yfd].State == cell.State)
                    n++;
                else
                    firstDirection = true;
                if (secondDirection == false && this[xsd, ysd].State == cell.State)
                    n++;
                else
                    secondDirection = true;
            }
            if (n >= 5) 
                return true;
            else
                return false;
        }

        #endregion

        #region otherMethods
        public IList<Cell> GetMarkedCells()
        {
            var cellNoEmpty = from c in FieldCells
                              where c.State != CellState.Empty
                              select c;
            return cellNoEmpty.ToList<Cell>();
        }
        public IList<Cell> GetMarkedCells(CellState state)
        {
            return this.FieldCells.Where(c => c.State == state).ToList();
        }

        public void Clear()
        {
            this.FieldCells.Clear();
        }


        private bool IsSet(int x, int y)
        {
            var cell = from c in FieldCells
                       where c.X == x && c.Y == y
                       select c;
            if (cell.Count() != 0)
                return true;
            else
                return false;
        }
        private void SwitchBiDirect(BiDirect direct, out int dFdX, out int dSdX, out int dFdY, out int dSdY)
        {
            switch (direct)
            {
                case BiDirect.LeftToRight:
                    {
                        dFdX = -1; dSdX = 1;
                        dFdY = dSdY = 0;
                        break;
                    }
                case BiDirect.TopToDown:
                    {
                        dFdX = dSdX = 0;
                        dFdY = -1; dSdY = 1;
                        break;
                    }
                case BiDirect.LeftDownToRightUp:
                    {
                        dFdX = -1; dSdX = 1;
                        dFdY = 1; dSdY = -1;
                        break;
                    }
                case BiDirect.LeftUpToRightDown:
                    {
                        dFdX = -1; dSdX = 1;
                        dFdY = -1; dSdY = 1;
                        break;
                    }
                default:
                    {
                        dFdX = dFdY = dSdX = dSdY = 0;
                        break;
                    }
            }
        }
        #endregion

    }
}
