﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;

namespace ASTU.DotNet.TickTackToe
{
    
    public interface IField
    {
        #region Prop
        int Count { get; }
        Cell this[int x, int y] { get; set; }
        CellState LastTurnState { get; set; }
        #endregion

        #region TurnMethods
        void Turn(CellState state, int x, int y);
        void Cancel(int TurnStep);
        #endregion

        #region CheckEndMethods
        bool IsEnd();
        #endregion

        #region otherMethods
        IList<Cell> GetMarkedCells();
        IList<Cell> GetMarkedCells(CellState state);
        void Clear();
        #endregion
    }

    public interface IFieldEvent
    {
        //delegate void FieldTurnEventHandler(object sender, EventArgs args);
        //event FieldTurnEventHandler Turned;
    }
}
