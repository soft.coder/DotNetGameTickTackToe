﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
namespace ASTU.DotNet.TickTackToe
{
    interface ICell
    {
        
        CellState State { get; set; }
        int X { get; set; }
        int Y { get; set; }
    }
}
