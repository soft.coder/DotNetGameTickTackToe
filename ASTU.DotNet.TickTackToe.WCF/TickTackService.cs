﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using ASTU.DotNet.TickTackToe;

namespace ASTU.DotNet.TickTackToe.WCF
{
    [ServiceContract]
    [DataContract]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)] 
    public class FieldWCF : IField
    {
        private Field FieldCells { get; set; }
        [DataMember]
        public int CountConnected {get; private set; } 
        public FieldWCF()
        {
            this.FieldCells = new Field();

        }
        [OperationContract]
        public int Connect()
        {
            if (this.CountConnected < 2)
            {
                return ++this.CountConnected;
                
            }
            else
                return -1;
        }
        [OperationContract]
        public int GetCountConnected()
        {
            return this.CountConnected;
        }
        [OperationContract]
        public CellState GetLastTurnState()
        {
            return this.FieldCells.LastTurnState;
        }
        #region IField Members
        [DataMember]
        public int Count
        {
            get { return FieldCells.Count; }
        }
        [DataMember]
        public Cell this[int x, int y]
        {
            get
            {
                return this.FieldCells[x, y];
            }
            set
            {
                this.FieldCells[x, y] = value;
            }
        }
        [DataMember]
        public CellState LastTurnState
        {
            get
            {
                return this.FieldCells.LastTurnState;
            }
            set
            {
                this.FieldCells.LastTurnState = value;
            }
        }
        [OperationContract]
        public void Turn(CellState state, int x, int y)
        {
            this.FieldCells.Turn(state, x, y);
        }
        [OperationContract]
        public void Cancel(int TurnStep)
        {
            this.FieldCells.Cancel(TurnStep);
        }
        [OperationContract]
        public bool IsEnd()
        {
            return this.FieldCells.IsEnd();
        }
        [OperationContract]
        public IList<Cell> GetMarkedCells()
        {
            return this.FieldCells.GetMarkedCells();
        }
        [OperationContract(Name="GetMarkedCellsWithState")]
        public IList<Cell> GetMarkedCells(CellState state)
        {
            return this.FieldCells.GetMarkedCells(state);
        }
        [OperationContract]
        public void Clear()
        {
            this.FieldCells.Clear();
        }

        #endregion
    }
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    //public class TickTackService : ITickTackService
    //{
    //    public string GetData(int value)
    //    {
    //        return string.Format("You entered: {0}", value);
    //    }

    //    public CompositeType GetDataUsingDataContract(CompositeType composite)
    //    {
    //        if (composite == null)
    //        {
    //            throw new ArgumentNullException("composite");
    //        }
    //        if (composite.BoolValue)
    //        {
    //            composite.StringValue += "Suffix";
    //        }
    //        return composite;
    //    }
    //}
}
