<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="ru"><head>


	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<meta name="generator" content="vBulletin 3.8.5">

<meta name="keywords" content=" �������� ������, �����, �����������, delphi, �����, ����������������, ���������, assembler, winapi, c++, html, php">
<meta name="description" content=" �������� ������ ����� ������� Delphi">


<!-- CSS Stylesheet -->
<style type="text/css" id="vbulletin_css">
/**
* vBulletin 3.8.5 CSS
* Style: 'club'; Style ID: 2
*/
body
{
	background: #FFFFFF;
	color: #000000;
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	margin: 5px 10px 10px 10px;
	padding: 0px;
}
a:link, body_alink
{
	color: #1C3289;
}
a:visited, body_avisited
{
	color: #1C3289;
}
a:hover, a:active, body_ahover
{
	color: #000000;
}
.page
{
	background: #FFFFFF;
	color: #000000;
}
td, th, p, li
{
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.tborder
{
	background: #5C5E5C;
	color: #000000;
	border: 1px solid #5C5E5C;
}
.tcat
{
	background: #5C5E5C;
	color: #E4DEE4;
	font: bold 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.tcat a:link, .tcat_alink
{
	color: #E4DEE4;
	text-decoration: none;
}
.tcat a:visited, .tcat_avisited
{
	color: #E4DEE4;
	text-decoration: none;
}
.tcat a:hover, .tcat a:active, .tcat_ahover
{
	color: #E4DEE4;
	text-decoration: underline;
}
.thead
{
	background: #D4D2CC;
	color: #000000;
	font: bold 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.thead a:link, .thead_alink
{
	color: #1C3289;
	text-decoration: none;
}
.thead a:visited, .thead_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.thead a:hover, .thead a:active, .thead_ahover
{
	color: #000000;
	text-decoration: underline;
}
.tfoot
{
	background: #5C5E5C;
	color: #E4DEE4;
}
.tfoot a:link, .tfoot_alink
{
	color: #E4DEE4;
}
.tfoot a:visited, .tfoot_avisited
{
	color: #E4DEE4;
}
.tfoot a:hover, .tfoot a:active, .tfoot_ahover
{
	color: #E4DEE4;
}
.alt1, .alt1Active
{
	background: #FFFFFF;
	color: #000000;
}
.alt1 a:link, .alt1_alink, .alt1Active a:link, .alt1Active_alink
{
	color: #1C3289;
	text-decoration: none;
}
.alt1 a:visited, .alt1_avisited, .alt1Active a:visited, .alt1Active_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.alt1 a:hover, .alt1 a:active, .alt1_ahover, .alt1Active a:hover, .alt1Active a:active, .alt1Active_ahover
{
	color: #4B4A4A;
	text-decoration: underline;
}
.alt2, .alt2Active
{
	background: #D4D0C8;
	color: #000000;
}
.alt2 a:link, .alt2_alink, .alt2Active a:link, .alt2Active_alink
{
	color: #1C3289;
	text-decoration: none;
}
.alt2 a:visited, .alt2_avisited, .alt2Active a:visited, .alt2Active_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.alt2 a:hover, .alt2 a:active, .alt2_ahover, .alt2Active a:hover, .alt2Active a:active, .alt2Active_ahover
{
	color: #4B4A4A;
	text-decoration: underline;
}
.inlinemod
{
	background: #D4D2CC;
	color: #000000;
}
.wysiwyg
{
	background: #D3D3D2;
	color: #000000;
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
textarea, .bginput
{
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.bginput option, .bginput optgroup
{
	font-size: 10pt;
	font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.button
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
select
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
option, optgroup
{
	font-size: 11px;
	font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.smallfont
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.time
{
	color: #000000;
}
.navbar
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.highlight
{
	color: #FF0000;
	font-weight: bold;
}
.fjsel
{
	background: #9E9E9C;
	color: #000000;
}
.fjdpth0
{
	background: #F7F7F7;
	color: #000000;
}
.panel
{
	background: #D4D2CC;
	color: #000000;
	padding: 10px;
	border: 2px outset;
}
.panelsurround
{
	background: #9E9E9C;
	color: #000000;
}
legend
{
	color: #000000;
	font: 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.vbmenu_control
{
	background: #5C5E5C;
	color: #FFFFFF;
	font: bold 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	padding: 3px 6px 3px 6px;
	white-space: nowrap;
}
.vbmenu_control a:link, .vbmenu_control_alink
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_control a:visited, .vbmenu_control_avisited
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_control a:hover, .vbmenu_control a:active, .vbmenu_control_ahover
{
	color: #FFFFFF;
	text-decoration: underline;
}
.vbmenu_popup
{
	background: #5C5E5C;
	color: #FFFFFF;
	border: 1px solid #D4D2CC;
}
.vbmenu_option
{
	background: #D4D2CC;
	color: #000000;
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	white-space: nowrap;
	cursor: pointer;
}
.vbmenu_option a:link, .vbmenu_option_alink
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_option a:visited, .vbmenu_option_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_option a:hover, .vbmenu_option a:active, .vbmenu_option_ahover
{
	color: #000000;
	text-decoration: none;
}
.vbmenu_hilite
{
	background: #FFFFCC;
	color: #000000;
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	white-space: nowrap;
	cursor: pointer;
}
.vbmenu_hilite a:link, .vbmenu_hilite_alink
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_hilite a:visited, .vbmenu_hilite_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_hilite a:hover, .vbmenu_hilite a:active, .vbmenu_hilite_ahover
{
	color: #000000;
	text-decoration: none;
}
/* ***** styling for 'big' usernames on postbit etc. ***** */
.bigusername { font-size: 14pt; }

/* ***** small padding on 'thead' elements ***** */
td.thead, th.thead, div.thead { padding: 4px; }

/* ***** basic styles for multi-page nav elements */
.pagenav a { text-decoration: none; }
.pagenav td { padding: 2px 4px 2px 4px; }

/* ***** de-emphasized text */
.shade, a.shade:link, a.shade:visited { color: #777777; text-decoration: none; }
a.shade:active, a.shade:hover { color: #FF4400; text-decoration: underline; }
.tcat .shade, .thead .shade, .tfoot .shade { color: #DDDDDD; }

/* ***** define margin and font-size for elements inside panels ***** */
.fieldset { margin-bottom: 6px; }
.fieldset, .fieldset td, .fieldset p, .fieldset li { font-size: 11px; }
</style>
<link rel="stylesheet" type="text/css" href="showthread1_files/vbulletin_important.css">


<!-- / CSS Stylesheet -->

<script type="text/javascript" src="showthread1_files/yahoo-dom-event.js"></script>
<script type="text/javascript" src="showthread1_files/connection-min.js"></script>
<script type="text/javascript">
<!--
var SESSIONURL = "";
var SECURITYTOKEN = "guest";
var IMGDIR_MISC = "images/1070/misc";
var vb_disable_ajax = parseInt("0", 10);
// -->
</script>
<script type="text/javascript" src="showthread1_files/vbulletin_global.js"></script>
<script type="text/javascript" src="showthread1_files/vbulletin_menu.js"></script>


	<link rel="alternate" type="application/rss+xml" title="����� ������������� RSS Feed" href="http://www.programmersforum.ru/external.php?type=RSS2">
	
		<link rel="alternate" type="application/rss+xml" title="����� ������������� - ����� ������� Delphi - RSS Feed" href="http://www.programmersforum.ru/external.php?type=RSS2&amp;forumids=2">
	

	    <title> �������� ������ - ����� ������� Delphi  - ����� �������������</title>
	<script type="text/javascript" src="showthread1_files/vbulletin_post_loader.js"></script>
	<style type="text/css" id="vbulletin_showthread_css">
	<!--
	
	#links div { white-space: nowrap; }
	#links img { vertical-align: middle; }
	-->
	</style>
<style type="text/css">.GleeThemeDefault{ background-color:#333 !important; color:#fff !important; font-family: Calibri, "Lucida Grande", Lucida, Arial, sans-serif !important; } .GleeThemeWhite{ background-color:#fff !important; color:#000 !important; border: 1px solid #939393 !important; -moz-border-radius: 10px !important; font-family: Calibri, "Lucida Grande", Lucida, Arial, sans-serif !important; } #gleeBox.GleeThemeWhite{ opacity:0.75; } #gleeSearchField.GleeThemeWhite{ border: 1px solid #939393 !important; } .GleeThemeRuby{ background-color: #530000 !important; color: #f6b0ab !important; font-family: "Lucida Grande", Lucida, Verdana, sans-serif !important; } .GleeThemeGreener{ background-color: #2e5c4f !important; color: #d3ff5a !important; font-family: Georgia, "Times New Roman", Times, serif !important; } .GleeThemeConsole{ font-family: Monaco, Consolas, "Courier New", Courier, mono !important; color: #eafef6 !important; background-color: #111 !important; } .GleeThemeGlee{ background-color: #eb1257 !important; color: #fff300 !important; -webkit-box-shadow: #eb1257 0px 0px 8px !important; -moz-box-shadow: #eb1257 0px 0px 8px !important; opacity: 0.8 !important; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif !important; }.GleeReaped{ background-color: #fbee7e !important; border: 1px dotted #818181 !important; } .GleeHL{ background-color: #d7fe65 !important; -webkit-box-shadow: rgb(177, 177, 177) 0px 0px 9px !important; -moz-box-shadow: rgb(177, 177, 177) 0px 0px 9px !important; padding: 3px !important; color: #1c3249 !important; border: 1px solid #818181 !important; } .GleeHL a{ color: #1c3249 !important; }#gleeBox{ line-height:20px; height:auto !important; z-index:100000; position:fixed; left:5%; top:35%; display:none; overflow:auto; width:90%; background-color:#333; opacity:0.65; color:#fff; margin:0; font-family: Calibri, "Lucida Grande", Lucida, Arial, sans-serif; padding:4px 6px; text-align:left; /*rounded corners*/ -moz-border-radius:7px; } #gleeSearchField{ outline:none; -moz-box-shadow:none; width:90%; margin:0; background:none !important; padding:0; margin:3px 0; border:none !important; height:auto !important; font-size:100px; color:#fff; } #gleeSub{ font-size:15px !important; font-family:inherit !important; font-weight:normal !important; height:auto !important; margin:0 !important; padding:0 !important; color:inherit !important; line-height:normal !important; }#gleeSubText, #gleeSubURL, #gleeSubActivity{ width:auto !important; font-size:inherit !important; color:inherit !important; font-family:inherit !important; line-height:20px !important; font-weight:normal !important; } #gleeSubText{ float:left; } #gleeSubURL{ display:inline; float:right; } #gleeSubActivity{ height:10px; display:inline; float:left; padding-left:5px; }</style></head><body onload=""><script src="showthread1_files/5778" defer="defer" async="" type="text/javascript"></script>
<!-- logo -->
<a name="top"></a>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%.">
<tbody><tr>
	<td align="left"><a href="http://www.programmersforum.ru/index.php"><img title="����� �������������" src="showthread1_files/vbulletin3_logo_white.gif" alt="����� �������������" border="0"></a></td>
	<td id="header_right_cell" align="right">
		<script type="text/javascript"><!--
google_ad_client = "pub-3780994971058297";
/* 468x60, ������� 29.10.09 */
google_ad_slot = "2537596968";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript" src="showthread1_files/show_ads.js">
</script><script src="showthread1_files/show_ads_impl.js"></script><script src="showthread1_files/expansion_embed.js"></script><script src="showthread1_files/test_domain.js"></script><script>google_protectAndRun("ads_core.google_render_ad", google_handleError, google_render_ad);</script><ins style="display: inline-table; border: medium none; height: 60px; margin: 0pt; padding: 0pt; position: relative; visibility: visible; width: 468px;"><ins id="google_ads_frame1_anchor" style="display: block; border: medium none; height: 60px; margin: 0pt; padding: 0pt; position: relative; visibility: visible; width: 468px;"><iframe allowtransparency="true" hspace="0" id="google_ads_frame1" marginheight="0" marginwidth="0" name="google_ads_frame" src="showthread1_files/ads.htm" style="left: 0pt; position: absolute; top: 0pt;" vspace="0" scrolling="no" frameborder="0" height="60" width="468"></iframe></ins></ins>

	</td>
</tr>
</tbody></table>
<!-- /logo -->

<!-- content table -->
<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">




<style>
A { text-decoration: none; }
A:hover { text-decoration: underline;  }
    </style><b>
<a href="http://programmersclub.ru/">�������</a>
&nbsp;|&nbsp;
<a href="http://www.programmersforum.ru/rules.php">������� ������</a>
&nbsp;|&nbsp;
<a href="http://www.programmersclub.ru/lab">��������� Delphi</a>
&nbsp;|&nbsp;
<a href="http://www.delphibasics.ru/">������ Delphi</a>
&nbsp;|&nbsp;
<a href="http://www.pblog.ru/">���� �������������</a>
&nbsp;|&nbsp;
<a href="http://programmersclub.ru/category/%D1%80%D0%B0%D1%81%D1%81%D1%8B%D0%BB%D0%BA%D0%B0/">��������</a>
&nbsp;|&nbsp;
<noindex><nofollow>
<a href="http://programmersforum.printdirect.ru/">������ ��������!</a>
</nofollow></noindex>
</b>
<br><br><center>
<!--  AdRiver code START. Type:728x90 Site: prgclub PZ: 0 BN: 1 -->
<script language="javascript" type="text/javascript"><!--
var RndNum4NoCash = Math.round(Math.random() * 1000000000);
var ar_Tail='unknown'; if (document.referrer) ar_Tail = escape(document.referrer);
document.write(
'<iframe src="http://ad.adriver.ru/cgi-bin/erle.cgi?'
+ 'sid=162574&bn=1&target=blank&bt=36&pz=0&rnd=' + RndNum4NoCash + '&tail256=' + ar_Tail
+ '" frameborder=0 vspace=0 hspace=0 width=728 height=90 marginwidth=0'
+ ' marginheight=0 scrolling=no></iframe>');
//--></script><iframe src="showthread1_files/erle.htm" vspace="0" hspace="0" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" height="90" width="728"></iframe>
<noscript>
<a href="http://ad.adriver.ru/cgi-bin/click.cgi?sid=162574&bn=1&bt=36&pz=0&rnd=911497674" target=_blank>
<img src="http://ad.adriver.ru/cgi-bin/rle.cgi?sid=162574&bn=1&bt=36&pz=0&rnd=911497674" alt="-AdRiver-" border=0 width=728 height=90></a>
</noscript>
<!--  AdRiver code END  -->
</center>
<br>

<!-- breadcrumb, login, pm info -->
<table class="tborder" align="center" border="0" cellpadding="8" cellspacing="1" width="100%">
<tbody><tr>
	<td class="alt1" width="100%">
		
			<table border="0" cellpadding="0" cellspacing="0">
			<tbody><tr valign="bottom">
				<td><a href="#" onclick="history.back(1); return false;"><img title="���������" src="showthread1_files/navbits_start.gif" alt="���������" border="0"></a></td>
				<td>&nbsp;</td>
				<td width="100%"><span class="navbar"><a href="http://www.programmersforum.ru/index.php" accesskey="1">����� �������������</a></span> 
	<span class="navbar">&gt; <a href="http://www.programmersforum.ru/forumdisplay.php?f=1">Delphi ����������������</a></span>


	<span class="navbar">&gt; <a href="http://www.programmersforum.ru/forumdisplay.php?f=2">����� ������� Delphi</a></span>

</td>
			</tr>
			<tr>
				<td class="navbar" style="font-size: 10pt; padding-top: 1px;" colspan="3"><a href="http://www.programmersforum.ru/showthread.php?t=23477"><img title="������������� ��������" class="inlineimg" src="showthread1_files/navbits_finallink_ltr.gif" alt="������������� ��������" border="0"></a> <strong>
	 �������� ������

</strong></td>
			</tr>
			</tbody></table>
		
	</td>

	<td class="alt2" style="padding: 0px;" nowrap="nowrap">
		<!-- login form -->
		<form action="login.php?do=login" method="post" onsubmit="md5hash(vb_login_password, vb_login_md5password, vb_login_md5password_utf, 0)">
		<script type="text/javascript" src="showthread1_files/vbulletin_md5.js"></script>
		<table border="0" cellpadding="0" cellspacing="3">
		<tbody><tr>
			<td class="smallfont" style="white-space: nowrap;"><label for="navbar_username">���</label></td>
			<td><input class="bginput" style="font-size: 11px;" name="vb_login_username" id="navbar_username" size="10" accesskey="u" tabindex="101" value="���" onfocus="if (this.value == '���') this.value = '';" type="text"></td>
			<td class="smallfont" nowrap="nowrap"><label for="cb_cookieuser_navbar"><input name="cookieuser" value="1" tabindex="103" id="cb_cookieuser_navbar" accesskey="c" type="checkbox">���������?</label></td>
		</tr>
		<tr>
			<td class="smallfont"><label for="navbar_password">������</label></td>
			<td><input class="bginput" style="font-size: 11px;" name="vb_login_password" id="navbar_password" size="10" tabindex="102" type="password"></td>
			<td><input class="button" value="����" tabindex="104" title="������� ���� ��� ������������ � ������, ����� �����, ��� ������� ������ '�����������', ����� ������������������." accesskey="s" type="submit"></td>
		</tr>
		</tbody></table>
		<input name="s" value="" type="hidden">
		<input name="securitytoken" value="guest" type="hidden">
		<input name="do" value="login" type="hidden">
		<input name="vb_login_md5password" type="hidden">
		<input name="vb_login_md5password_utf" type="hidden">
		</form>
		<!-- / login form -->
	</td>

</tr>
</tbody></table>
<!-- / breadcrumb, login, pm info -->

<!-- nav buttons bar -->
<div class="tborder" style="padding: 1px; border-top-width: 0px;">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr align="center">
		
		
			<td class="vbmenu_control"><a href="http://www.programmersforum.ru/register.php" rel="nofollow">�����������</a></td>
		
		
		<td class="vbmenu_control"><a rel="help" href="http://www.programmersforum.ru/faq.php" accesskey="5">�������</a></td>
		
			<td class="vbmenu_control"><a style="cursor: pointer;" id="community" href="http://www.programmersforum.ru/showthread.php?t=23477&amp;nojs=1#community" rel="nofollow" accesskey="6">���������� <img alt="" title="" src="showthread1_files/menu_open.gif" border="0"></a> <script type="text/javascript"> vbmenu_register("community"); </script></td>
		
		<td class="vbmenu_control"><a href="http://www.programmersforum.ru/calendar.php">���������</a></td>
		
			
				
				<td class="vbmenu_control"><a href="http://www.programmersforum.ru/search.php?do=getdaily" accesskey="2">��������� �� ����</a></td>
				
				<td class="vbmenu_control"><a id="navbar_search" href="http://www.programmersforum.ru/search.php" accesskey="4" rel="nofollow">�����</a> </td>
			
			
		
		
		
		</tr>
	</tbody></table>
</div>
<!-- / nav buttons bar -->

<br>






<!-- NAVBAR POPUP MENUS -->

	
	<!-- community link menu -->
	<div class="vbmenu_popup" id="community_menu" style="display: none; margin-top: 3px;" align="left">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr><td class="thead">�����</td></tr>
		
		
		
		
		
			<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/memberlist.php">������������</a></td></tr>
		
		
		</tbody></table>
	</div>
	<!-- / community link menu -->
	
	
	
	<!-- header quick search form -->
	<div class="vbmenu_popup" id="navbar_search_menu" style="display: none; margin-top: 3px;" align="left">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead">����� �� ������</td>
		</tr>
		<tr>
			<td class="vbmenu_option" title="nohilite">
				<form action="search.php?do=process" method="post">

					<input name="do" value="process" type="hidden">
					<input name="quicksearch" value="1" type="hidden">
					<input name="childforums" value="1" type="hidden">
					<input name="exactname" value="1" type="hidden">
					<input name="s" value="" type="hidden">
					<input name="securitytoken" value="guest" type="hidden">
					<div><input class="bginput" name="query" size="25" tabindex="1001" type="text"><input class="button" value="�����" tabindex="1004" type="submit"></div>
					<div style="margin-top: 8px;">
						<label for="rb_nb_sp0"><input name="showposts" value="0" id="rb_nb_sp0" tabindex="1002" checked="checked" type="radio">���������� ����</label>
						&nbsp;
						<label for="rb_nb_sp1"><input name="showposts" value="1" id="rb_nb_sp1" tabindex="1003" type="radio">���������� ���������</label>
					</div>
				</form>
			</td>
		</tr>
		
		<tr>
			<td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php" accesskey="4" rel="nofollow">����������� �����</a></td>
		</tr>
		
		</tbody></table>
	</div>
	<!-- / header quick search form -->
	
	
	
<!-- / NAVBAR POPUP MENUS -->

<!-- PAGENAV POPUP -->
	<div class="vbmenu_popup" id="pagenav_menu" style="display: none; position: absolute; z-index: 50;">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead" nowrap="nowrap">� ��������...</td>
		</tr>
		<tr>
			<td class="vbmenu_option" title="">
			<form action="index.php" method="get" onsubmit="return this.gotopage()" id="pagenav_form">
				<input class="bginput" id="pagenav_itxt" style="font-size: 11px;" size="4" type="text">
				<input class="button" id="pagenav_ibtn" value="�����" type="button">
			</form>
			</td>
		</tr>
		</tbody></table>
	</div>
<!-- / PAGENAV POPUP -->










<a name="poststop" id="poststop"></a>

<!-- controls above postbits -->
<table style="margin-bottom: 3px;" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="bottom">
	
		<td class="smallfont"><a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;noquote=1&amp;p=123968" rel="nofollow"><img title="�����" src="showthread1_files/reply.gif" alt="�����" border="0"></a></td>
	
	<td align="right"><div class="pagenav" align="right">
<table class="tborder" border="0" cellpadding="3" cellspacing="1">
<tbody><tr>
	<td class="vbmenu_control" style="font-weight: normal;">�������� 1 �� 2</td>
	
	
		<td class="alt2"><span class="smallfont" title="�������� � 1 �� 10 �� 11."><strong>1</strong></span></td>
 <td class="alt1"><a class="smallfont" href="http://www.programmersforum.ru/showthread.php?t=23477&amp;page=2" title="� 11 �� 11 �� 11">2</a></td>
	<td class="alt1"><a rel="next" class="smallfont" href="http://www.programmersforum.ru/showthread.php?t=23477&amp;page=2" title="��������� �������� - � 11 �� 11 �� 11">&gt;</a></td>
	
	<td style="cursor: pointer;" id="pagenav.33" class="vbmenu_control" title=""> <img alt="" title="" src="showthread1_files/menu_open.gif" border="0"></td>
</tr>
</tbody></table>
</div></td>
</tr>
</tbody></table>
<!-- / controls above postbits -->


	 	

<!-- toolbar -->
<table class="tborder" style="border-bottom-width: 0px;" align="center" border="0" cellpadding="8" cellspacing="1" width="100%">
<tbody><tr>
	<td class="tcat" width="100%">
		<div class="smallfont">
		
		&nbsp;
		</div>
	</td>
	<td style="cursor: pointer;" class="vbmenu_control" id="threadtools" nowrap="nowrap">
		<a href="http://www.programmersforum.ru/showthread.php?t=23477&amp;nojs=1#goto_threadtools">����� ����</a>
		<script type="text/javascript"> vbmenu_register("threadtools"); </script> <img alt="" title="" src="showthread1_files/menu_open.gif" border="0">
	</td>
	
	
	

	

</tr>
</tbody></table>
<!-- / toolbar -->



<!-- end content table -->

		</div>
	</div>
</div>

<!-- / close content container -->
<!-- / end content table -->





<div id="posts"><!-- post #123968 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit123968" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post123968" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post123968"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			18.07.2008, 19:59
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=123968&amp;postcount=1" target="new" rel="nofollow" id="postcount123968" name="1"><strong>1</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_123968">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=9228">aesoem</a>
				<script type="text/javascript"> vbmenu_register("postmenu_123968", true); </script>
				
			</div>

			
			<div class="smallfont">����������</div>
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 24.12.2007</div>
				
				
				<div>
					���������: 196
				</div>
				
				
				<div><span id="repdisplay_123968_9228">���������: 12</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_123968" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="��������" class="inlineimg" src="showthread1_files/icon11.gif" alt="��������" border="0">
				<strong>�������� ������</strong>
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_123968">
			
			������������ ���� ����... ������ �������... ��� ��� �� �������� �����
 ���������, ��������� ��� ���������! ���� ���� ��� ���� ������ 
���������� 0 � ��� ������ ������ ��������� ������ � ������� �������� 
������. ��� � ��� �������� ����� ����� ���� ��������� ������ ������� 
������ ���� � ������ �������� ��� ���� ������ ���! <br>
<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">���:</div>
<pre><code class=" delphi">
<span class="function"><span class="keyword">procedure</span> <span class="title">TForm1</span>.<span class="title">PKTimerTimer</span><span class="params">(Sender: TObject)</span>;</span>
<span class="keyword">var</span>
i:integer;
<span class="keyword">begin</span>
sstatusbar1.Panels[<span class="number">0</span>].Text:=<span class="string">'��������� ������ ���'</span>;
i:=random(<span class="number">9</span>);


<span class="keyword">if</span> i=<span class="number">9</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE9.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image9.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">8</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">8</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE8.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image8.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">7</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">7</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE7.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image7.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">6</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">6</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE6.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image6.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">5</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">5</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE5.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image5.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">4</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">4</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE4.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image4.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">3</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">3</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE3.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image3.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">2</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">2</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE2.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image2.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=random(<span class="number">1</span>);
  <span class="keyword">end</span>;
<span class="keyword">if</span> i=<span class="number">1</span> <span class="keyword">then</span>
  <span class="keyword">begin</span>
    <span class="keyword">IF</span> IMAGE1.Tag=<span class="number">0</span> <span class="keyword">THEN</span>
    image1.Picture.LoadFromFile(Extractfilepath(application.ExeName)+<span class="string">'FALSE.bmp'</span>) <span class="keyword">ELSE</span>
    i:=<span class="number">1</span>;
    form2.showmodal;

  <span class="keyword">end</span>;
sstatusbar1.Panels[<span class="number">0</span>].Text:=<span class="string">'������� ����� ������ ���'</span>;
sstatusbar1.Panels[<span class="number">1</span>].Text:=<span class="string">'��������� ������ ���'</span>;            <span class="comment">// &lt;---------------</span>
enabledp:=<span class="number">1</span>;
PKTimer.Enabled:=False;


<span class="keyword">end</span>;
</code></pre>
</div>
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="aesoem ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="aesoem ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=123968" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 123968 popup menu -->
<div class="vbmenu_popup" id="postmenu_123968_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">aesoem</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=9228">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=9228" rel="nofollow">����� ��� ��������� �� aesoem</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 123968 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #123968 --><script type="text/javascript" src="showthread1_files/highlight.js"></script>
<script type="text/javascript">hljs.initHighlightingOnLoad('delphi', 'cpp', 'css', 'html', 'javascript', 'java', 'php');</script><script type="text/javascript" src="showthread1_files/static.js"></script><script type="text/javascript" src="showthread1_files/www.js"></script><script type="text/javascript" src="showthread1_files/javascript.js"></script><script type="text/javascript" src="showthread1_files/dynamic.js"></script>

<link rel="stylesheet" href="showthread1_files/programmersforum.css">
<!-- post #123971 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit123971" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post123971" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post123971"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			18.07.2008, 20:17
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=123971&amp;postcount=2" target="new" rel="nofollow" id="postcount123971" name="2"><strong>2</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_123971">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=6088">puporev</a>
				<script type="text/javascript"> vbmenu_register("postmenu_123971", true); </script>
				
			</div>

			
			<div class="smallfont">������������</div>
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 13.10.2007</div>
				
				
				<div>
					���������: 2,606
				</div>
				
				
				<div><span id="repdisplay_123971_6088">���������: 731</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_123971" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_123971">
			
			Random(9) - ��� ��������� ����� �� ��������� [0,8]. ���� ��� �� �����
 ����, ������ Random(9) +1; ����� �������� [1,9]. ������� ������� 
��������� ���������� �� ���� ������ , ��� �����. ����� ����� �� 
�����������, � ������ ���� ����� ��������� ��������� Randomize;
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="puporev ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="puporev ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=123971" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 123971 popup menu -->
<div class="vbmenu_popup" id="postmenu_123971_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">puporev</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=6088">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=6088" rel="nofollow">����� ��� ��������� �� puporev</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 123971 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #123971 --><!-- post #123974 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit123974" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post123974" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post123974"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			18.07.2008, 20:28
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=123974&amp;postcount=3" target="new" rel="nofollow" id="postcount123974" name="3"><strong>3</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_123974">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=10637">Altera</a>
				<script type="text/javascript"> vbmenu_register("postmenu_123974", true); </script>
				
			</div>

			<div class="smallfont">�������� �����</div>
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://www.programmersforum.ru/member.php?u=10637"><img title="������ ��� Altera" src="showthread1_files/image.png" alt="������ ��� Altera" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 29.01.2008</div>
				<div>�����: ������� ����� ����</div>
				
				<div>
					���������: 2,234
				</div>
				
				
				<div><span id="repdisplay_123974_10637">���������: 610</span></div>
				
				<div><a href="#" onclick="imwindow('icq', '10637', 500, 450); return false;"><img title="��������� ��������� ��� Altera � ������� ICQ" src="showthread1_files/im_icq.gif" alt="��������� ��������� ��� Altera � ������� ICQ" border="0"></a>    <a href="#" onclick="imwindow('skype', '10637', 400, 285); return false;"><img title="��������� ��������� ��� Altera � ������� Skype�" src="showthread1_files/ak_altera.png" alt="��������� ��������� ��� Altera � ������� Skype�" border="0"></a></div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_123974" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_123974">
			
			���, ��������� ���� �������� ���� ����... <br>
������ ������ ��� �� ����� <img src="showthread1_files/redface.gif" alt="" title="��������" class="inlineimg" border="0"> ����� �����, �� ��� ����, �� ������ �������! <img src="showthread1_files/smile.gif" alt="" title="�������" class="inlineimg" border="0">
		</div>
		<!-- / message -->

		
		<!-- attachments -->
			<div style="padding: 8px;">

			

			

			

			
				<fieldset class="fieldset">
					<legend>��������</legend>
					<table border="0" cellpadding="0" cellspacing="3">
					<tbody><tr>
	<td><img title="��� �����: rar" class="inlineimg" src="showthread1_files/rar.gif" alt="��� �����: rar" style="vertical-align: baseline;" border="0" height="16" width="16"></td>
	<td><a href="http://www.programmersforum.ru/attachment.php?attachmentid=4887&amp;d=1216398509">Hrenka.rar</a> (391.0 ��, 208 ����������)</td>
</tr>
					</tbody></table>
				</fieldset>
			

			

			</div>
		<!-- / attachments -->
		

		
		

		
		<!-- sig -->
			<div>
				__________________<br>
				<font size="1"><font color="DarkOrchid">�������� ����� � ������� ����� ���</font> <img src="showthread1_files/wink.gif" alt="" title="������������" class="inlineimg" border="0"><br>
<font color="DarkRed">���������������� �����������, �������������!</font></font>
			</div>
		<!-- / sig -->
		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Altera ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="Altera ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=123974" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 123974 popup menu -->
<div class="vbmenu_popup" id="postmenu_123974_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Altera</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=10637">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=10637" rel="nofollow">����� ��� ��������� �� Altera</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 123974 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #123974 --><!-- post #123982 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit123982" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post123982" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post123982"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			18.07.2008, 20:57
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=123982&amp;postcount=4" target="new" rel="nofollow" id="postcount123982" name="4"><strong>4</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_123982">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=9228">aesoem</a>
				<script type="text/javascript"> vbmenu_register("postmenu_123982", true); </script>
				
			</div>

			
			<div class="smallfont">����������</div>
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 24.12.2007</div>
				
				
				<div>
					���������: 196
				</div>
				
				
				<div><span id="repdisplay_123982_9228">���������: 12</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_123982" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_123982">
			
			�������� ��������� ���������� ����...
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="aesoem ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="aesoem ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=123982" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 123982 popup menu -->
<div class="vbmenu_popup" id="postmenu_123982_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">aesoem</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=9228">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=9228" rel="nofollow">����� ��� ��������� �� aesoem</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 123982 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #123982 --><!-- post #123983 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit123983" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post123983" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post123983"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			18.07.2008, 20:59
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=123983&amp;postcount=5" target="new" rel="nofollow" id="postcount123983" name="5"><strong>5</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_123983">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=10637">Altera</a>
				<script type="text/javascript"> vbmenu_register("postmenu_123983", true); </script>
				
			</div>

			<div class="smallfont">�������� �����</div>
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://www.programmersforum.ru/member.php?u=10637"><img title="������ ��� Altera" src="showthread1_files/image.png" alt="������ ��� Altera" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 29.01.2008</div>
				<div>�����: ������� ����� ����</div>
				
				<div>
					���������: 2,234
				</div>
				
				
				<div><span id="repdisplay_123983_10637">���������: 610</span></div>
				
				<div><a href="#" onclick="imwindow('icq', '10637', 500, 450); return false;"><img title="��������� ��������� ��� Altera � ������� ICQ" src="showthread1_files/im_icq.gif" alt="��������� ��������� ��� Altera � ������� ICQ" border="0"></a>    <a href="#" onclick="imwindow('skype', '10637', 400, 285); return false;"><img title="��������� ��������� ��� Altera � ������� Skype�" src="showthread1_files/ak_altera.png" alt="��������� ��������� ��� Altera � ������� Skype�" border="0"></a></div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_123983" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_123983">
			
			<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">������:</div>
	<table border="0" cellpadding="8" cellspacing="0" width="100%">
	<tbody><tr>
		<td class="alt2" style="border: 1px inset;">
			
				<div>
					��������� �� <strong>aesoem</strong>
					<a href="http://www.programmersforum.ru/showthread.php?p=123982#post123982" rel="nofollow"><img title="���������� ���������" class="inlineimg" src="showthread1_files/viewpost.gif" alt="���������� ���������" border="0"></a>
				</div>
				<div style="font-style: italic;">�������� ��������� ���������� ����...</div>
			
		</td>
	</tr>
	</tbody></table>
</div>���, � ��� ���������? <img src="showthread1_files/eek.gif" alt="" title="���������" class="inlineimg" border="0">
		</div>
		<!-- / message -->

		

		
		

		
		<!-- sig -->
			<div>
				__________________<br>
				<font size="1"><font color="DarkOrchid">�������� ����� � ������� ����� ���</font> <img src="showthread1_files/wink.gif" alt="" title="������������" class="inlineimg" border="0"><br>
<font color="DarkRed">���������������� �����������, �������������!</font></font>
			</div>
		<!-- / sig -->
		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Altera ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="Altera ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=123983" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 123983 popup menu -->
<div class="vbmenu_popup" id="postmenu_123983_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Altera</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=10637">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=10637" rel="nofollow">����� ��� ��������� �� Altera</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 123983 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #123983 --><!-- post #123989 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit123989" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post123989" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post123989"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			18.07.2008, 21:26
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=123989&amp;postcount=6" target="new" rel="nofollow" id="postcount123989" name="6"><strong>6</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_123989">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=5190">Ring0Sn</a>
				<script type="text/javascript"> vbmenu_register("postmenu_123989", true); </script>
				
			</div>

			
			<div class="smallfont">����������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://www.programmersforum.ru/member.php?u=5190"><img title="������ ��� Ring0Sn" src="showthread1_files/image_002.png" alt="������ ��� Ring0Sn" border="0" height="79" width="79"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 06.09.2007</div>
				<div>�����: www.���������</div>
				
				<div>
					���������: 239
				</div>
				
				
				<div><span id="repdisplay_123989_5190">���������: 133</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_123989" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_123989">
			
			aesoem, �������� ��� ���
		</div>
		<!-- / message -->

		
		<!-- attachments -->
			<div style="padding: 8px;">

			

			

			

			
				<fieldset class="fieldset">
					<legend>��������</legend>
					<table border="0" cellpadding="0" cellspacing="3">
					<tbody><tr>
	<td><img title="��� �����: rar" class="inlineimg" src="showthread1_files/rar.gif" alt="��� �����: rar" style="vertical-align: baseline;" border="0" height="16" width="16"></td>
	<td><a href="http://www.programmersforum.ru/attachment.php?attachmentid=4888&amp;d=1216401971">Krestik_nolik.rar</a> (5.5 ��, 193 ����������)</td>
</tr>
					</tbody></table>
				</fieldset>
			

			

			</div>
		<!-- / attachments -->
		

		
		

		
		<!-- sig -->
			<div>
				__________________<br>
				<a href="http://googleforidiots.com/" target="_blank"><b><font color="Blue">G</font><font color="Red">o</font><font color="Yellow">o</font><font color="Blue">g</font><font color="Lime">l</font><font color="Red">e</font>foridiots.com</b></a>
			</div>
		<!-- / sig -->
		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Ring0Sn ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="Ring0Sn ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=123989" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 123989 popup menu -->
<div class="vbmenu_popup" id="postmenu_123989_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Ring0Sn</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=5190">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=5190" rel="nofollow">����� ��� ��������� �� Ring0Sn</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 123989 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #123989 --><!-- post #124701 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit124701" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post124701" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post124701"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			22.07.2008, 01:53
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=124701&amp;postcount=7" target="new" rel="nofollow" id="postcount124701" name="7"><strong>7</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_124701">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=7825">Terran</a>
				<script type="text/javascript"> vbmenu_register("postmenu_124701", true); </script>
				
			</div>

			
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://www.programmersforum.ru/member.php?u=7825"><img title="������ ��� Terran" src="showthread1_files/image.jpg" alt="������ ��� Terran" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 28.11.2007</div>
				<div>�����: ��������, �.������</div>
				
				<div>
					���������: 1,434
				</div>
				
				
				<div><span id="repdisplay_124701_7825">���������: 483</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_124701" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_124701">
			
			��� ��� ���� �������� ���� �������� ������, ����� ������� ������:<br>
<br>
<a href="http://www.programmersforum.ru/attachment.php?attachmentid=4936&amp;d=1216677119" target="_blank" title="��������: cross_zero.zip
����������: 180

������: 91.7 ��">cross_zero.zip</a>
		</div>
		<!-- / message -->

		
		<!-- attachments -->
			<div style="padding: 8px;">

			

			

			

			

			

			</div>
		<!-- / attachments -->
		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Terran ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="Terran ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=124701" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 124701 popup menu -->
<div class="vbmenu_popup" id="postmenu_124701_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Terran</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=7825">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=7825" rel="nofollow">����� ��� ��������� �� Terran</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 124701 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #124701 --><!-- post #264898 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit264898" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post264898" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post264898"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			16.05.2009, 16:49
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=264898&amp;postcount=8" target="new" rel="nofollow" id="postcount264898" name="8"><strong>8</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_264898">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=39087">������</a>
				<script type="text/javascript"> vbmenu_register("postmenu_264898", true); </script>
				
			</div>

			
			<div class="smallfont">������������</div>
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 25.03.2009</div>
				
				
				<div>
					���������: 10
				</div>
				
				
				<div><span id="repdisplay_264898_39087">���������: 10</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_264898" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_264898">
			
			��� �������� ��������-������ � Delphi � ���������� ���������� ����� 
�������� �����? � ������ ������� �� ���� ��� ��� ���� ����� ��������� � 
Delphi . �������� ����������.���� ���� � ����-������ ��������� ���� ����
 � ��������,�� �������� ���������� . ����� �����! ���� ���, 379113303 
��� ���. � ������ ���� ������ ���� ���� ��� ��������
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="������ ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="������ ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=264898" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 264898 popup menu -->
<div class="vbmenu_popup" id="postmenu_264898_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">������</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=39087">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=39087" rel="nofollow">����� ��� ��������� �� ������</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 264898 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #264898 --><!-- post #294987 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit294987" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post294987" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post294987"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			22.06.2009, 21:40
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=294987&amp;postcount=9" target="new" rel="nofollow" id="postcount294987" name="9"><strong>9</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_294987">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=39087">������</a>
				<script type="text/javascript"> vbmenu_register("postmenu_294987", true); </script>
				
			</div>

			
			<div class="smallfont">������������</div>
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 25.03.2009</div>
				
				
				<div>
					���������: 10
				</div>
				
				
				<div><span id="repdisplay_294987_39087">���������: 10</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_294987" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_294987">
			
			program KrestikiNoliki;<br>
<br>
{$APPTYPE CONSOLE}<br>
uses<br>
  SysUtils;<br>
<br>
const n = 3;<br>
type Matr = array [1..n,1..n] of char;<br>
var x:       matr;<br>
    k,l,i,j: byte;<br>
    a:       boolean;<br>
    f:       text;<br>
    str1, str2, s:       string[20];<br>
<br>
<br>
<br>
Function Stolbci(var a: matr): boolean;<br>
var i,j,k: byte;<br>
        x: boolean;<br>
  begin<br>
   x:=false;<br>
   { �������� ���������� �� ������� }<br>
    for i := 1 to n do<br>
      begin<br>
        k:=0;<br>
        for j := 1 to n-1 do<br>
         begin<br>
           if (a[i,j]&lt;&gt;'@') then<br>
             if (a[i,j]=a[i,j+1]) then inc(k);<br>
         end;<br>
        if k=n-1 then<br>
          begin<br>
            x:=true;<br>
            break;<br>
          end;<br>
      end;<br>
    Stolbci:=x;<br>
  end;<br>
<br>
<br>
<br>
Function Stroki(var a: matr): boolean;<br>
var i,j,k: byte;<br>
      x:   boolean;<br>
  begin<br>
     x:=false;<br>
   { �������� ���������� �� �������� }<br>
    for j := 1 to n do<br>
      begin<br>
        k:=0;<br>
        for i := 1 to n-1 do<br>
          begin<br>
           if (a[i,j]&lt;&gt;'@') then<br>
             if (a[i,j]=a[i+1,j]) then inc(k);<br>
          end;<br>
        if k=n-1 then<br>
          begin<br>
            x:=true;<br>
            break;<br>
          end;<br>
      end;<br>
    Stroki:=x;<br>
  end;<br>
<br>
<br>
<br>
Function LR(var a: matr): boolean;<br>
var i,j,k: byte;<br>
  begin<br>
   { �������� ���������� �� ��������� �� ������ �������� ���� � ������ ������ }<br>
    LR:=false;<br>
    k:=0;<br>
    for i := 1 to n-1 do<br>
         if a[i,i]&lt;&gt;'@' then<br>
              if a[i,i]=a[i+1,i+1] then inc(k);<br>
    if k=n-1 then LR:=true;<br>
  end;<br>
<br>
<br>
<br>
Function RL(var a: matr): boolean;<br>
var i,j,k: byte;<br>
  begin<br>
    RL:=false;<br>
   { �������� ���������� �� ��������� �� ������� �������� ���� � ����� ������  }<br>
    k:=0;<br>
   for i := n downto 2 do<br>
     for j := 1 to n-1 do<br>
         if a[i,j]&lt;&gt;'@' then<br>
              if a[i,j]=a[i-1,j+1] then inc(k);<br>
    if k=n-1 then RL:=true;<br>
  end;<br>
<br>
<br>
Function proverka(var a: matr): boolean;<br>
var x: boolean;<br>
  begin<br>
   { �������� �������� ���������� }<br>
    x:=Stroki(a);<br>
    if x=false then<br>
      begin<br>
         x:=Stolbci(a);<br>
         if x=false then<br>
          begin<br>
             x:=LR(a);<br>
             if x=false then x:=RL(a);<br>
          end;<br>
      end;<br>
    Proverka:=x;<br>
  end;
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="������ ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="������ ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=294987" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 294987 popup menu -->
<div class="vbmenu_popup" id="postmenu_294987_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">������</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=39087">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=39087" rel="nofollow">����� ��� ��������� �� ������</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 294987 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #294987 --><!-- post #294988 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit294988" style="padding: 0px 0px 8px;">
	



<table id="post294988" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post294988"><img title="������" class="inlineimg" src="showthread1_files/post_old.gif" alt="������" border="0"></a>
			22.06.2009, 21:40
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://www.programmersforum.ru/showpost.php?p=294988&amp;postcount=10" target="new" rel="nofollow" id="postcount294988" name="10"><strong>10</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_294988">
				
				<a class="bigusername" href="http://www.programmersforum.ru/member.php?u=39087">������</a>
				<script type="text/javascript"> vbmenu_register("postmenu_294988", true); </script>
				
			</div>

			
			<div class="smallfont">������������</div>
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 25.03.2009</div>
				
				
				<div>
					���������: 10
				</div>
				
				
				<div><span id="repdisplay_294988_39087">���������: 10</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_294988" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread1_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_294988">
			
			Procedure XodX (var a: matr);<br>
  var i,j: byte;<br>
  begin<br>
   { ��� 1 ������ }<br>
    write(str1, ', vash xod. Sdelayte xod X : ');<br>
     read(i,j);<br>
      write(f,'��� ������� ����� :' );<br>
     write(f,i);<br>
    writeln(f,j);<br>
   if a[i,j]='@' then a[i,j]:='X';<br>
  end;<br>
<br>
<br>
<br>
Procedure XodO(var a: matr);<br>
 var i,j : byte;<br>
 begin<br>
   { ��� 2 ������ }<br>
     write(str2, ', vash xod. Sdelayte xod O: ');<br>
      read(i,j);<br>
       write(f,'��� ������� ������: ' );<br>
      write(f,i);<br>
     writeln(f,j);<br>
   if a[i,j]='@' then a[i,j]:='O';<br>
 end;<br>
<br>
<br>
<br>
begin<br>
  { ��������-������ }<br>
<br>
   AssignFile(f,'igra.txt');<br>
   Append(f);<br>
    writeln;<br>
     writeln('-------------------------------------------------------------------------------- ');<br>
     writeln('---ZDRAVSTVUYTE. VAS PRIVETSTVUET IGRA KRESTIKI-NOLIKI MEZHDU DVUMYA IGROKAMI--- ');<br>
     writeln ('-------------------------------------------------------------------------------- ');<br>
     writeln;<br>
      writeln('      **************************');<br>
      writeln ('     ***Imya pervogo igroka:***');<br>
      writeln('    **************************');<br>
      writeln;<br>
      readln(str1);<br>
      writeln (f, str1);<br>
      writeln;<br>
      writeln('      **************************');<br>
      writeln ('     ***Imya vtorogo igroka:***');<br>
      writeln('    **************************');<br>
      writeln;<br>
      readln(str2);<br>
      writeln (f, str2);<br>
      writeln;<br>
      writeln('!!! IGRA NACHALAS !!! JELAU USPEXOV !!! VPERED !!!');<br>
      writeln;<br>
  writeln(f,'���� ��������-������ ����� ����� ��������');<br>
  l:=0; {�������}<br>
   for i := 1 to n do<br>
    for j := 1 to n do<br>
     x[i,j]:='@';<br>
      vivodmatr(x);<br>
       writeln;<br>
 for k := 1 to n*n do<br>
 begin<br>
   while l&lt;&gt;n*n do<br>
   begin<br>
       XodX(x);<br>
       l:=l+1;<br>
       a:=Proverka(x);<br>
       if a=true then<br>
          begin<br>
            writeln;<br>
            Vivodmatr(x);<br>
            writeln;<br>
            writeln( str1, ', pozdravlyau! Vi pobedili!!! ');<br>
            writeln(f, str1, ', �� ��������');<br>
            break;<br>
          end;<br>
<br>
          begin<br>
          writeln;<br>
          Vivodmatr(x);<br>
           writeln;<br>
          XodO(x);<br>
          l:=l+1;<br>
          a:=Proverka(x);<br>
          if a=true then<br>
           begin<br>
             writeln;<br>
             Vivodmatr(x);<br>
             writeln;<br>
             writeln( str2, ', pozdravlyau! Vi pobedili!!! ');<br>
             writeln(f, str2, ', �� ��������');<br>
             break;<br>
           end;<br>
      readln;<br>
      writeln;<br>
      Vivodmatr(x);<br>
      writeln;<br>
          end;<br>
   end;<br>
 end;<br>
 if l=n*n then<br>
             begin<br>
              writeln('NICYA');<br>
             end;<br>
<br>
 write(f,'���������� �����: ');<br>
 writeln(f,l);<br>
 CloseFile(f);<br>
 readln;<br>
 readln;<br>
end.
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="������ ��� ������" class="inlineimg" src="showthread1_files/user_offline.gif" alt="������ ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;p=294988" rel="nofollow"><img title="�������� � ������������" src="showthread1_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 294988 popup menu -->
<div class="vbmenu_popup" id="postmenu_294988_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">������</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/member.php?u=39087">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.programmersforum.ru/search.php?do=finduser&amp;u=39087" rel="nofollow">����� ��� ��������� �� ������</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 294988 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #294988 --><div id="lastpost"></div></div>

<!-- start content table -->
<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

<!-- / start content table -->

<!-- controls below postbits -->
<table style="margin-top: -5px;" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
	
		<td class="smallfont"><a href="http://www.programmersforum.ru/newreply.php?do=newreply&amp;noquote=1&amp;p=294988" rel="nofollow"><img title="�����" src="showthread1_files/reply.gif" alt="�����" border="0"></a></td>
	
	
		<td align="right"><div class="pagenav" align="right">
<table class="tborder" border="0" cellpadding="3" cellspacing="1">
<tbody><tr>
	<td class="vbmenu_control" style="font-weight: normal;">�������� 1 �� 2</td>
	
	
		<td class="alt2"><span class="smallfont" title="�������� � 1 �� 10 �� 11."><strong>1</strong></span></td>
 <td class="alt1"><a class="smallfont" href="http://www.programmersforum.ru/showthread.php?t=23477&amp;page=2" title="� 11 �� 11 �� 11">2</a></td>
	<td class="alt1"><a rel="next" class="smallfont" href="http://www.programmersforum.ru/showthread.php?t=23477&amp;page=2" title="��������� �������� - � 11 �� 11 �� 11">&gt;</a></td>
	
	<td style="cursor: pointer;" id="pagenav.137" class="vbmenu_control" title=""> <img alt="" title="" src="showthread1_files/menu_open.gif" border="0"></td>
</tr>
</tbody></table>
</div>
		
		</td>
	
</tr>
</tbody></table>
<!-- / controls below postbits -->







<!-- ������.������ -->
<script type="text/javascript"><!--
yandex_partner_id = 5778;
yandex_site_bg_color = 'FFFFFF';
yandex_site_charset = 'windows-1251';
yandex_ad_format = 'direct';
yandex_font_size = 1.1;
yandex_direct_type = 'horizontal';
yandex_direct_border_type = 'ad';
yandex_direct_limit = 2;
yandex_direct_header_bg_color = 'CCCCCC';
yandex_direct_bg_color = 'FFFFFF';
yandex_direct_border_color = 'CCCCCC';
yandex_direct_title_color = '000000';
yandex_direct_url_color = '666666';
yandex_direct_all_color = '000000';
yandex_direct_text_color = '000000';
yandex_direct_hover_color = 'CCCCCC';
document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/system/context.js"></sc'+'ript>');
//--></script><script type="text/javascript" src="showthread1_files/context.js"></script><link rel="stylesheet" type="text/css" href="showthread1_files/context_r469.css"><script type="text/javascript" src="showthread1_files/context_static_r494.js" yandex_load_check="yes"></script><script type="text/javascript" src="showthread1_files/5778.htm" yandex_load_check="yes"></script><style type="text/css">#y5_direct1 .y5_item {background-color: #FFFFFF !important;} #y5_direct1 .y5_item {border-color: #CCCCCC !important; border-style: solid !important;} #y5_direct1 .y5_bg {background-color: #CCCCCC !important;} #y5_direct1 .y5_ad div a {color: #000000 !important;} #y5_direct1 .y5_ad div {color: #000000 !important;} #y5_direct1 .y5_ad span, #y5_direct1 .y5_ad span a {color: #666666 !important;} #y5_direct1 .y5_all a, #y5_direct1 .y5_how a {color: #000000 !important;} #y5_direct1 .y5_ad div a:hover {color: #CCCCCC !important;} #y5_direct1 .y5_icon em {background-color: #666666 !important;} #y5_direct1 {font-size: 1.1em !important;} </style><div id="y5_direct1" class="y5 y5_nf y5_horizontal snap_noshots"><div class="y5_ba y5_ads2 y5_has_warnings y5_exp0"><div class="y5_h"><div class="y5_ya"><span class="y5_black"><em>�</em>�����</span><span class="y5_black y5_bg"><a class="snap_noshots" href="http://direct.yandex.ru/?partner" target="_blank">������</a></span></div><div class="y5_how"><span><a target="_blank" href="http://advertising.yandex.ru/welcome/?from=context">���� ����������</a></span></div></div><table class="y5_ads"><tbody><tr><td class="y5_item"><div class="y5_ad"><div class="ad-link"><a class="snap_noshot" href="http://an.yandex.ru/count/7HUniiBnfe840000ZheJYD04XP9f0PK2cm5kGoi1YA0gUWk9hvpx7vWndPycyPouNyWo1PQk_B-9cgYKvHcIf3e23ugcqYCAfPs3jP6ltYAxZxUXPFC7dxd8bayCaoMP1ekiwQ17GeoGKmUWa5C7f810VWm0?test-tag=35816494" target="_blank">�������� ������� �����.</a></div><div>����� �������� �� ��.</div> <span class="url"><ins class="y5_icon"><em style="left: 8px ! important; top: 3px ! important; width: 3px ! important;"></em><em style="left: 7px ! important; top: 4px ! important; width: 5px ! important;"></em><em style="left: 6px ! important; top: 5px ! important; width: 5px ! important;"></em><em style="left: 12px ! important; top: 5px ! important; width: 1px ! important;"></em><em style="left: 5px ! important; top: 6px ! important; width: 3px ! important;"></em><em style="left: 9px ! important; top: 6px ! important; width: 1px ! important;"></em><em style="left: 11px ! important; top: 6px ! important; width: 1px ! important;"></em><em style="left: 4px ! important; top: 7px ! important; width: 3px ! important;"></em><em style="left: 10px ! important; top: 7px ! important; width: 1px ! important;"></em><em style="left: 3px ! important; top: 8px ! important; width: 3px ! important;"></em><em style="left: 2px ! important; top: 9px ! important; width: 3px ! important;"></em><em style="left: 2px ! important; top: 10px ! important; width: 4px ! important;"></em><em style="left: 2px ! important; top: 11px ! important; width: 3px ! important;"></em><em style="left: 6px ! important; top: 11px ! important; width: 1px ! important;"></em><em style="left: 3px ! important; top: 12px ! important; width: 1px ! important;"></em><em style="left: 5px ! important; top: 12px ! important; width: 1px ! important;"></em><em style="left: 4px ! important; top: 13px ! important; width: 1px ! important;"></em></ins><a class="snap_noshot" href="http://an.yandex.ru/count/7HUnifthuDm40000ZheJYD04XP9f0PK2cm5kGoi1YA0gUWk9hvpx7vWndPycyPouNyWo1PQk_B-9cgYKvHcIf3e23ugcqYCAfPs3jP6ltYAxZxUXPFC7dxd8bayCaoMP1ekYwg17GeoGKmUWa5C7f810VWm0?test-tag=35816494" target="_blank">����� � �������</a>&nbsp;&nbsp;�&nbsp; www.korporacia.ru</span></div></td><td class="y5_nbsp"><div></div></td><td class="y5_item"><div class="y5_ad"><div class="ad-link"><a class="snap_noshot" href="http://an.yandex.ru/count/7HUniltvPta40000ZheJYD04XP9f0PK2cm5kGoi1CeYa6OSEYQyUkYYOCPsYpe84dBXVo385bgxylucQh-xQKPAhs7uJYg5yVmsbc7ZCaQ_U8hkFlg9IS0sViC7ljWIJ9Pa6YwVMUtn2Z93Y1A2GKmUaW41u3G00?test-tag=35816494" target="_blank">��� ��������� ��� ���</a></div><div>����: �����, ������, ����������, ���������, ���������... ����� ���������!</div> <span class="url">playpanel.ru</span></div></td></tr></tbody></table></div></div>


<!-- lightbox scripts -->
	<script type="text/javascript" src="showthread1_files/vbulletin_lightbox.js"></script>
	<script type="text/javascript">
	<!--
	vBulletin.register_control("vB_Lightbox_Container", "posts", 1);
	//-->
	</script>
<!-- / lightbox scripts -->










<!-- next / previous links -->
	<br>
	<div class="smallfont" align="center">
		<strong>�</strong>
			<a href="http://www.programmersforum.ru/showthread.php?t=23477&amp;goto=nextoldest" rel="nofollow">���������� ����</a>
			|
			<a href="http://www.programmersforum.ru/showthread.php?t=23477&amp;goto=nextnewest" rel="nofollow">��������� ����</a>
		<strong>�</strong>
	</div>
<!-- / next / previous links -->







<!-- popup menu contents -->
<br>

<!-- thread tools menu -->
<div class="vbmenu_popup" id="threadtools_menu" style="display: none;">
<form action="postings.php?t=23477&amp;pollid=" method="post" name="threadadminform">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">����� ����<a name="goto_threadtools"></a></td>
	</tr>
	<tr>
		<td class="vbmenu_option"><img title="������ ��� ������" class="inlineimg" src="showthread1_files/printer.gif" alt="������ ��� ������"> <a href="http://www.programmersforum.ru/printthread.php?t=23477" accesskey="3" rel="nofollow">������ ��� ������</a></td>
	</tr>
	
	<tr>
		<td class="vbmenu_option"><img title="��������� �� ����������� �����" class="inlineimg" src="showthread1_files/sendtofriend.gif" alt="��������� �� ����������� �����"> <a href="http://www.programmersforum.ru/sendmessage.php?do=sendtofriend&amp;t=23477" rel="nofollow">��������� �� ����������� �����</a></td>
	</tr>
	
	
	
	
	</tbody></table>
</form>
</div>
<!-- / thread tools menu -->

<!-- **************************************************** -->



<!-- **************************************************** -->



<!-- / popup menu contents -->


<!-- forum rules and admin links -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="bottom">
	<td valign="top" width="100%">
		<table class="tborder" border="0" cellpadding="8" cellspacing="1" width="210">
<thead>
<tr>
	<td class="thead">
		<a style="float: right;" href="#top" onclick="return toggle_collapse('forumrules')"><img id="collapseimg_forumrules" src="showthread1_files/collapse_thead.gif" alt="" border="0"></a>
		���� ����� � �������
	</td>
</tr>
</thead>
<tbody id="collapseobj_forumrules" style="">
<tr>
	<td class="alt1" nowrap="nowrap"><div class="smallfont">
		
		<div>�� <strong>�� ������</strong> ��������� ����� ����</div>
		<div>�� <strong>�� ������</strong> �������� � �����</div>
		<div>�� <strong>�� ������</strong> ����������� ��������</div>
		<div>�� <strong>�� ������</strong> ������������� ���� ���������</div>
		<hr>
		
		<div><a href="http://www.programmersforum.ru/misc.php?do=bbcode" target="_blank">BB ����</a> <strong>���.</strong></div>
		<div><a href="http://www.programmersforum.ru/misc.php?do=showsmilies" target="_blank">������</a> <strong>���.</strong></div>
		<div><a href="http://www.programmersforum.ru/misc.php?do=bbcode#imgcode" target="_blank">[IMG]</a> ��� <strong>���.</strong></div>
		<div>HTML ��� <strong>����.</strong></div>
		<hr>
		<div><a href="http://www.programmersforum.ru/misc.php?do=showrules" target="_blank">Forum Rules</a></div>
	</div></td>
</tr>
</tbody>
</table>
	</td>
	<td class="smallfont" align="right">
		<table border="0" cellpadding="0" cellspacing="0">
		
		<tbody><tr>
			<td>
			<div class="smallfont" style="text-align: left; white-space: nowrap;">
	<form action="forumdisplay.php" method="get">
	<input name="s" value="" type="hidden">
	<input name="daysprune" value="" type="hidden">
	<strong>������� �������</strong><br>
	<select name="f" onchange="this.form.submit();">
		<optgroup label="��������� �� ������">
			<option value="cp">��� �������</option>
			<option value="pm">������ ���������</option>
			<option value="subs">��������</option>
			<option value="wol">��� �� ������</option>
			<option value="search">����� �� ������</option>
			<option value="home">������� �������� ������</option>
		</optgroup>
		
		<optgroup label="�������">
		<option value="45" class="fjdpth0"> ������� � ����������������</option>
<option value="31" class="fjdpth1">&nbsp; &nbsp;  ������ ���������</option>
<option value="7" class="fjdpth1">&nbsp; &nbsp;  �������</option>
<option value="1" class="fjdpth0"> Delphi ����������������</option>
<option value="2" class="fjsel" selected="selected">&nbsp; &nbsp;  ����� ������� Delphi</option>
<option value="47" class="fjdpth1">&nbsp; &nbsp;  ����������� � Delphi</option>
<option value="39" class="fjdpth1">&nbsp; &nbsp;  ���������� Delphi</option>
<option value="3" class="fjdpth1">&nbsp; &nbsp;  ������ � ����� � Delphi</option>
<option value="5" class="fjdpth1">&nbsp; &nbsp;  �� � Delphi</option>
<option value="8" class="fjdpth0"> �������������� ����������������</option>
<option value="4" class="fjdpth1">&nbsp; &nbsp;  Win Api</option>
<option value="9" class="fjdpth1">&nbsp; &nbsp;  Assembler</option>
<option value="13" class="fjdpth0"> C++ ����������������</option>
<option value="14" class="fjdpth1">&nbsp; &nbsp;  ����� ������� C/C++</option>
<option value="40" class="fjdpth1">&nbsp; &nbsp;  ����������������� ����������������</option>
<option value="41" class="fjdpth1">&nbsp; &nbsp;  ���������������� ��� .NET</option>
<option value="10" class="fjdpth0"> Java ����������������</option>
<option value="11" class="fjdpth1">&nbsp; &nbsp;  Java SE</option>
<option value="49" class="fjdpth1">&nbsp; &nbsp;  Java ME</option>
<option value="15" class="fjdpth0"> Web ����������������</option>
<option value="16" class="fjdpth1">&nbsp; &nbsp;  HTML � CSS</option>
<option value="12" class="fjdpth1">&nbsp; &nbsp;  Javascript � ������ ���������� �������</option>
<option value="17" class="fjdpth1">&nbsp; &nbsp;  PHP � ������ ��������� �������</option>
<option value="48" class="fjdpth1">&nbsp; &nbsp;  WordPress � ������ CMS</option>
<option value="32" class="fjdpth0"> ����������</option>
<option value="6" class="fjdpth1">&nbsp; &nbsp;  ������������</option>
<option value="33" class="fjdpth1">&nbsp; &nbsp;  �������� � ���������� ���</option>
<option value="44" class="fjdpth1">&nbsp; &nbsp;  ���� SQL</option>
<option value="34" class="fjdpth1">&nbsp; &nbsp;  ������������ �������</option>
<option value="50" class="fjdpth1">&nbsp; &nbsp;  ��������������</option>
<option value="18" class="fjdpth0"> Microsoft Office � VBA</option>
<option value="20" class="fjdpth1">&nbsp; &nbsp;  Microsoft Office Excel</option>
<option value="21" class="fjdpth1">&nbsp; &nbsp;  Microsoft Office Access</option>
<option value="19" class="fjdpth1">&nbsp; &nbsp;  Microsoft Office Word</option>
<option value="28" class="fjdpth0"> ������ ��� ������������</option>
<option value="30" class="fjdpth1">&nbsp; &nbsp;  ������ �� ���������� ������</option>
<option value="29" class="fjdpth1">&nbsp; &nbsp;  �������</option>
<option value="23" class="fjdpth0"> ���� �������������</option>
<option value="37" class="fjdpth1">&nbsp; &nbsp;  � ������ � ������ �����</option>
<option value="24" class="fjdpth1">&nbsp; &nbsp;  ���������� ������</option>
<option value="25" class="fjdpth1">&nbsp; &nbsp;  ���������� ��������</option>
<option value="46" class="fjdpth1">&nbsp; &nbsp;  ������</option>
<option value="26" class="fjdpth1">&nbsp; &nbsp;  ��������� �������</option>

		</optgroup>
		
	</select><input class="button" value="�����" type="submit">
	</form>
</div>
			</td>
		</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table>
<!-- /forum rules and admin links -->

<br>

<table class="tborder" align="center" border="0" cellpadding="8" cellspacing="1" width="100%">
<thead>
	<tr>
		<td class="tcat" colspan="5" width="100%">
			<a style="float: right;" href="#top" onclick="return toggle_collapse('similarthreads')"><img id="collapseimg_similarthreads" src="showthread1_files/collapse_tcat.gif" alt="" border="0"></a>
			������� ����<a name="similarthreads"></a>
		</td>
	</tr>
</thead>
<tbody id="collapseobj_similarthreads" style="">
<tr class="thead" align="center">
	<td class="thead" width="40%">����</td>
	<td class="thead" nowrap="nowrap" width="15%">�����</td>
	<td class="thead" width="20%">������</td>
	<td class="thead" width="5%">�������</td>
	<td class="thead" width="20%">��������� ���������</td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://www.programmersforum.ru/showthread.php?t=1634" title="����� � ���� ��������, ������� �������. � ��� ����. 
�� ������ ������, ��� ��� ��� ������ ���� ������ � Borland �++.">�������� ������ �� C++</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">Alar</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">�������� � ���������� ���</span></td>
	<td class="alt2" align="center"><span class="smallfont">11</span></td>
	<td class="alt1" align="right"><span class="smallfont">15.03.2010 <span class="time">17:09</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://www.programmersforum.ru/showthread.php?t=11067" title="������ ����,������ ���� �� ����� �������, � ������� ��� ��� ��������� ������, �� � �� ���� ������ ������ ��� ���������������, ���������� ��������...">�������� - ������</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">��������</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">������ ���������</span></td>
	<td class="alt2" align="center"><span class="smallfont">6</span></td>
	<td class="alt1" align="right"><span class="smallfont">09.05.2008 <span class="time">22:09</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://www.programmersforum.ru/showthread.php?t=15541" title="������ ����! ������� ���������� ������ ������� �� ������ �������� ������? ����� ���� �� ������,������ � ��� ������ ... 
������� �������!">x_O ��������-������</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">by -ORTODOX-</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">����� ������� Delphi</span></td>
	<td class="alt2" align="center"><span class="smallfont">1</span></td>
	<td class="alt1" align="right"><span class="smallfont">10.03.2008 <span class="time">20:03</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://www.programmersforum.ru/showthread.php?t=11181" title="��������, �� ��� ����� �������� - ������ �������� ���� 3*3,�������. ��������� ������ �� 2 ������!!!!! ���������� �������� ���� �� ������!!!!!!!">��� ��� �������� - ������.</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">��������</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">������ ���������</span></td>
	<td class="alt2" align="center"><span class="smallfont">2</span></td>
	<td class="alt1" align="right"><span class="smallfont">17.12.2007 <span class="time">20:09</span></span></td>
</tr>

</tbody>
</table>

<br>

<br><center>��� ��������-�������� ������ �� ���������: <a href="http://computers.wikimart.ru/">���������� � ����</a>, <a href="http://books.wikimart.ru/">�������������� ����� � ���������������� ����������</a>, <a href="http://watch.wikimart.ru/wristwatch/">�������� ����</a>.
<br><br>
<div id="mixkt_4294940266" style="height: 110px;"><table id="4294940266_746" border="0" cellpadding="0" cellspacing="0"><tbody><tr> <td> <table bgcolor="#808080" border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr bgcolor="#ffffff" valign="top"><td width="50%"><table cellpadding="5"><tbody><tr>          <td align="center" width="100"><a href="http://www2.mixmarket.biz/clk.php?id=7961348&amp;gid=4294940266&amp;cat=1000479" target="_blank"><img src="showthread1_files/6151204.jpg" border="0" height="100" width="100"></a></td>          <td valign="top">  <center><a href="http://www2.mixmarket.biz/clk.php?id=7961348&amp;gid=4294940266&amp;cat=1000479" target="_blank"> Supra TBS-300"</a> - <span>1800 ���.</span></center>  <small><br><br> <a href="http://www2.mixmarket.biz/clk.php?id=7961348&amp;gid=4294940266&amp;cat=1000479" target="_blank">SUPRAshop</a></small></td>          </tr></tbody></table></td><td width="50%"><table cellpadding="5"><tbody><tr>          <td align="center" width="100"><a href="http://www2.mixmarket.biz/clk.php?id=8004770&amp;gid=4294940266&amp;cat=1000479" target="_blank"><img src="showthread1_files/8004799.jpg" border="0" height="100" width="100"></a></td>          <td valign="top">  <center><a href="http://www2.mixmarket.biz/clk.php?id=8004770&amp;gid=4294940266&amp;cat=1000479" target="_blank">������� ������������� Mundorf M-Coil pin-core F100 0. 22 mH 1. 0 mm</a> - <span>238 ���.</span></center>  <small><br><br><span>������� ������:� 1; �������������,� ���:� 0.�22; �������������,� ��:� 0.�11</span> <a href="http://www2.mixmarket.biz/clk.php?id=8004770&amp;gid=4294940266&amp;cat=1000479" target="_blank">����������</a></small></td>          </tr></tbody></table></td></tr> </tbody></table> </td> </tr></tbody></table><img src="showthread1_files/t_002.gif" height="1" width="1"><div style="width: 0pt; height: 0pt; font-size: 0pt;" id="mixmarket_ktbuff_4294940266_746"></div><div class="kt_footer" style="padding-top: 5px;" id="mix_block_1294942089"><div class="mix_outer mix_m_1294932579"><div class="mix_inter"><div style="margin: 0pt 0pt 0pt 20px; padding: 0pt; text-indent: -20px;">   <span style="white-space: nowrap;"><a href="http://mixmarket.biz/uni/clk.php?id=1294932175&amp;zid=1294942089&amp;s=e3eb&amp;tt=12151150" target="_blank" style="text-decoration: none ! important;" title="����������� ���� ����������"><img src="showthread1_files/99.png" style="vertical-align: middle; border: medium none; margin-right: 4px;" alt=""></a><a href="http://mixmarket.biz/uni/clk.php?id=1294932210&amp;zid=1294942089&amp;s=e3eb&amp;tt=12151150" target="_blank">���������� ������</a></span> <span style="white-space: nowrap;">��� <a href="http://mixmarket.biz/uni/clk.php?id=1294932209&amp;zid=1294942089&amp;s=e3eb&amp;tt=12151150" target="_blank">����� ���������</a></span> </div></div></div><img src="showthread1_files/t.gif" height="1" width="1"></div></div>
</center><br>
<script>
document.write('<scr' + 'ipt language="javascript" type="text/javascript" src="http://4294940266.kt.mixmarket.biz/show/4294940266/&div=mixkt_4294940266&r=' + escape(document.referrer) + '&rnd=' + Math.round(Math.random() * 100000) + '" charset="windows-1251"><' + '/scr' + 'ipt>');
</script><script language="javascript" type="text/javascript" src="showthread1_files/showthread.php%253Ft%253D40078&amp;rnd=94132" charset="windows-1251"></script><script language="javascript" type="text/javascript" src="showthread1_files/showthread.htm" charset="windows-1251"></script><script src="showthread1_files/cnt.js" defer="defer"></script><script src="showthread1_files/cnt.js" defer="defer"></script>

<br>
<div class="smallfont" align="center"><span class="time">11:49</span>.</div>
<br>


		</div>
	</div>
</div>

<!-- / close content container -->
<!-- /content area table -->

<form action="index.php" method="get" style="clear: left;">

<table class="page" align="center" border="0" cellpadding="8" cellspacing="0" width="100%.">
<tbody><tr>
	
		<td class="tfoot">
			<select name="styleid" onchange="switch_id(this, 'style')">
				<optgroup label="����� �����">
					<option value="2" class="" selected="selected">-- club</option>
<option value="1" class="">-- ������� �����</option>

				</optgroup>
			</select>
		</td>
	
	
	<td class="tfoot" align="right" width="100%">
		<div class="smallfont">
			<strong>
				<a href="http://programmersforum.ru/sendmessage.php" rel="nofollow" accesskey="9">�������� �����</a> -
				<a href="http://www.programmersclub.ru/">���� ������������� Delphi</a> -
				
				
				<a href="http://www.programmersforum.ru/archive/index.php">�����</a> -
				
				
				
				<a href="#top" onclick="self.scrollTo(0, 0); return false;">�����</a>
			</strong>
		</div>
	</td>
</tr>
</tbody></table>

<br>

<div align="center">
	<div class="smallfont" align="center">
	<!-- Do not remove this copyright notice -->
	Powered by vBulletin� Version 3.8.5<br>Copyright �2000 - 2010, Jelsoft Enterprises Ltd.
	<!-- Do not remove this copyright notice -->
	</div>

	<div class="smallfont" align="center">
	<!-- Do not remove cronimage or your scheduled tasks will cease to function -->
	
	<!-- Do not remove cronimage or your scheduled tasks will cease to function -->

	
	</div>
</div>

</form>

<br><center>

<a href="http://healpth.com/" title="doctors reviews">Ask Doctor</a> at Healpth.com
<br>
<br>


<!--Rating@Mail.ru COUNTEr--><script language="JavaScript" type="text/javascript"><!--
d=document;var a='';a+=';r='+escape(d.referrer)
js=10//--></script><script language="JavaScript1.1" type="text/javascript"><!--
a+=';j='+navigator.javaEnabled()
js=11//--></script><script language="JavaScript1.2" type="text/javascript"><!--
s=screen;a+=';s='+s.width+'*'+s.height
a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth)
js=12//--></script><script language="JavaScript1.3" type="text/javascript"><!--
js=13//--></script><script language="JavaScript" type="text/javascript"><!--
d.write('<a href="http://top.mail.ru/jump?from=1223103"'+
' target=_top><img src="http://d9.ca.b2.a1.top.list.ru/counter'+
'?id=1223103;t=47;js='+js+a+';rand='+Math.random()+
'" alt="�������@Mail.ru"'+' border=0 height=31 width=88/><\/a>')
if(11<js)d.write('<'+'!-- ')//--></script><a href="http://top.mail.ru/jump?from=1223103" target="_top"><img title="�������@Mail.ru" src="showthread1_files/counter.gif" alt="�������@Mail.ru" border="0" height="31" width="88/"></a><!-- <noscript><a
target=_top href="http://top.mail.ru/jump?from=1223103"><img
src="http://d9.ca.b2.a1.top.list.ru/counter?js=na;id=1223103;t=47"
border=0 height=31 width=88
alt="�������@Mail.ru"/></a></noscript><script language="JavaScript" type="text/javascript"><!--
if(11<js)d.write('--'+'>')//--><!--/COUNTER-->

<style type="text/css">a img {border: none;}</style>
<a href="http://programmersforum.ru/showthread.php?t=114639"><img src="showthread1_files/donateforumfooter.gif"></a>




<script type="text/javascript">
<!--
	// Main vBulletin Javascript Initialization
	vBulletin_init();
//-->
</script>

<!-- temp -->
<div style="display: none;">
	<!-- thread rate -->
	
		
	
	<!-- / thread rate -->
</div>

</center><div class="GleeThemeDefault" id="gleeBox" style="display: none; top: 35%;"><input style="font-size: 50px;" class="GleeThemeDefault" id="gleeSearchField" type="text"><div id="gleeSub"><div id="gleeSubText">Nothing selected</div><div id="gleeSubActivity"></div><div id="gleeSubURL"></div></div></div></body></html>