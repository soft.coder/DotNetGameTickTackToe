<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="ru"><head>


	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<meta name="generator" content="vBulletin 3.8.5">

<meta name="keywords" content=" ��������-������, �����, �����������, delphi, �����, ����������������, ���������, assembler, winapi, c++, html, php">
<meta name="description" content=" ��������-������ �������� � ���������� ���">


<!-- CSS Stylesheet -->
<style type="text/css" id="vbulletin_css">
/**
* vBulletin 3.8.5 CSS
* Style: 'club'; Style ID: 2
*/
body
{
	background: #FFFFFF;
	color: #000000;
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	margin: 5px 10px 10px 10px;
	padding: 0px;
}
a:link, body_alink
{
	color: #1C3289;
}
a:visited, body_avisited
{
	color: #1C3289;
}
a:hover, a:active, body_ahover
{
	color: #000000;
}
.page
{
	background: #FFFFFF;
	color: #000000;
}
td, th, p, li
{
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.tborder
{
	background: #5C5E5C;
	color: #000000;
	border: 1px solid #5C5E5C;
}
.tcat
{
	background: #5C5E5C;
	color: #E4DEE4;
	font: bold 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.tcat a:link, .tcat_alink
{
	color: #E4DEE4;
	text-decoration: none;
}
.tcat a:visited, .tcat_avisited
{
	color: #E4DEE4;
	text-decoration: none;
}
.tcat a:hover, .tcat a:active, .tcat_ahover
{
	color: #E4DEE4;
	text-decoration: underline;
}
.thead
{
	background: #D4D2CC;
	color: #000000;
	font: bold 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.thead a:link, .thead_alink
{
	color: #1C3289;
	text-decoration: none;
}
.thead a:visited, .thead_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.thead a:hover, .thead a:active, .thead_ahover
{
	color: #000000;
	text-decoration: underline;
}
.tfoot
{
	background: #5C5E5C;
	color: #E4DEE4;
}
.tfoot a:link, .tfoot_alink
{
	color: #E4DEE4;
}
.tfoot a:visited, .tfoot_avisited
{
	color: #E4DEE4;
}
.tfoot a:hover, .tfoot a:active, .tfoot_ahover
{
	color: #E4DEE4;
}
.alt1, .alt1Active
{
	background: #FFFFFF;
	color: #000000;
}
.alt1 a:link, .alt1_alink, .alt1Active a:link, .alt1Active_alink
{
	color: #1C3289;
	text-decoration: none;
}
.alt1 a:visited, .alt1_avisited, .alt1Active a:visited, .alt1Active_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.alt1 a:hover, .alt1 a:active, .alt1_ahover, .alt1Active a:hover, .alt1Active a:active, .alt1Active_ahover
{
	color: #4B4A4A;
	text-decoration: underline;
}
.alt2, .alt2Active
{
	background: #D4D0C8;
	color: #000000;
}
.alt2 a:link, .alt2_alink, .alt2Active a:link, .alt2Active_alink
{
	color: #1C3289;
	text-decoration: none;
}
.alt2 a:visited, .alt2_avisited, .alt2Active a:visited, .alt2Active_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.alt2 a:hover, .alt2 a:active, .alt2_ahover, .alt2Active a:hover, .alt2Active a:active, .alt2Active_ahover
{
	color: #4B4A4A;
	text-decoration: underline;
}
.inlinemod
{
	background: #D4D2CC;
	color: #000000;
}
.wysiwyg
{
	background: #D3D3D2;
	color: #000000;
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
textarea, .bginput
{
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.bginput option, .bginput optgroup
{
	font-size: 10pt;
	font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.button
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
select
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
option, optgroup
{
	font-size: 11px;
	font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.smallfont
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.time
{
	color: #000000;
}
.navbar
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.highlight
{
	color: #FF0000;
	font-weight: bold;
}
.fjsel
{
	background: #9E9E9C;
	color: #000000;
}
.fjdpth0
{
	background: #F7F7F7;
	color: #000000;
}
.panel
{
	background: #D4D2CC;
	color: #000000;
	padding: 10px;
	border: 2px outset;
}
.panelsurround
{
	background: #9E9E9C;
	color: #000000;
}
legend
{
	color: #000000;
	font: 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.vbmenu_control
{
	background: #5C5E5C;
	color: #FFFFFF;
	font: bold 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	padding: 3px 6px 3px 6px;
	white-space: nowrap;
}
.vbmenu_control a:link, .vbmenu_control_alink
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_control a:visited, .vbmenu_control_avisited
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_control a:hover, .vbmenu_control a:active, .vbmenu_control_ahover
{
	color: #FFFFFF;
	text-decoration: underline;
}
.vbmenu_popup
{
	background: #5C5E5C;
	color: #FFFFFF;
	border: 1px solid #D4D2CC;
}
.vbmenu_option
{
	background: #D4D2CC;
	color: #000000;
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	white-space: nowrap;
	cursor: pointer;
}
.vbmenu_option a:link, .vbmenu_option_alink
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_option a:visited, .vbmenu_option_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_option a:hover, .vbmenu_option a:active, .vbmenu_option_ahover
{
	color: #000000;
	text-decoration: none;
}
.vbmenu_hilite
{
	background: #FFFFCC;
	color: #000000;
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	white-space: nowrap;
	cursor: pointer;
}
.vbmenu_hilite a:link, .vbmenu_hilite_alink
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_hilite a:visited, .vbmenu_hilite_avisited
{
	color: #1C3289;
	text-decoration: none;
}
.vbmenu_hilite a:hover, .vbmenu_hilite a:active, .vbmenu_hilite_ahover
{
	color: #000000;
	text-decoration: none;
}
/* ***** styling for 'big' usernames on postbit etc. ***** */
.bigusername { font-size: 14pt; }

/* ***** small padding on 'thead' elements ***** */
td.thead, th.thead, div.thead { padding: 4px; }

/* ***** basic styles for multi-page nav elements */
.pagenav a { text-decoration: none; }
.pagenav td { padding: 2px 4px 2px 4px; }

/* ***** de-emphasized text */
.shade, a.shade:link, a.shade:visited { color: #777777; text-decoration: none; }
a.shade:active, a.shade:hover { color: #FF4400; text-decoration: underline; }
.tcat .shade, .thead .shade, .tfoot .shade { color: #DDDDDD; }

/* ***** define margin and font-size for elements inside panels ***** */
.fieldset { margin-bottom: 6px; }
.fieldset, .fieldset td, .fieldset p, .fieldset li { font-size: 11px; }
</style>
<link rel="stylesheet" type="text/css" href="showthread2_files/vbulletin_important.css">


<!-- / CSS Stylesheet -->

<script type="text/javascript" src="showthread2_files/yahoo-dom-event.js"></script>
<script type="text/javascript" src="showthread2_files/connection-min.js"></script>
<script type="text/javascript">
<!--
var SESSIONURL = "";
var SECURITYTOKEN = "guest";
var IMGDIR_MISC = "images/1070/misc";
var vb_disable_ajax = parseInt("0", 10);
// -->
</script>
<script type="text/javascript" src="showthread2_files/vbulletin_global.js"></script>
<script type="text/javascript" src="showthread2_files/vbulletin_menu.js"></script>


	<link rel="alternate" type="application/rss+xml" title="����� ������������� RSS Feed" href="http://programmersforum.ru/external.php?type=RSS2">
	
		<link rel="alternate" type="application/rss+xml" title="����� ������������� - �������� � ���������� ��� - RSS Feed" href="http://programmersforum.ru/external.php?type=RSS2&amp;forumids=33">
	

	    <title> ��������-������ - �������� � ���������� ���  - ����� �������������</title>
	<script type="text/javascript" src="showthread2_files/vbulletin_post_loader.js"></script>
	<style type="text/css" id="vbulletin_showthread_css">
	<!--
	
	#links div { white-space: nowrap; }
	#links img { vertical-align: middle; }
	-->
	</style>
<style type="text/css">.GleeThemeDefault{ background-color:#333 !important; color:#fff !important; font-family: Calibri, "Lucida Grande", Lucida, Arial, sans-serif !important; } .GleeThemeWhite{ background-color:#fff !important; color:#000 !important; border: 1px solid #939393 !important; -moz-border-radius: 10px !important; font-family: Calibri, "Lucida Grande", Lucida, Arial, sans-serif !important; } #gleeBox.GleeThemeWhite{ opacity:0.75; } #gleeSearchField.GleeThemeWhite{ border: 1px solid #939393 !important; } .GleeThemeRuby{ background-color: #530000 !important; color: #f6b0ab !important; font-family: "Lucida Grande", Lucida, Verdana, sans-serif !important; } .GleeThemeGreener{ background-color: #2e5c4f !important; color: #d3ff5a !important; font-family: Georgia, "Times New Roman", Times, serif !important; } .GleeThemeConsole{ font-family: Monaco, Consolas, "Courier New", Courier, mono !important; color: #eafef6 !important; background-color: #111 !important; } .GleeThemeGlee{ background-color: #eb1257 !important; color: #fff300 !important; -webkit-box-shadow: #eb1257 0px 0px 8px !important; -moz-box-shadow: #eb1257 0px 0px 8px !important; opacity: 0.8 !important; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif !important; }.GleeReaped{ background-color: #fbee7e !important; border: 1px dotted #818181 !important; } .GleeHL{ background-color: #d7fe65 !important; -webkit-box-shadow: rgb(177, 177, 177) 0px 0px 9px !important; -moz-box-shadow: rgb(177, 177, 177) 0px 0px 9px !important; padding: 3px !important; color: #1c3249 !important; border: 1px solid #818181 !important; } .GleeHL a{ color: #1c3249 !important; }#gleeBox{ line-height:20px; height:auto !important; z-index:100000; position:fixed; left:5%; top:35%; display:none; overflow:auto; width:90%; background-color:#333; opacity:0.65; color:#fff; margin:0; font-family: Calibri, "Lucida Grande", Lucida, Arial, sans-serif; padding:4px 6px; text-align:left; /*rounded corners*/ -moz-border-radius:7px; } #gleeSearchField{ outline:none; -moz-box-shadow:none; width:90%; margin:0; background:none !important; padding:0; margin:3px 0; border:none !important; height:auto !important; font-size:100px; color:#fff; } #gleeSub{ font-size:15px !important; font-family:inherit !important; font-weight:normal !important; height:auto !important; margin:0 !important; padding:0 !important; color:inherit !important; line-height:normal !important; }#gleeSubText, #gleeSubURL, #gleeSubActivity{ width:auto !important; font-size:inherit !important; color:inherit !important; font-family:inherit !important; line-height:20px !important; font-weight:normal !important; } #gleeSubText{ float:left; } #gleeSubURL{ display:inline; float:right; } #gleeSubActivity{ height:10px; display:inline; float:left; padding-left:5px; }</style></head><body onload="if (document.body.scrollIntoView &amp;&amp; (window.location.href.indexOf('#') == -1 || window.location.href.indexOf('#post') > -1)) { fetch_object('currentPost').scrollIntoView(true); }"><script src="showthread2_files/5778" defer="defer" async="" type="text/javascript"></script>
<!-- logo -->
<a name="top"></a>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%.">
<tbody><tr>
	<td align="left"><a href="http://programmersforum.ru/index.php"><img title="����� �������������" src="showthread2_files/vbulletin3_logo_white.gif" alt="����� �������������" border="0"></a></td>
	<td id="header_right_cell" align="right">
		<script type="text/javascript"><!--
google_ad_client = "pub-3780994971058297";
/* 468x60, ������� 29.10.09 */
google_ad_slot = "2537596968";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript" src="showthread2_files/show_ads.js">
</script><script src="showthread2_files/show_ads_impl.js"></script><script src="showthread2_files/expansion_embed.js"></script><script src="showthread2_files/test_domain.js"></script><script>google_protectAndRun("ads_core.google_render_ad", google_handleError, google_render_ad);</script><ins style="display: inline-table; border: medium none; height: 60px; margin: 0pt; padding: 0pt; position: relative; visibility: visible; width: 468px;"><ins id="google_ads_frame1_anchor" style="display: block; border: medium none; height: 60px; margin: 0pt; padding: 0pt; position: relative; visibility: visible; width: 468px;"><iframe allowtransparency="true" hspace="0" id="google_ads_frame1" marginheight="0" marginwidth="0" name="google_ads_frame" src="showthread2_files/ads.htm" style="left: 0pt; position: absolute; top: 0pt;" vspace="0" scrolling="no" frameborder="0" height="60" width="468"></iframe></ins></ins>

	</td>
</tr>
</tbody></table>
<!-- /logo -->

<!-- content table -->
<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">




<style>
A { text-decoration: none; }
A:hover { text-decoration: underline;  }
    </style><b>
<a href="http://programmersclub.ru/">�������</a>
&nbsp;|&nbsp;
<a href="http://programmersforum.ru/rules.php">������� ������</a>
&nbsp;|&nbsp;
<a href="http://www.programmersclub.ru/lab">��������� Delphi</a>
&nbsp;|&nbsp;
<a href="http://www.delphibasics.ru/">������ Delphi</a>
&nbsp;|&nbsp;
<a href="http://www.pblog.ru/">���� �������������</a>
&nbsp;|&nbsp;
<a href="http://programmersclub.ru/category/%D1%80%D0%B0%D1%81%D1%81%D1%8B%D0%BB%D0%BA%D0%B0/">��������</a>
&nbsp;|&nbsp;
<noindex><nofollow>
<a href="http://programmersforum.printdirect.ru/">������ ��������!</a>
</nofollow></noindex>
</b>
<br><br><center>
<!--  AdRiver code START. Type:728x90 Site: prgclub PZ: 0 BN: 1 -->
<script language="javascript" type="text/javascript"><!--
var RndNum4NoCash = Math.round(Math.random() * 1000000000);
var ar_Tail='unknown'; if (document.referrer) ar_Tail = escape(document.referrer);
document.write(
'<iframe src="http://ad.adriver.ru/cgi-bin/erle.cgi?'
+ 'sid=162574&bn=1&target=blank&bt=36&pz=0&rnd=' + RndNum4NoCash + '&tail256=' + ar_Tail
+ '" frameborder=0 vspace=0 hspace=0 width=728 height=90 marginwidth=0'
+ ' marginheight=0 scrolling=no></iframe>');
//--></script><iframe src="showthread2_files/erle.htm" vspace="0" hspace="0" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" height="90" width="728"></iframe>
<noscript>
<a href="http://ad.adriver.ru/cgi-bin/click.cgi?sid=162574&bn=1&bt=36&pz=0&rnd=911497674" target=_blank>
<img src="http://ad.adriver.ru/cgi-bin/rle.cgi?sid=162574&bn=1&bt=36&pz=0&rnd=911497674" alt="-AdRiver-" border=0 width=728 height=90></a>
</noscript>
<!--  AdRiver code END  -->
</center>
<br>

<!-- breadcrumb, login, pm info -->
<table class="tborder" align="center" border="0" cellpadding="8" cellspacing="1" width="100%">
<tbody><tr>
	<td class="alt1" width="100%">
		
			<table border="0" cellpadding="0" cellspacing="0">
			<tbody><tr valign="bottom">
				<td><a href="#" onclick="history.back(1); return false;"><img title="���������" src="showthread2_files/navbits_start.gif" alt="���������" border="0"></a></td>
				<td>&nbsp;</td>
				<td width="100%"><span class="navbar"><a href="http://programmersforum.ru/index.php" accesskey="1">����� �������������</a></span> 
	<span class="navbar">&gt; <a href="http://programmersforum.ru/forumdisplay.php?f=32">����������</a></span>


	<span class="navbar">&gt; <a href="http://programmersforum.ru/forumdisplay.php?f=33">�������� � ���������� ���</a></span>

</td>
			</tr>
			<tr>
				<td class="navbar" style="font-size: 10pt; padding-top: 1px;" colspan="3"><a href="http://programmersforum.ru/showthread.php?p=174381"><img title="������������� ��������" class="inlineimg" src="showthread2_files/navbits_finallink_ltr.gif" alt="������������� ��������" border="0"></a> <strong>
	 ��������-������

</strong></td>
			</tr>
			</tbody></table>
		
	</td>

	<td class="alt2" style="padding: 0px;" nowrap="nowrap">
		<!-- login form -->
		<form action="login.php?do=login" method="post" onsubmit="md5hash(vb_login_password, vb_login_md5password, vb_login_md5password_utf, 0)">
		<script type="text/javascript" src="showthread2_files/vbulletin_md5.js"></script>
		<table border="0" cellpadding="0" cellspacing="3">
		<tbody><tr>
			<td class="smallfont" style="white-space: nowrap;"><label for="navbar_username">���</label></td>
			<td><input class="bginput" style="font-size: 11px;" name="vb_login_username" id="navbar_username" size="10" accesskey="u" tabindex="101" value="���" onfocus="if (this.value == '���') this.value = '';" type="text"></td>
			<td class="smallfont" nowrap="nowrap"><label for="cb_cookieuser_navbar"><input name="cookieuser" value="1" tabindex="103" id="cb_cookieuser_navbar" accesskey="c" type="checkbox">���������?</label></td>
		</tr>
		<tr>
			<td class="smallfont"><label for="navbar_password">������</label></td>
			<td><input class="bginput" style="font-size: 11px;" name="vb_login_password" id="navbar_password" size="10" tabindex="102" type="password"></td>
			<td><input class="button" value="����" tabindex="104" title="������� ���� ��� ������������ � ������, ����� �����, ��� ������� ������ '�����������', ����� ������������������." accesskey="s" type="submit"></td>
		</tr>
		</tbody></table>
		<input name="s" value="" type="hidden">
		<input name="securitytoken" value="guest" type="hidden">
		<input name="do" value="login" type="hidden">
		<input name="vb_login_md5password" type="hidden">
		<input name="vb_login_md5password_utf" type="hidden">
		</form>
		<!-- / login form -->
	</td>

</tr>
</tbody></table>
<!-- / breadcrumb, login, pm info -->

<!-- nav buttons bar -->
<div class="tborder" style="padding: 1px; border-top-width: 0px;">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr align="center">
		
		
			<td class="vbmenu_control"><a href="http://programmersforum.ru/register.php" rel="nofollow">�����������</a></td>
		
		
		<td class="vbmenu_control"><a rel="help" href="http://programmersforum.ru/faq.php" accesskey="5">�������</a></td>
		
			<td class="vbmenu_control"><a style="cursor: pointer;" id="community" href="http://programmersforum.ru/showthread.php?p=174381&amp;nojs=1#community" rel="nofollow" accesskey="6">���������� <img alt="" title="" src="showthread2_files/menu_open.gif" border="0"></a> <script type="text/javascript"> vbmenu_register("community"); </script></td>
		
		<td class="vbmenu_control"><a href="http://programmersforum.ru/calendar.php">���������</a></td>
		
			
				
				<td class="vbmenu_control"><a href="http://programmersforum.ru/search.php?do=getdaily" accesskey="2">��������� �� ����</a></td>
				
				<td class="vbmenu_control"><a id="navbar_search" href="http://programmersforum.ru/search.php" accesskey="4" rel="nofollow">�����</a> </td>
			
			
		
		
		
		</tr>
	</tbody></table>
</div>
<!-- / nav buttons bar -->

<br>






<!-- NAVBAR POPUP MENUS -->

	
	<!-- community link menu -->
	<div class="vbmenu_popup" id="community_menu" style="display: none; margin-top: 3px;" align="left">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr><td class="thead">�����</td></tr>
		
		
		
		
		
			<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/memberlist.php">������������</a></td></tr>
		
		
		</tbody></table>
	</div>
	<!-- / community link menu -->
	
	
	
	<!-- header quick search form -->
	<div class="vbmenu_popup" id="navbar_search_menu" style="display: none; margin-top: 3px;" align="left">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead">����� �� ������</td>
		</tr>
		<tr>
			<td class="vbmenu_option" title="nohilite">
				<form action="search.php?do=process" method="post">

					<input name="do" value="process" type="hidden">
					<input name="quicksearch" value="1" type="hidden">
					<input name="childforums" value="1" type="hidden">
					<input name="exactname" value="1" type="hidden">
					<input name="s" value="" type="hidden">
					<input name="securitytoken" value="guest" type="hidden">
					<div><input class="bginput" name="query" size="25" tabindex="1001" type="text"><input class="button" value="�����" tabindex="1004" type="submit"></div>
					<div style="margin-top: 8px;">
						<label for="rb_nb_sp0"><input name="showposts" value="0" id="rb_nb_sp0" tabindex="1002" checked="checked" type="radio">���������� ����</label>
						&nbsp;
						<label for="rb_nb_sp1"><input name="showposts" value="1" id="rb_nb_sp1" tabindex="1003" type="radio">���������� ���������</label>
					</div>
				</form>
			</td>
		</tr>
		
		<tr>
			<td class="vbmenu_option"><a href="http://programmersforum.ru/search.php" accesskey="4" rel="nofollow">����������� �����</a></td>
		</tr>
		
		</tbody></table>
	</div>
	<!-- / header quick search form -->
	
	
	
<!-- / NAVBAR POPUP MENUS -->

<!-- PAGENAV POPUP -->
	<div class="vbmenu_popup" id="pagenav_menu" style="display: none; position: absolute; z-index: 50;">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead" nowrap="nowrap">� ��������...</td>
		</tr>
		<tr>
			<td class="vbmenu_option" title="">
			<form action="index.php" method="get" onsubmit="return this.gotopage()" id="pagenav_form">
				<input class="bginput" id="pagenav_itxt" style="font-size: 11px;" size="4" type="text">
				<input class="button" id="pagenav_ibtn" value="�����" type="button">
			</form>
			</td>
		</tr>
		</tbody></table>
	</div>
<!-- / PAGENAV POPUP -->










<a name="poststop" id="poststop"></a>

<!-- controls above postbits -->
<table style="margin-bottom: 3px;" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="bottom">
	
		<td class="smallfont"><a href="http://programmersforum.ru/newreply.php?do=newreply&amp;noquote=1&amp;p=174381" rel="nofollow"><img title="�����" src="showthread2_files/reply.gif" alt="�����" border="0"></a></td>
	
	<td align="right"><div class="pagenav" align="right">
<table class="tborder" border="0" cellpadding="3" cellspacing="1">
<tbody><tr>
	<td class="vbmenu_control" style="font-weight: normal;">�������� 1 �� 2</td>
	
	
		<td class="alt2"><span class="smallfont" title="�������� � 1 �� 10 �� 20."><strong>1</strong></span></td>
 <td class="alt1"><a class="smallfont" href="http://programmersforum.ru/showthread.php?t=33566&amp;page=2" title="� 11 �� 20 �� 20">2</a></td>
	<td class="alt1"><a rel="next" class="smallfont" href="http://programmersforum.ru/showthread.php?t=33566&amp;page=2" title="��������� �������� - � 11 �� 20 �� 20">&gt;</a></td>
	
	<td style="cursor: pointer;" id="pagenav.33" class="vbmenu_control" title=""> <img alt="" title="" src="showthread2_files/menu_open.gif" border="0"></td>
</tr>
</tbody></table>
</div></td>
</tr>
</tbody></table>
<!-- / controls above postbits -->


	 	

<!-- toolbar -->
<table class="tborder" style="border-bottom-width: 0px;" align="center" border="0" cellpadding="8" cellspacing="1" width="100%">
<tbody><tr>
	<td class="tcat" width="100%">
		<div class="smallfont">
		
		&nbsp;
		</div>
	</td>
	<td style="cursor: pointer;" class="vbmenu_control" id="threadtools" nowrap="nowrap">
		<a href="http://programmersforum.ru/showthread.php?p=174381&amp;nojs=1#goto_threadtools">����� ����</a>
		<script type="text/javascript"> vbmenu_register("threadtools"); </script> <img alt="" title="" src="showthread2_files/menu_open.gif" border="0">
	</td>
	
	
	

	

</tr>
</tbody></table>
<!-- / toolbar -->



<!-- end content table -->

		</div>
	</div>
</div>

<!-- / close content container -->
<!-- / end content table -->





<div id="posts"><!-- post #174381 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit174381" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post174381" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);" id="currentPost">
			<!-- status icon and date -->
			<a name="post174381"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			20.12.2008, 20:20
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=174381&amp;postcount=1" target="new" rel="nofollow" id="postcount174381" name="1"><strong>1</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_174381">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=27367">ROD</a>
				<script type="text/javascript"> vbmenu_register("postmenu_174381", true); </script>
				
			</div>

			<div class="smallfont">����������</div>
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=27367"><img title="������ ��� ROD" src="showthread2_files/image_002.jpg" alt="������ ��� ROD" border="0" height="76" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 30.11.2008</div>
				<div>�����: 127.0.0.1</div>
				
				<div>
					���������: 2,151
				</div>
				
				
				<div><span id="repdisplay_174381_27367">���������: 412</span></div>
				
				<div><a href="#" onclick="imwindow('icq', '27367', 500, 450); return false;"><img title="��������� ��������� ��� ROD � ������� ICQ" src="showthread2_files/im_icq.gif" alt="��������� ��������� ��� ROD � ������� ICQ" border="0"></a>    <a href="#" onclick="imwindow('skype', '27367', 400, 285); return false;"><img title="��������� ��������� ��� ROD � ������� Skype�" src="showthread2_files/russian_pinky.png" alt="��������� ��������� ��� ROD � ������� Skype�" border="0"></a></div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_174381" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				<strong>��������-������</strong>
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_174381">
			
			������� �������, ���� ������.<br>
<br>
<b>��������� �������:</b><br>
<br>
�: ��� � ��� ������ �����?<br>
�: � ������ ���� ��������� ����, � ��� ���������.<br>
<br>
�: ��� ������� ��������� ������.<br>
�: � ������, ������� ����� �������.<br>
<br>
�: ����� ������� �������������� ��� ����������?<br>
�: �� � ������� ������� ����������� � � �, � ����� �������� (����� ��� 
��� ������, ��� � ��� ��) ���������� ����� �� ����� ��� ������� ��� ���.<br>
<br>
�: � ����� ������ � ���������<br>
�: �������. �������, ����������, ��� �� �� ��������.
		</div>
		<!-- / message -->

		

		
		

		

		

		
		<!-- edit note -->
			<div class="smallfont">
				<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
				<em>
					
						��������� ��� ��������������� ROD; 25.12.2008 � <span class="time">22:04</span>.
					
					
				</em>
			</div>
		<!-- / edit note -->
		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="ROD ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="ROD ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=174381" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 174381 popup menu -->
<div class="vbmenu_popup" id="postmenu_174381_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">ROD</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=27367">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=27367" rel="nofollow">����� ��� ��������� �� ROD</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 174381 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #174381 --><script type="text/javascript" src="showthread2_files/highlight.js"></script>
<script type="text/javascript">hljs.initHighlightingOnLoad('delphi', 'cpp', 'css', 'html', 'javascript', 'java', 'php');</script><script type="text/javascript" src="showthread2_files/static.js"></script><script type="text/javascript" src="showthread2_files/www.js"></script><script type="text/javascript" src="showthread2_files/javascript.js"></script><script type="text/javascript" src="showthread2_files/dynamic.js"></script>

<link rel="stylesheet" href="showthread2_files/programmersforum.css">
<!-- post #174718 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit174718" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post174718" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post174718"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			21.12.2008, 14:46
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=174718&amp;postcount=2" target="new" rel="nofollow" id="postcount174718" name="2"><strong>2</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_174718">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=27367">ROD</a>
				<script type="text/javascript"> vbmenu_register("postmenu_174718", true); </script>
				
			</div>

			<div class="smallfont">����������</div>
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=27367"><img title="������ ��� ROD" src="showthread2_files/image_002.jpg" alt="������ ��� ROD" border="0" height="76" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 30.11.2008</div>
				<div>�����: 127.0.0.1</div>
				
				<div>
					���������: 2,151
				</div>
				
				
				<div><span id="repdisplay_174718_27367">���������: 412</span></div>
				
				<div><a href="#" onclick="imwindow('icq', '27367', 500, 450); return false;"><img title="��������� ��������� ��� ROD � ������� ICQ" src="showthread2_files/im_icq.gif" alt="��������� ��������� ��� ROD � ������� ICQ" border="0"></a>    <a href="#" onclick="imwindow('skype', '27367', 400, 285); return false;"><img title="��������� ��������� ��� ROD � ������� Skype�" src="showthread2_files/russian_pinky.png" alt="��������� ��������� ��� ROD � ������� Skype�" border="0"></a></div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_174718" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_174718">
			
			���������: <br>
����� ��������� ������.<br>
������������ ��������� , ���� � ���� ����� 3 �� ����������  �������.
		</div>
		<!-- / message -->

		
		<!-- attachments -->
			<div style="padding: 8px;">

			

			

			

			
				<fieldset class="fieldset">
					<legend>��������</legend>
					<table border="0" cellpadding="0" cellspacing="3">
					<tbody><tr>
	<td><img title="��� �����: rar" class="inlineimg" src="showthread2_files/rar.gif" alt="��� �����: rar" style="vertical-align: baseline;" border="0" height="16" width="16"></td>
	<td><a href="http://programmersforum.ru/attachment.php?attachmentid=7366&amp;d=1229859916">��������-������ V1.1.rar</a> (33.4 ��, 197 ����������)</td>
</tr>
					</tbody></table>
				</fieldset>
			

			

			</div>
		<!-- / attachments -->
		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="ROD ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="ROD ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=174718" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 174718 popup menu -->
<div class="vbmenu_popup" id="postmenu_174718_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">ROD</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=27367">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=27367" rel="nofollow">����� ��� ��������� �� ROD</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 174718 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #174718 --><!-- post #190820 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit190820" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post190820" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post190820"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			22.01.2009, 09:32
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=190820&amp;postcount=3" target="new" rel="nofollow" id="postcount190820" name="3"><strong>3</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_190820">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=31991">Prikol</a>
				<script type="text/javascript"> vbmenu_register("postmenu_190820", true); </script>
				
			</div>

			<div class="smallfont">�������</div>
			
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 18.01.2009</div>
				
				
				<div>
					���������: 2
				</div>
				
				
				<div><span id="repdisplay_190820_31991">���������: 2</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_190820" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_190820">
			
			��� ��������� ����� !)) ���� ) ���� �� ������� ....
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Prikol ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="Prikol ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=190820" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 190820 popup menu -->
<div class="vbmenu_popup" id="postmenu_190820_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Prikol</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=31991">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=31991" rel="nofollow">����� ��� ��������� �� Prikol</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 190820 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #190820 --><!-- post #191130 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191130" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post191130" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191130"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			22.01.2009, 17:48
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191130&amp;postcount=4" target="new" rel="nofollow" id="postcount191130" name="4"><strong>4</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191130">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=27367">ROD</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191130", true); </script>
				
			</div>

			<div class="smallfont">����������</div>
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=27367"><img title="������ ��� ROD" src="showthread2_files/image_002.jpg" alt="������ ��� ROD" border="0" height="76" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 30.11.2008</div>
				<div>�����: 127.0.0.1</div>
				
				<div>
					���������: 2,151
				</div>
				
				
				<div><span id="repdisplay_191130_27367">���������: 412</span></div>
				
				<div><a href="#" onclick="imwindow('icq', '27367', 500, 450); return false;"><img title="��������� ��������� ��� ROD � ������� ICQ" src="showthread2_files/im_icq.gif" alt="��������� ��������� ��� ROD � ������� ICQ" border="0"></a>    <a href="#" onclick="imwindow('skype', '27367', 400, 285); return false;"><img title="��������� ��������� ��� ROD � ������� Skype�" src="showthread2_files/russian_pinky.png" alt="��������� ��������� ��� ROD � ������� Skype�" border="0"></a></div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191130" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191130">
			
			����� ������� ��� � ������� (�� ������) ����� ��������� � ����������  � ������� "������ ���������".<br>
<br>
� ���... � ���� ��� �� ���������� ������ ������ � ����� ������ ������, � ����� ������� �� ����� ������ ������ �� �����������.
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="ROD ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="ROD ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191130" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191130 popup menu -->
<div class="vbmenu_popup" id="postmenu_191130_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">ROD</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=27367">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=27367" rel="nofollow">����� ��� ��������� �� ROD</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191130 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191130 --><!-- post #191149 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191149" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post191149" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191149"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			22.01.2009, 18:07
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191149&amp;postcount=5" target="new" rel="nofollow" id="postcount191149" name="5"><strong>5</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191149">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=19542">Arigato</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191149", true); </script>
				
			</div>

			<div class="smallfont">WEB-�����������</div>
			<div class="smallfont">����������� ���������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=19542"><img title="������ ��� Arigato" src="showthread2_files/image_003.jpg" alt="������ ��� Arigato" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 27.07.2008</div>
				<div>�����: ������</div>
				
				<div>
					���������: 3,876
				</div>
				
				
				<div><span id="repdisplay_191149_19542">���������: 1224</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191149" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191149">
			
			� ���� ����������? ������ ������������ ��������-������ �� ���� 3�3 - 
���� �� ����������. ������������ ����������� ����� ������� ������ 
���������� �����������, � ��� ���� ���� ����� ����������� ������ �������
 �����.<br>
����� ������� ��� �������� ��� �����, ��� �������, ��� ��������� ����� 
����, ���� ��� ���� ���, ���� ����� ����������, ��� ��������-������.
		</div>
		<!-- / message -->

		

		
		

		
		<!-- sig -->
			<div>
				__________________<br>
				<font size="1"><i>��� ���� ���� ������, ��� �� �����...</i></font>
			</div>
		<!-- / sig -->
		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Arigato ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="Arigato ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191149" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191149 popup menu -->
<div class="vbmenu_popup" id="postmenu_191149_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Arigato</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=19542">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=19542" rel="nofollow">����� ��� ��������� �� Arigato</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191149 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191149 --><!-- post #191405 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191405" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post191405" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191405"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			23.01.2009, 00:07
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191405&amp;postcount=6" target="new" rel="nofollow" id="postcount191405" name="6"><strong>6</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191405">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=31241">SaiLight</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191405", true); </script>
				
			</div>

			
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=31241"><img title="������ ��� SaiLight" src="showthread2_files/image.png" alt="������ ��� SaiLight" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 10.01.2009</div>
				<div>�����: � ����.</div>
				
				<div>
					���������: 49
				</div>
				
				
				<div><span id="repdisplay_191405_31241">���������: 40</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191405" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191405">
			
			<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">������:</div>
	<table border="0" cellpadding="8" cellspacing="0" width="100%">
	<tbody><tr>
		<td class="alt2" style="border: 1px inset;">
			
				<div>
					��������� �� <strong>ROD</strong>
					<a href="http://programmersforum.ru/showthread.php?p=174381#post174381" rel="nofollow"><img title="���������� ���������" class="inlineimg" src="showthread2_files/viewpost.gif" alt="���������� ���������" border="0"></a>
				</div>
				<div style="font-style: italic;">�: ��� ������� ��������� ������.<br>
�: � ������, ������� ����� �������.</div>
			
		</td>
	</tr>
	</tbody></table>
</div>������� ��������� ������ ������ ����� � ���������� ��������� ������� �����.
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="SaiLight ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="SaiLight ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191405" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191405 popup menu -->
<div class="vbmenu_popup" id="postmenu_191405_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">SaiLight</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=31241">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=31241" rel="nofollow">����� ��� ��������� �� SaiLight</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191405 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191405 --><!-- post #191520 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191520" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post191520" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191520"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			23.01.2009, 10:45
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191520&amp;postcount=7" target="new" rel="nofollow" id="postcount191520" name="7"><strong>7</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191520">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=27367">ROD</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191520", true); </script>
				
			</div>

			<div class="smallfont">����������</div>
			<div class="smallfont">������������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=27367"><img title="������ ��� ROD" src="showthread2_files/image_002.jpg" alt="������ ��� ROD" border="0" height="76" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 30.11.2008</div>
				<div>�����: 127.0.0.1</div>
				
				<div>
					���������: 2,151
				</div>
				
				
				<div><span id="repdisplay_191520_27367">���������: 412</span></div>
				
				<div><a href="#" onclick="imwindow('icq', '27367', 500, 450); return false;"><img title="��������� ��������� ��� ROD � ������� ICQ" src="showthread2_files/im_icq.gif" alt="��������� ��������� ��� ROD � ������� ICQ" border="0"></a>    <a href="#" onclick="imwindow('skype', '27367', 400, 285); return false;"><img title="��������� ��������� ��� ROD � ������� Skype�" src="showthread2_files/russian_pinky.png" alt="��������� ��������� ��� ROD � ������� Skype�" border="0"></a></div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191520" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191520">
			
			<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">������:</div>
	<table border="0" cellpadding="8" cellspacing="0" width="100%">
	<tbody><tr>
		<td class="alt2" style="border: 1px inset;">
			
				������� ��������� ������ ������ ����� � ���������� ��������� ������� �����.
			
		</td>
	</tr>
	</tbody></table>
</div>�� ����� ��� ������ ������ (����� ���������� ����� ���������).<br>
<br>
<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">������:</div>
	<table border="0" cellpadding="8" cellspacing="0" width="100%">
	<tbody><tr>
		<td class="alt2" style="border: 1px inset;">
			
				� ���� ����������?
			
		</td>
	</tr>
	</tbody></table>
</div>����� ����, ����� � ������. ����� ������� ������� ��� Borland C++ 6 ����������, ��� �������� ���� �������� ����������.
		</div>
		<!-- / message -->

		

		
		

		

		

		
		<!-- edit note -->
			<div class="smallfont">
				<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
				<em>
					
						��������� ��� ��������������� ROD; 23.01.2009 � <span class="time">10:47</span>.
					
					
				</em>
			</div>
		<!-- / edit note -->
		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="ROD ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="ROD ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191520" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191520 popup menu -->
<div class="vbmenu_popup" id="postmenu_191520_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">ROD</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=27367">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=27367" rel="nofollow">����� ��� ��������� �� ROD</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191520 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191520 --><!-- post #191599 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191599" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post191599" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191599"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			23.01.2009, 13:45
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191599&amp;postcount=8" target="new" rel="nofollow" id="postcount191599" name="8"><strong>8</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191599">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=29257">Sazary</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191599", true); </script>
				
			</div>

			<div class="smallfont">� ����</div>
			<div class="smallfont">����������� ���������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=29257"><img title="������ ��� Sazary" src="showthread2_files/image.jpg" alt="������ ��� Sazary" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 19.12.2008</div>
				
				
				<div>
					���������: 5,786
				</div>
				
				
				<div><span id="repdisplay_191599_29257">���������: 969</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191599" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191599">
			
			<b>ROD</b> � �� ����������� �������� ���������� ��������� ) ���� �� ������ ������ �� ���������.<br>
 ������� ��������.. ��������� ����� "current player make incorrect turn", ���� ��� ����� ��������� (� ������ ������).<br>
���� ��� ����� �� 11 ��� ������ �_�.. ��� ������ ��������.<br>
<br>
-------<br>
��������� ������� � ��� ��������� �����, ������ ���� ��������-������. �������, �� �� ������?<br>
 <br>
���� ����������. ���������� - ��������� ���-���� (1..9). ��������� 
�����, ����������� �� ������� ����������� (��� � �� ������).. ������ ���
 �� ����������� ���� � ��������� ��� ������� �������. ��� ������ ����� -
 ��� ���� ���������. ���� ���������� ���������, �� �������� �� ��� 
��������.<br>
<br>
��� ������ ������� ��������� ������� ���� "ArsKNrez.ffsp" � ���������� 
���� ����������, ������� ����������� ��� ��������� ��������.
		</div>
		<!-- / message -->

		
		<!-- attachments -->
			<div style="padding: 8px;">

			

			

			

			
				<fieldset class="fieldset">
					<legend>��������</legend>
					<table border="0" cellpadding="0" cellspacing="3">
					<tbody><tr>
	<td><img title="��� �����: txt" class="inlineimg" src="showthread2_files/txt.gif" alt="��� �����: txt" style="vertical-align: baseline;" border="0" height="16" width="16"></td>
	<td><a href="http://programmersforum.ru/attachment.php?attachmentid=8199&amp;d=1232707534" target="_blank">krest-nol-log.txt</a> (853 ����, 85 ����������)</td>
</tr><tr>
	<td><img title="��� �����: rar" class="inlineimg" src="showthread2_files/rar.gif" alt="��� �����: rar" style="vertical-align: baseline;" border="0" height="16" width="16"></td>
	<td><a href="http://programmersforum.ru/attachment.php?attachmentid=8202&amp;d=1232714190">KRESTIKI-NOLIKI-beta1.rar</a> (17.0 ��, 108 ����������)</td>
</tr>
					</tbody></table>
				</fieldset>
			

			

			</div>
		<!-- / attachments -->
		

		
		

		

		

		
		<!-- edit note -->
			<div class="smallfont">
				<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
				<em>
					
						��������� ��� ��������������� Sazary; 23.01.2009 � <span class="time">15:36</span>.
					
					
						�������: ��������
					
				</em>
			</div>
		<!-- / edit note -->
		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Sazary ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="Sazary ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191599" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191599 popup menu -->
<div class="vbmenu_popup" id="postmenu_191599_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Sazary</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=29257">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=29257" rel="nofollow">����� ��� ��������� �� Sazary</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191599 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191599 --><!-- post #191640 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191640" style="padding: 0px 0px 8px;">
	<!-- this is not the last post shown on the page -->



<table id="post191640" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191640"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			23.01.2009, 15:52
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191640&amp;postcount=9" target="new" rel="nofollow" id="postcount191640" name="9"><strong>9</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191640">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=19542">Arigato</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191640", true); </script>
				
			</div>

			<div class="smallfont">WEB-�����������</div>
			<div class="smallfont">����������� ���������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=19542"><img title="������ ��� Arigato" src="showthread2_files/image_003.jpg" alt="������ ��� Arigato" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 27.07.2008</div>
				<div>�����: ������</div>
				
				<div>
					���������: 3,876
				</div>
				
				
				<div><span id="repdisplay_191640_19542">���������: 1224</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191640" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191640">
			
			�������� ���������� �� ����� � ����������� �����������. ����� ���� 
���������� �������������� �� ����-�������� ���������. �������� ���������
 ����������.
		</div>
		<!-- / message -->

		
		<!-- attachments -->
			<div style="padding: 8px;">

			

			

			

			
				<fieldset class="fieldset">
					<legend>��������</legend>
					<table border="0" cellpadding="0" cellspacing="3">
					<tbody><tr>
	<td><img title="��� �����: rar" class="inlineimg" src="showthread2_files/rar.gif" alt="��� �����: rar" style="vertical-align: baseline;" border="0" height="16" width="16"></td>
	<td><a href="http://programmersforum.ru/attachment.php?attachmentid=8203&amp;d=1232715034">XO.rar</a> (186.9 ��, 148 ����������)</td>
</tr>
					</tbody></table>
				</fieldset>
			

			

			</div>
		<!-- / attachments -->
		

		
		

		
		<!-- sig -->
			<div>
				__________________<br>
				<font size="1"><i>��� ���� ���� ������, ��� �� �����...</i></font>
			</div>
		<!-- / sig -->
		

		

		
		<!-- edit note -->
			<div class="smallfont">
				<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
				<em>
					
						��������� ��� ��������������� Arigato; 23.01.2009 � <span class="time">18:09</span>.
					
					
				</em>
			</div>
		<!-- / edit note -->
		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Arigato ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="Arigato ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191640" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191640 popup menu -->
<div class="vbmenu_popup" id="postmenu_191640_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Arigato</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=19542">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=19542" rel="nofollow">����� ��� ��������� �� Arigato</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191640 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191640 --><!-- post #191723 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

	<div id="edit191723" style="padding: 0px 0px 8px;">
	



<table id="post191723" class="tborder" align="center" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="font-weight: normal; border-width: 1px 0px 1px 1px; border-style: solid none solid solid; border-color: rgb(92, 94, 92) -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
			<!-- status icon and date -->
			<a name="post191723"><img title="������" class="inlineimg" src="showthread2_files/post_old.gif" alt="������" border="0"></a>
			23.01.2009, 17:33
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="font-weight: normal; border-width: 1px 1px 1px 0px; border-style: solid solid solid none; border-color: rgb(92, 94, 92) rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
			&nbsp;
			#<a href="http://programmersforum.ru/showpost.php?p=191723&amp;postcount=10" target="new" rel="nofollow" id="postcount191723" name="10"><strong>10</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-left: 1px solid rgb(92, 94, 92); border-width: 0px 1px; border-style: none solid; border-color: -moz-use-text-color rgb(92, 94, 92);" width="175">

			<div id="postmenu_191723">
				
				<a class="bigusername" href="http://programmersforum.ru/member.php?u=29257">Sazary</a>
				<script type="text/javascript"> vbmenu_register("postmenu_191723", true); </script>
				
			</div>

			<div class="smallfont">� ����</div>
			<div class="smallfont">����������� ���������</div>
			

			
				<div class="smallfont">
					&nbsp;<br><a href="http://programmersforum.ru/member.php?u=29257"><img title="������ ��� Sazary" src="showthread2_files/image.jpg" alt="������ ��� Sazary" border="0" height="80" width="80"></a>
				</div>
			

			<div class="smallfont">
				&nbsp;<br>
				<div>�����������: 19.12.2008</div>
				
				
				<div>
					���������: 5,786
				</div>
				
				
				<div><span id="repdisplay_191723_29257">���������: 969</span></div>
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_191723" style="border-right: 1px solid rgb(92, 94, 92);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				<img title="�� ���������" class="inlineimg" src="showthread2_files/icon1.gif" alt="�� ���������" border="0">
				
			</div>
			<hr style="color: rgb(92, 94, 92); background-color: rgb(92, 94, 92);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_191723">
			
			<b>Arigato</b> � �� ����������, ��� ����� �������� ��� ����������� ��������? � �� ������/����/���� ������ ������� �� �������..
		</div>
		<!-- / message -->

		

		
		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92);">
		<img title="Sazary ��� ������" class="inlineimg" src="showthread2_files/user_offline.gif" alt="Sazary ��� ������" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-right: 1px solid rgb(92, 94, 92); border-width: 0px 1px 1px 0px; border-style: none solid solid none; border-color: -moz-use-text-color rgb(92, 94, 92) rgb(92, 94, 92) -moz-use-text-color;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://programmersforum.ru/newreply.php?do=newreply&amp;p=191723" rel="nofollow"><img title="�������� � ������������" src="showthread2_files/quote.gif" alt="�������� � ������������" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 191723 popup menu -->
<div class="vbmenu_popup" id="postmenu_191723_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Sazary</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/member.php?u=29257">���������� �������</a></td></tr>
	
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://programmersforum.ru/search.php?do=finduser&amp;u=29257" rel="nofollow">����� ��� ��������� �� Sazary</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 191723 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #191723 --><div id="lastpost"></div></div>

<!-- start content table -->
<!-- open content container -->

<div align="center">
	<div class="page" style="text-align: left;">
		<div style="padding: 0px;" align="left">

<!-- / start content table -->

<!-- controls below postbits -->
<table style="margin-top: -5px;" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
	
		<td class="smallfont"><a href="http://programmersforum.ru/newreply.php?do=newreply&amp;noquote=1&amp;p=191723" rel="nofollow"><img title="�����" src="showthread2_files/reply.gif" alt="�����" border="0"></a></td>
	
	
		<td align="right"><div class="pagenav" align="right">
<table class="tborder" border="0" cellpadding="3" cellspacing="1">
<tbody><tr>
	<td class="vbmenu_control" style="font-weight: normal;">�������� 1 �� 2</td>
	
	
		<td class="alt2"><span class="smallfont" title="�������� � 1 �� 10 �� 20."><strong>1</strong></span></td>
 <td class="alt1"><a class="smallfont" href="http://programmersforum.ru/showthread.php?t=33566&amp;page=2" title="� 11 �� 20 �� 20">2</a></td>
	<td class="alt1"><a rel="next" class="smallfont" href="http://programmersforum.ru/showthread.php?t=33566&amp;page=2" title="��������� �������� - � 11 �� 20 �� 20">&gt;</a></td>
	
	<td style="cursor: pointer;" id="pagenav.143" class="vbmenu_control" title=""> <img alt="" title="" src="showthread2_files/menu_open.gif" border="0"></td>
</tr>
</tbody></table>
</div>
		
		</td>
	
</tr>
</tbody></table>
<!-- / controls below postbits -->







<!-- ������.������ -->
<script type="text/javascript"><!--
yandex_partner_id = 5778;
yandex_site_bg_color = 'FFFFFF';
yandex_site_charset = 'windows-1251';
yandex_ad_format = 'direct';
yandex_font_size = 1.1;
yandex_direct_type = 'horizontal';
yandex_direct_border_type = 'ad';
yandex_direct_limit = 2;
yandex_direct_header_bg_color = 'CCCCCC';
yandex_direct_bg_color = 'FFFFFF';
yandex_direct_border_color = 'CCCCCC';
yandex_direct_title_color = '000000';
yandex_direct_url_color = '666666';
yandex_direct_all_color = '000000';
yandex_direct_text_color = '000000';
yandex_direct_hover_color = 'CCCCCC';
document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/system/context.js"></sc'+'ript>');
//--></script><script type="text/javascript" src="showthread2_files/context.js"></script><link rel="stylesheet" type="text/css" href="showthread2_files/context_r469.css"><script type="text/javascript" src="showthread2_files/context_static_r494.js" yandex_load_check="yes"></script><script type="text/javascript" src="showthread2_files/5778.htm" yandex_load_check="yes"></script><style type="text/css">#y5_direct1 .y5_item {background-color: #FFFFFF !important;} #y5_direct1 .y5_item {border-color: #CCCCCC !important; border-style: solid !important;} #y5_direct1 .y5_bg {background-color: #CCCCCC !important;} #y5_direct1 .y5_ad div a {color: #000000 !important;} #y5_direct1 .y5_ad div {color: #000000 !important;} #y5_direct1 .y5_ad span, #y5_direct1 .y5_ad span a {color: #666666 !important;} #y5_direct1 .y5_all a, #y5_direct1 .y5_how a {color: #000000 !important;} #y5_direct1 .y5_ad div a:hover {color: #CCCCCC !important;} #y5_direct1 .y5_icon em {background-color: #666666 !important;} #y5_direct1 {font-size: 1.1em !important;} </style><div id="y5_direct1" class="y5 y5_nf y5_horizontal snap_noshots"><div class="y5_ba y5_ads2 y5_no_warnings y5_exp0"><div class="y5_h"><div class="y5_ya"><span class="y5_black"><em>�</em>�����</span><span class="y5_black y5_bg"><a class="snap_noshots" href="http://direct.yandex.ru/?partner" target="_blank">������</a></span></div><div class="y5_how"><span><a target="_blank" href="http://advertising.yandex.ru/welcome/?from=context">���� ����������</a></span></div></div><table class="y5_ads"><tbody><tr><td class="y5_item"><div class="y5_ad"><div class="ad-link"><a class="snap_noshot" href="http://an.yandex.ru/count/RNL2wCJiw5840000ZheGYD04XP9f0PK2cm5kGoi1YAjC4Gw9fpTL8fXddPhhJvo_xQyp1PQWntR3cgHl806Ifjp14eglFmeDfP4YoP6sH03i1e-mIZRA39-Zo56QaoMPLukZy-bqGeoGSGAWaE84f810VWm0?test-tag=35816494" target="_blank">������� ������� �� 5 ������</a></div><div>�� ������� ����� ��� - ������ ������� �� YPAG.RU</div> <span class="url">www.ypag.ru</span></div></td><td class="y5_nbsp"><div></div></td><td class="y5_item"><div class="y5_ad"><div class="ad-link"><a class="snap_noshot" href="http://an.yandex.ru/count/RNL2w5LvbI840000ZheGYD04XP9f0PK2cm5kGoi1CeYoN0mW0OcIPpYOI9scXTK5dB_jhpC5bg37TiEQgCrDJPAYbuaLYgvo7W-baPpOaRP40Em6Zx9HlcyBdxRrp645aoMPLukeHFUTGeoGSGAWa742f810UGq0?test-tag=35816494" target="_blank">���������� ����� ����������</a></div><div>������� � ������� ����������� ��� ���������, ��� ��� � ��� �����������</div> <span class="url">www.winwins.ru</span></div></td></tr></tbody></table></div></div>


<!-- lightbox scripts -->
	<script type="text/javascript" src="showthread2_files/vbulletin_lightbox.js"></script>
	<script type="text/javascript">
	<!--
	vBulletin.register_control("vB_Lightbox_Container", "posts", 1);
	//-->
	</script>
<!-- / lightbox scripts -->










<!-- next / previous links -->
	<br>
	<div class="smallfont" align="center">
		<strong>�</strong>
			<a href="http://programmersforum.ru/showthread.php?t=33566&amp;goto=nextoldest" rel="nofollow">���������� ����</a>
			|
			<a href="http://programmersforum.ru/showthread.php?t=33566&amp;goto=nextnewest" rel="nofollow">��������� ����</a>
		<strong>�</strong>
	</div>
<!-- / next / previous links -->







<!-- popup menu contents -->
<br>

<!-- thread tools menu -->
<div class="vbmenu_popup" id="threadtools_menu" style="display: none;">
<form action="postings.php?t=33566&amp;pollid=" method="post" name="threadadminform">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">����� ����<a name="goto_threadtools"></a></td>
	</tr>
	<tr>
		<td class="vbmenu_option"><img title="������ ��� ������" class="inlineimg" src="showthread2_files/printer.gif" alt="������ ��� ������"> <a href="http://programmersforum.ru/printthread.php?t=33566" accesskey="3" rel="nofollow">������ ��� ������</a></td>
	</tr>
	
	<tr>
		<td class="vbmenu_option"><img title="��������� �� ����������� �����" class="inlineimg" src="showthread2_files/sendtofriend.gif" alt="��������� �� ����������� �����"> <a href="http://programmersforum.ru/sendmessage.php?do=sendtofriend&amp;t=33566" rel="nofollow">��������� �� ����������� �����</a></td>
	</tr>
	
	
	
	
	</tbody></table>
</form>
</div>
<!-- / thread tools menu -->

<!-- **************************************************** -->



<!-- **************************************************** -->



<!-- / popup menu contents -->


<!-- forum rules and admin links -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="bottom">
	<td valign="top" width="100%">
		<table class="tborder" border="0" cellpadding="8" cellspacing="1" width="210">
<thead>
<tr>
	<td class="thead">
		<a style="float: right;" href="#top" onclick="return toggle_collapse('forumrules')"><img id="collapseimg_forumrules" src="showthread2_files/collapse_thead.gif" alt="" border="0"></a>
		���� ����� � �������
	</td>
</tr>
</thead>
<tbody id="collapseobj_forumrules" style="">
<tr>
	<td class="alt1" nowrap="nowrap"><div class="smallfont">
		
		<div>�� <strong>�� ������</strong> ��������� ����� ����</div>
		<div>�� <strong>�� ������</strong> �������� � �����</div>
		<div>�� <strong>�� ������</strong> ����������� ��������</div>
		<div>�� <strong>�� ������</strong> ������������� ���� ���������</div>
		<hr>
		
		<div><a href="http://programmersforum.ru/misc.php?do=bbcode" target="_blank">BB ����</a> <strong>���.</strong></div>
		<div><a href="http://programmersforum.ru/misc.php?do=showsmilies" target="_blank">������</a> <strong>���.</strong></div>
		<div><a href="http://programmersforum.ru/misc.php?do=bbcode#imgcode" target="_blank">[IMG]</a> ��� <strong>���.</strong></div>
		<div>HTML ��� <strong>����.</strong></div>
		<hr>
		<div><a href="http://programmersforum.ru/misc.php?do=showrules" target="_blank">Forum Rules</a></div>
	</div></td>
</tr>
</tbody>
</table>
	</td>
	<td class="smallfont" align="right">
		<table border="0" cellpadding="0" cellspacing="0">
		
		<tbody><tr>
			<td>
			<div class="smallfont" style="text-align: left; white-space: nowrap;">
	<form action="forumdisplay.php" method="get">
	<input name="s" value="" type="hidden">
	<input name="daysprune" value="" type="hidden">
	<strong>������� �������</strong><br>
	<select name="f" onchange="this.form.submit();">
		<optgroup label="��������� �� ������">
			<option value="cp">��� �������</option>
			<option value="pm">������ ���������</option>
			<option value="subs">��������</option>
			<option value="wol">��� �� ������</option>
			<option value="search">����� �� ������</option>
			<option value="home">������� �������� ������</option>
		</optgroup>
		
		<optgroup label="�������">
		<option value="45" class="fjdpth0"> ������� � ����������������</option>
<option value="31" class="fjdpth1">&nbsp; &nbsp;  ������ ���������</option>
<option value="7" class="fjdpth1">&nbsp; &nbsp;  �������</option>
<option value="1" class="fjdpth0"> Delphi ����������������</option>
<option value="2" class="fjdpth1">&nbsp; &nbsp;  ����� ������� Delphi</option>
<option value="47" class="fjdpth1">&nbsp; &nbsp;  ����������� � Delphi</option>
<option value="39" class="fjdpth1">&nbsp; &nbsp;  ���������� Delphi</option>
<option value="3" class="fjdpth1">&nbsp; &nbsp;  ������ � ����� � Delphi</option>
<option value="5" class="fjdpth1">&nbsp; &nbsp;  �� � Delphi</option>
<option value="8" class="fjdpth0"> �������������� ����������������</option>
<option value="4" class="fjdpth1">&nbsp; &nbsp;  Win Api</option>
<option value="9" class="fjdpth1">&nbsp; &nbsp;  Assembler</option>
<option value="13" class="fjdpth0"> C++ ����������������</option>
<option value="14" class="fjdpth1">&nbsp; &nbsp;  ����� ������� C/C++</option>
<option value="40" class="fjdpth1">&nbsp; &nbsp;  ����������������� ����������������</option>
<option value="41" class="fjdpth1">&nbsp; &nbsp;  ���������������� ��� .NET</option>
<option value="10" class="fjdpth0"> Java ����������������</option>
<option value="11" class="fjdpth1">&nbsp; &nbsp;  Java SE</option>
<option value="49" class="fjdpth1">&nbsp; &nbsp;  Java ME</option>
<option value="15" class="fjdpth0"> Web ����������������</option>
<option value="16" class="fjdpth1">&nbsp; &nbsp;  HTML � CSS</option>
<option value="12" class="fjdpth1">&nbsp; &nbsp;  Javascript � ������ ���������� �������</option>
<option value="17" class="fjdpth1">&nbsp; &nbsp;  PHP � ������ ��������� �������</option>
<option value="48" class="fjdpth1">&nbsp; &nbsp;  WordPress � ������ CMS</option>
<option value="32" class="fjdpth0"> ����������</option>
<option value="6" class="fjdpth1">&nbsp; &nbsp;  ������������</option>
<option value="33" class="fjsel" selected="selected">&nbsp; &nbsp;  �������� � ���������� ���</option>
<option value="44" class="fjdpth1">&nbsp; &nbsp;  ���� SQL</option>
<option value="34" class="fjdpth1">&nbsp; &nbsp;  ������������ �������</option>
<option value="50" class="fjdpth1">&nbsp; &nbsp;  ��������������</option>
<option value="18" class="fjdpth0"> Microsoft Office � VBA</option>
<option value="20" class="fjdpth1">&nbsp; &nbsp;  Microsoft Office Excel</option>
<option value="21" class="fjdpth1">&nbsp; &nbsp;  Microsoft Office Access</option>
<option value="19" class="fjdpth1">&nbsp; &nbsp;  Microsoft Office Word</option>
<option value="28" class="fjdpth0"> ������ ��� ������������</option>
<option value="30" class="fjdpth1">&nbsp; &nbsp;  ������ �� ���������� ������</option>
<option value="29" class="fjdpth1">&nbsp; &nbsp;  �������</option>
<option value="23" class="fjdpth0"> ���� �������������</option>
<option value="37" class="fjdpth1">&nbsp; &nbsp;  � ������ � ������ �����</option>
<option value="24" class="fjdpth1">&nbsp; &nbsp;  ���������� ������</option>
<option value="25" class="fjdpth1">&nbsp; &nbsp;  ���������� ��������</option>
<option value="46" class="fjdpth1">&nbsp; &nbsp;  ������</option>
<option value="26" class="fjdpth1">&nbsp; &nbsp;  ��������� �������</option>

		</optgroup>
		
	</select><input class="button" value="�����" type="submit">
	</form>
</div>
			</td>
		</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table>
<!-- /forum rules and admin links -->

<br>

<table class="tborder" align="center" border="0" cellpadding="8" cellspacing="1" width="100%">
<thead>
	<tr>
		<td class="tcat" colspan="5" width="100%">
			<a style="float: right;" href="#top" onclick="return toggle_collapse('similarthreads')"><img id="collapseimg_similarthreads" src="showthread2_files/collapse_tcat.gif" alt="" border="0"></a>
			������� ����<a name="similarthreads"></a>
		</td>
	</tr>
</thead>
<tbody id="collapseobj_similarthreads" style="">
<tr class="thead" align="center">
	<td class="thead" width="40%">����</td>
	<td class="thead" nowrap="nowrap" width="15%">�����</td>
	<td class="thead" width="20%">������</td>
	<td class="thead" width="5%">�������</td>
	<td class="thead" width="20%">��������� ���������</td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://programmersforum.ru/showthread.php?t=1634" title="����� � ���� ��������, ������� �������. � ��� ����. 
�� ������ ������, ��� ��� ��� ������ ���� ������ � Borland �++.">�������� ������ �� C++</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">Alar</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">�������� � ���������� ���</span></td>
	<td class="alt2" align="center"><span class="smallfont">11</span></td>
	<td class="alt1" align="right"><span class="smallfont">15.03.2010 <span class="time">17:09</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://programmersforum.ru/showthread.php?t=23477" title="������������ ���� ����... ������ �������... ��� ��� �� �������� ����� ���������, ��������� ��� ���������! ���� ���� ��� ���� ������ ���������� 0 �...">�������� ������</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">aesoem</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">����� ������� Delphi</span></td>
	<td class="alt2" align="center"><span class="smallfont">10</span></td>
	<td class="alt1" align="right"><span class="smallfont">22.06.2009 <span class="time">21:41</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://programmersforum.ru/showthread.php?t=33524" title="����� � ���� ����� �������, �� ���������� � ���������� �����������, � ������ - �� ������ ����� � �� ����� ���� �� ���� ��� ���-�� (�� ��� �) �����. ...">��������-������ �++</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">ROD</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">������ ���������</span></td>
	<td class="alt2" align="center"><span class="smallfont">7</span></td>
	<td class="alt1" align="right"><span class="smallfont">21.12.2008 <span class="time">20:07</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://programmersforum.ru/showthread.php?t=11067" title="������ ����,������ ���� �� ����� �������, � ������� ��� ��� ��������� ������, �� � �� ���� ������ ������ ��� ���������������, ���������� ��������...">�������� - ������</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">��������</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">������ ���������</span></td>
	<td class="alt2" align="center"><span class="smallfont">6</span></td>
	<td class="alt1" align="right"><span class="smallfont">09.05.2008 <span class="time">22:09</span></span></td>
</tr>
<tr>
	<td class="alt1" align="left">
		
		<span class="smallfont"> <a href="http://programmersforum.ru/showthread.php?t=2491" title="����, ���������� ����� �����, �������� �������� ���� � ����������� &quot;��������-������&quot;. ���� ������ �������� � ������� �������������� ������, � ������...">��������-������ Delphi</a></span>
	</td>
	<td class="alt2" nowrap="nowrap"><span class="smallfont">yulia</span></td>
	<td class="alt1" nowrap="nowrap"><span class="smallfont">������ ���������</span></td>
	<td class="alt2" align="center"><span class="smallfont">12</span></td>
	<td class="alt1" align="right"><span class="smallfont">18.04.2007 <span class="time">18:03</span></span></td>
</tr>

</tbody>
</table>

<br>

<br><center>��� ��������-�������� ������ �� ���������: <a href="http://computers.wikimart.ru/">���������� � ����</a>, <a href="http://books.wikimart.ru/">�������������� ����� � ���������������� ����������</a>, <a href="http://watch.wikimart.ru/wristwatch/">�������� ����</a>.
<br><br>
<div id="mixkt_4294940266" style="height: 110px;"><table id="4294940266_269" border="0" cellpadding="0" cellspacing="0"><tbody><tr> <td> <table bgcolor="#808080" border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr bgcolor="#ffffff" valign="top"><td width="50%"><table cellpadding="5"><tbody><tr>          <td align="center" width="100"><a href="http://www2.mixmarket.biz/clk.php?id=8739952&amp;gid=4294940266&amp;cat=1000820" target="_blank"><img src="showthread2_files/8739952.jpg" border="0" height="100" width="100"></a></td>          <td valign="top">  <center><a href="http://www2.mixmarket.biz/clk.php?id=8739952&amp;gid=4294940266&amp;cat=1000820" target="_blank">������� ������������ Synology DS710+</a> - <span>21400 ���.</span></center>  <small><br><br> <a href="http://www2.mixmarket.biz/clk.php?id=8739952&amp;gid=4294940266&amp;cat=1000820" target="_blank">Flash Computers</a></small></td>          </tr></tbody></table></td><td width="50%"><table cellpadding="5"><tbody><tr>          <td align="center" width="100"><a href="http://www2.mixmarket.biz/clk.php?id=8740031&amp;gid=4294940266&amp;cat=1000820" target="_blank"><img src="showthread2_files/8740031.jpg" border="0" height="89" width="90"></a></td>          <td valign="top">  <center><a href="http://www2.mixmarket.biz/clk.php?id=8740031&amp;gid=4294940266&amp;cat=1000820" target="_blank">������� ������������ D-Link DNS-313</a> - <span>3600 ���.</span></center>  <small><br><br> <a href="http://www2.mixmarket.biz/clk.php?id=8740031&amp;gid=4294940266&amp;cat=1000820" target="_blank">Flash Computers</a></small></td>          </tr></tbody></table></td></tr> </tbody></table> </td> </tr></tbody></table><img src="showthread2_files/t_002.gif" height="1" width="1"><div style="width: 0pt; height: 0pt; font-size: 0pt;" id="mixmarket_ktbuff_4294940266_269"></div><div class="kt_footer" style="padding-top: 5px;" id="mix_block_1294942089"><div class="mix_outer mix_m_1294932579"><div class="mix_inter"><div style="margin: 0pt 0pt 0pt 20px; padding: 0pt; text-indent: -20px;">   <span style="white-space: nowrap;"><a href="http://mixmarket.biz/uni/clk.php?id=1294932175&amp;zid=1294942089&amp;s=e3eb&amp;tt=12151149" target="_blank" style="text-decoration: none ! important;" title="����������� ���� ����������"><img src="showthread2_files/99.png" style="vertical-align: middle; border: medium none; margin-right: 4px;" alt=""></a><a href="http://mixmarket.biz/uni/clk.php?id=1294932210&amp;zid=1294942089&amp;s=e3eb&amp;tt=12151149" target="_blank">���������� ������</a></span> <span style="white-space: nowrap;">��� <a href="http://mixmarket.biz/uni/clk.php?id=1294932209&amp;zid=1294942089&amp;s=e3eb&amp;tt=12151149" target="_blank">����� ���������</a></span> </div></div></div><img src="showthread2_files/t.gif" height="1" width="1"></div></div>
</center><br>
<script>
document.write('<scr' + 'ipt language="javascript" type="text/javascript" src="http://4294940266.kt.mixmarket.biz/show/4294940266/&div=mixkt_4294940266&r=' + escape(document.referrer) + '&rnd=' + Math.round(Math.random() * 100000) + '" charset="windows-1251"><' + '/scr' + 'ipt>');
</script><script language="javascript" type="text/javascript" src="showthread2_files/showthread.php%253Ft%253D40078&amp;rnd=82800" charset="windows-1251"></script><script language="javascript" type="text/javascript" src="showthread2_files/showthread.htm" charset="windows-1251"></script><script src="showthread2_files/cnt.js" defer="defer"></script><script src="showthread2_files/cnt.js" defer="defer"></script>

<br>
<div class="smallfont" align="center"><span class="time">11:48</span>.</div>
<br>


		</div>
	</div>
</div>

<!-- / close content container -->
<!-- /content area table -->

<form action="index.php" method="get" style="clear: left;">

<table class="page" align="center" border="0" cellpadding="8" cellspacing="0" width="100%.">
<tbody><tr>
	
		<td class="tfoot">
			<select name="styleid" onchange="switch_id(this, 'style')">
				<optgroup label="����� �����">
					<option value="2" class="" selected="selected">-- club</option>
<option value="1" class="">-- ������� �����</option>

				</optgroup>
			</select>
		</td>
	
	
	<td class="tfoot" align="right" width="100%">
		<div class="smallfont">
			<strong>
				<a href="http://programmersforum.ru/sendmessage.php" rel="nofollow" accesskey="9">�������� �����</a> -
				<a href="http://www.programmersclub.ru/">���� ������������� Delphi</a> -
				
				
				<a href="http://programmersforum.ru/archive/index.php">�����</a> -
				
				
				
				<a href="#top" onclick="self.scrollTo(0, 0); return false;">�����</a>
			</strong>
		</div>
	</td>
</tr>
</tbody></table>

<br>

<div align="center">
	<div class="smallfont" align="center">
	<!-- Do not remove this copyright notice -->
	Powered by vBulletin� Version 3.8.5<br>Copyright �2000 - 2010, Jelsoft Enterprises Ltd.
	<!-- Do not remove this copyright notice -->
	</div>

	<div class="smallfont" align="center">
	<!-- Do not remove cronimage or your scheduled tasks will cease to function -->
	
	<!-- Do not remove cronimage or your scheduled tasks will cease to function -->

	
	</div>
</div>

</form>

<br><center>

<a href="http://healpth.com/" title="doctors reviews">Ask Doctor</a> at Healpth.com
<br>
<br>


<!--Rating@Mail.ru COUNTEr--><script language="JavaScript" type="text/javascript"><!--
d=document;var a='';a+=';r='+escape(d.referrer)
js=10//--></script><script language="JavaScript1.1" type="text/javascript"><!--
a+=';j='+navigator.javaEnabled()
js=11//--></script><script language="JavaScript1.2" type="text/javascript"><!--
s=screen;a+=';s='+s.width+'*'+s.height
a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth)
js=12//--></script><script language="JavaScript1.3" type="text/javascript"><!--
js=13//--></script><script language="JavaScript" type="text/javascript"><!--
d.write('<a href="http://top.mail.ru/jump?from=1223103"'+
' target=_top><img src="http://d9.ca.b2.a1.top.list.ru/counter'+
'?id=1223103;t=47;js='+js+a+';rand='+Math.random()+
'" alt="�������@Mail.ru"'+' border=0 height=31 width=88/><\/a>')
if(11<js)d.write('<'+'!-- ')//--></script><a href="http://top.mail.ru/jump?from=1223103" target="_top"><img title="�������@Mail.ru" src="showthread2_files/counter.gif" alt="�������@Mail.ru" border="0" height="31" width="88/"></a><!-- <noscript><a
target=_top href="http://top.mail.ru/jump?from=1223103"><img
src="http://d9.ca.b2.a1.top.list.ru/counter?js=na;id=1223103;t=47"
border=0 height=31 width=88
alt="�������@Mail.ru"/></a></noscript><script language="JavaScript" type="text/javascript"><!--
if(11<js)d.write('--'+'>')//--><!--/COUNTER-->

<style type="text/css">a img {border: none;}</style>
<a href="http://programmersforum.ru/showthread.php?t=114639"><img src="showthread2_files/donateforumfooter.gif"></a>




<script type="text/javascript">
<!--
	// Main vBulletin Javascript Initialization
	vBulletin_init();
//-->
</script>

<!-- temp -->
<div style="display: none;">
	<!-- thread rate -->
	
		
	
	<!-- / thread rate -->
</div>

</center><div class="GleeThemeDefault" id="gleeBox" style="display: none; top: 35%;"><input style="font-size: 50px;" class="GleeThemeDefault" id="gleeSearchField" type="text"><div id="gleeSub"><div id="gleeSubText">Nothing selected</div><div id="gleeSubActivity"></div><div id="gleeSubURL"></div></div></div></body></html>