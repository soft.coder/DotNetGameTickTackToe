﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV;

namespace ASTU.DotNet.TickTackToe.GameParameter
{
    public interface IGameParam
    {
        Player First { get; set; }
        Player Second { get; set; }
        OrderPlay Order { get; set; }
    }
    public interface IPlayer
    {
         PlayerType Type { get; set; }
         CellState Figure { get;  }
         IRobot Robot { get; set; }
    }
}
