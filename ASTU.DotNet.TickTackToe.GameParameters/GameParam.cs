﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ASTU.DotNet.TickTackToe;
using ASTU.DotNet.TickTackToe.Robot.CherednichenkoSV;

namespace ASTU.DotNet.TickTackToe.GameParameter
{
    [Serializable]
    public enum PlayerType
    {
        User,
        Robot
    }
    [Serializable]
    public enum OrderPlay
    {
        FirstPlayer,
        SecondPlayer
    }
    [Serializable]
    public class Player :  IPlayer
    {
        public PlayerType Type { get; set; }
        public CellState Figure { get; set; }
        public IRobot Robot { get; set; }

        public Player()
        {
            this.Robot = new Robot.CherednichenkoSV.Robot();
        }
        public Player(PlayerType type, CellState figure)
            : this()
        {
            this.Type = type;
            this.Figure = figure;
        }
        public Player(Player player)
            : this(player.Type, player.Figure)
        { }
    }
    public class GameParam : MarshalByRefObject , IGameParam
    {
        public Player First { get; set; }
        public Player Second { get; set; }

        public OrderPlay Order { get; set; }

        public GameParam()
        { First = new Player(); Second = new Player(); Order = OrderPlay.FirstPlayer; }

        public GameParam(Player first, Player second)
        {
            this.Order = OrderPlay.FirstPlayer;
            this.First = new Player(first);
            this.Second = new Player(second);
        }
        public GameParam(IGameParam game)
            : this(game.First, game.Second)
        { }

    }
}
